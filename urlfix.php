<?php
try {
    require __DIR__ . '/app/bootstrap.php';
} catch (\Exception $e) {
    echo <<<HTML
<div style="font:12px/1.35em arial, helvetica, sans-serif;">
    <div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;">
        <h3 style="margin:0;font-size:1.7em;font-weight:normal;text-transform:none;text-align:left;color:#2f2f2f;">
        Autoload error</h3>
    </div>
    <p>{$e->getMessage()}</p>
</div>
HTML;
    exit(1);
}

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$obj = $bootstrap->getObjectManager();
$deploymentConfig = $obj->get('Magento\Framework\App\DeploymentConfig');

$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('admin');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');

$repo = $objectManager->get('Magento\Catalog\Model\ProductRepository');

$collection = $productCollection->create()->addAttributeToSelect('*')->load();

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$tableName = $resource->getTableName('catalog_product_entity_varchar');

foreach ($collection as $product){
    $name = $product->getName();
    $url_key = $product->getData('url_key');
    $sku = $product->getData('sku');
    $parts = explode('-', $url_key);
    $newkey = "";
    if (array_values(array_slice($parts, -1))[0] == $sku){
	array_pop($parts);
	foreach($parts as $part){
	    $newkey .= $part . '-';
	}
	$newkey = substr($newkey, 0, -1);
	try {
	    $product->setData('url_key', $newkey);
	    $product->save();
	//    $sql = "UPDATE " . $tableName . " SET `value` = '" . $newkey . "' WHERE `store_id` = '0' AND `attribute_id` = '119' AND `entity_id` = '" . $product->getData('entity_id') . "';";
	//    $connection->query($sql);
	}
	catch (\Exception $e){}
    }
    $sql = "UPDATE " . $tableName . " SET `value` = '" . $product->getData('url_key') . "' WHERE `store_id` = '0' AND `attribute_id` = '119' AND `entity_id` = '" . $product->getData('entity_id') . "';";
    $connection->query($sql);
/*    echo $name;
    echo "\n";
    echo $product->getData('url_key');
    echo "\n";
    echo $product->getData('sku');
    echo "\n";
    echo $newkey;
    echo "\n";
    echo "\n";
    echo "\n";*/
}
