<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Plugin\Layout;

use Magento\Framework\View\Layout\Element;
use Magento\Framework\View\Layout;
use Ecomatic\CacheWarmer\Model\Config;

/**
 * Class LayoutPlugin
 */
class LayoutPlugin
{
    /**
     * @var \Ecomatic\CacheWarmer\Model\Config
     */
    private $config;

    /**
     * @param \Ecomatic\CacheWarmer\Model\Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @param Layout $subject
     * @param string $result
     * @return string
     */
    public function afterGetOutput(Layout $subject, $result)
    {
        $paths = $subject->getUpdate()->asSimplexml()->xpath('//' . Element::TYPE_BLOCK . '[@cacheable="false"]');
        if (count($paths)) {
            foreach ($paths as $path) {
                $class = $path["class"];
                $name = $path["name"];

                $this->config->storeNotCacheableBlock((string)$class, (string)$name);
            }
        }

        return $result;
    }
}
