<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Plugin;

use Magento\Framework\App\CacheInterface;
use Psr\Log\LoggerInterface;

class AppCachePlugin
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    //    /**
    //     * @param CacheInterface $instance
    //     * @param array          $tags
    //     * @return void
    //     * @SuppressWarnings(PHPMD)
    //     */
    //    public function beforeClean($instance, $tags)
    //    {
    //        if (!is_array($tags)) {
    //            $tags = [$tags];
    //        }
    //
    //        @file_put_contents(
    //            dirname(__FILE__) . '/clean.log',
    //            date('d.m.Y H:i:s') . ' ' . implode($tags) . PHP_EOL . \Magento\Framework\Debug::backtrace(true),
    //            FILE_APPEND
    //        );
    //    }
}