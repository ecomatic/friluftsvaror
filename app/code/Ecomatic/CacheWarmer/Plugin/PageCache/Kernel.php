<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Plugin\PageCache;

/**
 * Builtin cache processor
 */
class Kernel
{
    /**
     * @var \Ecomatic\CacheWarmer\Helper\Info
     */
    protected $info;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @param  \Ecomatic\CacheWarmer\Helper\Info        $info
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Ecomatic\CacheWarmer\Helper\Info $info,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->info = $info;
        $this->objectManager = $objectManager;
        $this->moduleManager = $moduleManager;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundProcess($subject, \Closure $proceed, \Magento\Framework\App\Response\Http $response)
    {
        if ($this->moduleManager->isEnabled('Ecomatic_Seo') && self::checkProductSnippetsFunction()) {
            $response = $this->objectManager->get('\Ecomatic\Seo\Observer\Snippet')
                ->addProductSnippets(false, $response);
        }

        if (is_object($response->getHeader('Cache-Control'))) {
            $proceed($response);
        }

        $this->info->addInfoBlock($response, false);
    }

    /**
     * Check ProductSnippets function version
     *
     * @return bool
     */
    static protected function checkProductSnippetsFunction()
    {
        $checkProductSnippetsMethod = new \ReflectionMethod('\Ecomatic\Seo\Observer\Snippet', 'addProductSnippets');
        if (is_object($checkProductSnippetsMethod)
            && $checkProductSnippetsMethod->getNumberOfParameters() > 1
        ) {
            return true;
        }

        return false;
    }

}
