<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Controller\Adminhtml\Page;

use Ecomatic\CacheWarmer\Controller\Adminhtml\Page;

class Delete extends Page
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        if ($this->getRequest()->getParam('id')) {
            $ids = $this->getRequest()->getParam('id');

            if (!is_array($ids)) {
                $ids = [$ids];
            }

            foreach ($ids as $id) {
                try {
                    $page = $this->pageFactory->create();
                    $page->load($id);
                    $page->delete();
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                }
            }

            $this->messageManager->addSuccessMessage(
                __('%1 page(s) was removed.', count($ids))
            );
        } else {
            $this->messageManager->addErrorMessage(__('Please select page(s).'));
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}
