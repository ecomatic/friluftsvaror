<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Controller\Adminhtml\Page;

use Ecomatic\CacheWarmer\Controller\Adminhtml\Page;
use Ecomatic\CacheWarmer\Model\PageFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Ecomatic\CacheWarmer\Helper\Data as WarmerHelper;
use Ecomatic\CacheWarmer\Model\Warmer;

class Clean extends Page
{
    /**
     * @var Warmer
     */
    protected $warmer;

    /**
     * @param Warmer       $warmer
     * @param PageFactory  $pageFactory
     * @param WarmerHelper $warmerHelper
     * @param Registry     $registry
     * @param Context      $context
     */
    public function __construct(
        Warmer $warmer,
        PageFactory $pageFactory,
        WarmerHelper $warmerHelper,
        Registry $registry,
        Context $context
    ) {
        $this->warmer = $warmer;

        parent::__construct($pageFactory, $warmerHelper, $registry, $context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        if ($this->getRequest()->getParam('id')) {
            $ids = $this->getRequest()->getParam('id');

            if (!is_array($ids)) {
                $ids = [$ids];
            }

            foreach ($ids as $id) {
                try {
                    $page = $this->pageFactory->create();
                    $page->load($id);
                    $this->warmer->cleanPage($page);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                }
            }

            $this->messageManager->addSuccessMessage(
                __('%1 page(s) was cleaned.', count($ids))
            );
        } else {
            $this->messageManager->addErrorMessage(__('Please select page(s).'));
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}
