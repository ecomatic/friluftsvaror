<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Controller\Adminhtml\Page;

use Magento\Framework\Controller\ResultFactory;
use Ecomatic\CacheWarmer\Controller\Adminhtml\Page;

class Index extends Page
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $this->initPage($resultPage);

        $this->_addContent($resultPage->getLayout()
            ->createBlock('Ecomatic\CacheWarmer\Block\Adminhtml\Page'));

        $this->_addContent($resultPage->getLayout()
            ->createBlock('Ecomatic\CacheWarmer\Block\Adminhtml\Page\QuickReport', 'warmer.quick.report'));

        $resultPage->getLayout()->setChild('page.main.actions', 'warmer.quick.report', 'warmer.quick.report');

        return $resultPage;
    }
}
