<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Ecomatic\CacheWarmer\Helper\Data as WarmerHelper;

abstract class Job extends Action
{
    /**
     * @var \Ecomatic\CacheWarmer\Model\JobFactory
     */
    protected $jobFactory;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * @var WarmerHelper
     */
    protected $warmerHelper;

    /**
     * @param \Ecomatic\CacheWarmer\Model\JobFactory $jobFactory
     * @param WarmerHelper                           $warmerHelper
     * @param Registry                               $registry
     * @param Context                                $context
     */
    public function __construct(
        \Ecomatic\CacheWarmer\Model\JobFactory $jobFactory,
        WarmerHelper $warmerHelper,
        Registry $registry,
        Context $context
    ) {
        $this->jobFactory = $jobFactory;
        $this->warmerHelper = $warmerHelper;

        $this->registry = $registry;
        $this->context = $context;
        $this->backendSession = $context->getSession();
        $this->resultFactory = $context->getResultFactory();

        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     * @param \Magento\Backend\Model\View\Result\Page\Interceptor $resultPage
     * @return \Magento\Backend\Model\View\Result\Page\Interceptor
     */
    protected function initPage($resultPage)
    {
        $this->warmerHelper->checkCronStatus();

        $resultPage->setActiveMenu('Ecomatic_CacheWarmer::cache_warmer_job');
        $resultPage->getConfig()->getTitle()->prepend(__('Page Cache Warmer'));
        $resultPage->getConfig()->getTitle()->prepend(__('Jobs'));

        return $resultPage;
    }

    /**
     * @return \Ecomatic\CacheWarmer\Model\Job
     */
    public function initModel()
    {
        $job = $this->jobFactory->create();
        if ($this->getRequest()->getParam('id')) {
            $job->load($this->getRequest()->getParam('id'));
        }

        $this->registry->register('current_model', $job);

        return $job;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->context->getAuthorization()->isAllowed('Ecomatic_CacheWarmer::cache_warmer_job');
    }
}
