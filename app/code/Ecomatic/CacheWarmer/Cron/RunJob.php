<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Cron;

use Ecomatic\CacheWarmer\Model\JobFactory;

;
use Ecomatic\CacheWarmer\Model\Config;

class RunJob
{
    /**
     * @var JobFactory
     */
    protected $jobFactory;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @param JobFactory $jobFactory
     * @param Config     $config
     */
    public function __construct(
        JobFactory $jobFactory,
        Config $config
    ) {
        $this->jobFactory = $jobFactory;
        $this->config = $config;
    }

    /**
     * @return void
     */
    public function execute()
    {
        if (!$this->config->isEnabled()) {
            return;
        }
        
        /** @var \Ecomatic\CacheWarmer\Model\ResourceModel\Job\Collection $collection */
        $collection = $this->jobFactory->create()->getCollection()
            ->addFieldToFilter('started_at', ['null' => true]);

        foreach ($collection as $job) {
            if ($job && $job->getId()) {
                $job->run();
            }
        }
    }
}
