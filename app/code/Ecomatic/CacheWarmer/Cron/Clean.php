<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Cron;

use Ecomatic\CacheWarmer\Model\PageFactory;
use Ecomatic\CacheWarmer\Model\JobFactory;
use Ecomatic\CacheWarmer\Model\Config;

class Clean
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var JobFactory
     */
    protected $jobFactory;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @param PageFactory $pageFactory
     * @param JobFactory  $jobFactory
     * @param Config      $config
     */
    public function __construct(
        PageFactory $pageFactory,
        JobFactory $jobFactory,
        Config $config
    ) {
        $this->pageFactory = $pageFactory;
        $this->jobFactory = $jobFactory;
        $this->config = $config;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $jobCollection = $this->jobFactory->create()->getCollection()
            ->addFieldToFilter('finished_at', ['lteq' => date('Y-m-d H:i:s', time() - 2 * 24 * 60 * 60)]);

        /** @var \Ecomatic\CacheWarmer\Model\Job $job */
        foreach ($jobCollection as $job) {
            $job->delete();
        }

        $pageCollection = $this->pageFactory->create()->getCollection();

        /** @var \Ecomatic\CacheWarmer\Model\Page $page */
        foreach ($pageCollection as $page) {
            if ($this->config->isIgnoredPage($page)) {
                $page->delete();
            }
        }
    }
}
