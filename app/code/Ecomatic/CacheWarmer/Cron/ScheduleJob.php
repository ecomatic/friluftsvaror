<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Cron;

use Ecomatic\CacheWarmer\Model\JobFactory;
use Ecomatic\CacheWarmer\Model\Job;
use Magento\Framework\Stdlib\DateTime;

class ScheduleJob
{
    /**
     * @var JobFactory
     */
    protected $jobFactory;

    /**
     * @param JobFactory $jobFactory
     */
    public function __construct(
        JobFactory $jobFactory
    ) {
        $this->jobFactory = $jobFactory;
    }

    /**
     * @return void
     */
    public function execute()
    {
        /** @var \Ecomatic\CacheWarmer\Model\ResourceModel\Job\Collection $collection */
        $collection = $this->jobFactory->create()->getCollection();

        $collection->addFieldToFilter('started_at', ['null' => true])
            ->addFieldToFilter('priority', Job::PRIORITY_NORMAL);

        if ($collection->count() == 0) {
            $this->jobFactory->create()
                ->setPriority(Job::PRIORITY_NORMAL)
                ->setFilter([])
                ->save();
        }

        /** @var \Ecomatic\CacheWarmer\Model\ResourceModel\Job\Collection $collection */
        $collection = $this->jobFactory->create()->getCollection()
            ->addFieldToFilter('finished_at', ['null' => true])
            ->addFieldToFilter('started_at', ['notnull' => true]);
        $collection->getSelect()->where('NOW() - started_at > 1800');

        /** @var \Ecomatic\CacheWarmer\Model\Job $job */
        foreach ($collection as $job) {
            $job->setFinishedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT))
                ->save();
        }
    }
}
