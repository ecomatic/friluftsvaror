<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Cron;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Ecomatic\CacheWarmer\Helper\Data as DataHelper;
use Magento\Variable\Model\VariableFactory;

class FillRate
{
    /**
     * @var DataHelper
     */
    private $dataHelper;

    /**
     * @var VariableFactory
     */
    private $variableFactory;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    public function __construct(
        DataHelper $dataHelper,
        VariableFactory $variableFactory,
        TimezoneInterface $timezone
    ) {
        $this->dataHelper = $dataHelper;
        $this->variableFactory = $variableFactory;
        $this->timezone = $timezone;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $rate = $this->dataHelper->getFillRate();

        $variable = $this->variableFactory->create()
            ->loadByCode('cache_warmer_fill_rate');

        $value = $variable->getValue();

        if ($value) {
            $value = \Zend_Json::decode($value);
        } else {
            $value = [];
        }

        $value[$this->timezone->date()->format("H:i")] = $rate;

        if (count($value) > 600) {
            $value = array_slice($value, count($value) - 600, 600);
        }

        $variable->setCode('cache_warmer_fill_rate')
            ->setData('html_value', \Zend_Json::encode($value))
            ->save();
    }
}
