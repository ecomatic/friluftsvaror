<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Model\App\FrontController;

use Magento\Framework\App\Response\Http as ResponseHttp;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Store\Model\StoreManagerInterface;
use Ecomatic\CacheWarmer\Helper\VaryData as VaryDataHelper;
use Magento\Framework\App\FrontControllerInterface;
use Magento\Framework\App\RequestInterface;

class PagePlugin
{
    /**
     * @param VaryDataHelper            $varyDataHelper
     * @param StoreManagerInterface     $storeManager
     * @param CustomerSession           $customerSession
     * @param CustomerCollectionFactory $customerCollectionFactory
     */
    public function __construct(
        VaryDataHelper $varyDataHelper,
        StoreManagerInterface $storeManager,
        CustomerSession $customerSession,
        CustomerCollectionFactory $customerCollectionFactory
    ) {
        $this->varyDataHelper = $varyDataHelper;
        $this->storeManager = $storeManager;
        $this->customerSession = $customerSession;
        $this->customerCollectionFactory = $customerCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function beforeDispatch(
        FrontControllerInterface $subject,
        RequestInterface $request
    ) {
        $varyData = $this->varyDataHelper->getVaryData();
        if ($varyData) {
            /** @var \Magento\Store\Model\Store $store */
            $store = $this->storeManager->getStore();

            if (isset($varyData['current_currency'])) {
                $store->setCurrentCurrencyCode($varyData['current_currency']);
            }

            if (isset($varyData['customer_group'])) {
                $customer = $this->customerCollectionFactory->create()
                    ->addFieldToFilter('group_id', $varyData['customer_group'])
                    ->getFirstItem();
                if ($customer) {
                    $this->customerSession->loginById($customer->getId());
                }
            }
        }
    }
}
