<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Model\App\Response;

use Magento\Framework\App\Response\Http as ResponseHttp;
use Magento\Framework\Session\SessionManagerInterface;
use Ecomatic\CacheWarmer\Model\PageFactory;

class PagePlugin
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var SessionManagerInterface
     */
    protected $sessionManager;

    /**
     * @param PageFactory             $pageFactory
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        PageFactory $pageFactory,
        SessionManagerInterface $sessionManager
    ) {
        $this->pageFactory = $pageFactory;
        $this->sessionManager = $sessionManager;
    }

    /**
     * @param ResponseHttp $subject
     * @return void
     * @SuppressWarnings(PHPMD)
     */
    public function beforeSendResponse(ResponseHttp $subject)
    {
        $this->pageFactory->create()->onResponse();

        if (isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT'])
            && $_SERVER['HTTP_USER_AGENT'] == \Ecomatic\CacheWarmer\Model\Warmer::USER_AGENT
        ) {
            try {
                $this->sessionManager->start();
                $this->sessionManager->destroy();
                $this->sessionManager->clearStorage();
            } catch (\Exception $e) {
                file_put_contents(dirname(__FILE__) . '/log.txt', $e . PHP_EOL, FILE_APPEND);
            }
        }
    }

    /**
     * @param ResponseHttp $subject
     * @return void
     * @SuppressWarnings(PHPMD)
     */
    public function afterSendResponse(ResponseHttp $subject)
    {

    }
}
