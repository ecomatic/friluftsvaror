<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class WarmStrategy implements ArrayInterface
{
    const STRATEGY_POPULARITY = 'popularity';
    const STRATEGY_PAGE_TYPE = 'page_type';

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            self::STRATEGY_POPULARITY => __('Popularity'),
            self::STRATEGY_PAGE_TYPE  => __('Page Type')
        ];
    }
}