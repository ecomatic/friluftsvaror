<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Ecomatic\CacheWarmer\Model\ResourceModel\Page\CollectionFactory as PageCollectionFactory;

class PageType implements ArrayInterface
{
    /**
     * @var PageCollectionFactory
     */
    protected $pageCollectionFactory;

    /**
     * @param PageCollectionFactory $pageCollectionFactory
     */
    public function __construct(
        PageCollectionFactory $pageCollectionFactory
    ) {
        $this->pageCollectionFactory = $pageCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        $options = ['' => '-'];

        foreach ($this->getPageTypes() as $action) {
            $label = explode('_', $action);
            $label = array_map('ucfirst', $label);

            $options[$action] = implode(' · ', $label);
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getPageTypes()
    {
        $types = $this->pageCollectionFactory->create()->getUniquePageTypes();

        $types = array_merge_recursive(
            $types,
            ['cms_index_index', 'cms_page_view', 'catalog_category_view', 'catalog_product_view']
        );

        sort($types);

        return $types;
    }
}