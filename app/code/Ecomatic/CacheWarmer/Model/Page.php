<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\PageCache\Identifier as CacheIdentifier;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\App\PageCache\Cache;
use Magento\PageCache\Model\Config as PageCacheConfig;

/**
 * @method string getUri()
 * @method $this setUri($uri)
 *
 * @method string getCacheId()
 * @method $this setCacheId($cacheId)
 *
 * @method string getPageType()
 * @method $this setPageType($pageType)
 *
 * @method int getProductId()
 * @method $this setProductId($id)
 *
 * @method int getCategoryId()
 * @method $this setCategoryId($id)
 *
 * @method int getPopularity()
 * @method $this setPopularity($popularity)
 *
 * @method $this setVaryData($data)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Page extends AbstractModel
{
    /**
     * @var CacheIdentifier
     */
    protected $cacheIdentifier;

    /**
     * @var HttpContext
     */
    protected $httpContext;

    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Registry
     */
    protected $registry;

    public function __construct(
        CacheIdentifier $identifier,
        HttpContext $httpContext,
        Cache $cache,
        Config $config,
        Context $context,
        Registry $registry
    ) {
        $this->cacheIdentifier = $identifier;
        $this->httpContext = $httpContext;
        $this->cache = $cache;
        $this->config = $config;
        $this->registry = $registry;

        parent::__construct($context, $registry);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ecomatic\CacheWarmer\Model\ResourceModel\Page');
    }

    /**
     * @return bool
     */
    public function isCached()
    {
        if ($this->config->getCacheType() == PageCacheConfig::BUILT_IN) {
            return ($this->cache->load($this->getCacheId())) ? true : false;
        } else {
            try {
                $client = new \Zend_Http_Client();
                $client->setUri($this->getUri());
                $client->setConfig([
                    'maxredirects' => 0,
                    'timeout'      => 1,
                    'useragent'    => Warmer::STATUS_USER_AGENT,
                ]);

                if ($this->getVaryString()) {
                    $client->setCookie('X-Magento-Vary', $this->getVaryString());
                }

                $response = $client->request();
                $body = $response->getBody();

                if ($body == '*') {
                    return false;
                } else {
                    return true;
                }
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    /**
     * @return array
     */
    public function getVaryData()
    {
        return unserialize($this->getData('vary_data'));
    }

    /**
     * @return string
     */
    public function getVaryString()
    {
        $data = $this->getVaryData();

        if (!empty($data)) {
            ksort($data);

            return sha1(serialize($data));
        }

        return null;
    }

    /**
     * @param \Magento\Framework\App\Request\Http  $request
     * @param \Magento\Framework\App\Response\Http $response
     * @return void
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function collect($request, $response)
    {
        $matches = [];

        if (!$response->getHeader('Cache-Control')) {
            return;
        }

        if (!preg_match('/public.*s-maxage=(\d+)/', $response->getHeader('Cache-Control')->getFieldValue(), $matches)) {
            return;
        }

        $cacheId = $this->cacheIdentifier->getValue();

        $page = $this->load($cacheId, 'cache_id');

        $pageType = $request->getFullActionName();

        if (strpos($request->getUriString(), '?') !== false) {
            $pageType .= '_*';
        }

        if (!$page->getId()
            && $request->getFullActionName() !== '__'
            && strpos($request->getUriString(), '_=') === false
        ) {
            $product = $this->registry->registry('current_product');
            $category = $this->registry->registry('current_category');

            /** @var Page $page */
            $page = $this->getCollection()
                ->addFieldToFilter('uri', $request->getUriString())
                ->addFieldToFilter('cache_id', ['null' => true])
                ->getFirstItem();

            $page->setUri($request->getUriString())
                ->setCacheId($cacheId)
                ->setPageType($pageType)
                ->setProductId($product ? $product->getId() : false)
                ->setCategoryId($category ? $category->getId() : false)
                ->setVaryData(serialize($this->httpContext->getData()))
                ->setPopularity($page->getPopularity() + 1);

            if (!$this->config->isIgnoredPage($page)) {
                $page->save();
            }
        } elseif ($page->getId() && $pageType != $page->getPageType()) {
            $page->setUri($request->getUriString())
                ->setCacheId($cacheId)
                ->setPageType($pageType)
                ->setVaryData(serialize($this->httpContext->getData()))
                ->setPopularity($page->getPopularity() + 1)
                ->save();
        }
    }

    /**
     * @return void
     */
    public function onResponse()
    {
        $cacheId = $this->cacheIdentifier->getValue();
        $page = $this->load($cacheId, 'cache_id');

        if ($page->getId()) {
            $page->setPopularity($page->getPopularity() + 1)
                ->save();
        }
    }
}