<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Model;

use Magento\Framework\App\PageCache\Cache;
use Ecomatic\CacheWarmer\Helper\VaryData as VaryDataHelper;
use \Magento\Store\Model\StoreManagerInterface as StoreManager;

class Warmer
{
    const USER_AGENT        = 'CacheWarmer';
    const STATUS_USER_AGENT = 'CacheWarmerStatus';

    /**
     * @var int
     */
    protected $startTime;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @param Config         $config
     * @param Cache          $cache
     * @param VaryDataHelper $varyDataHelper
     * @param StoreManager   $storeManager
     */
    public function __construct(
        Config $config,
        Cache $cache,
        VaryDataHelper $varyDataHelper,
        StoreManager $storeManager
    ) {
        $this->config = $config;
        $this->cache = $cache;
        $this->varyDataHelper = $varyDataHelper;
        $this->storeManager = $storeManager;

        $this->startTime = microtime(true);
    }

    /**
     * @param ResourceModel\Page\Collection $collection
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function warmCollection($collection)
    {
        $queue = [];
        /** @var Page $page */
        while ($page = $collection->fetchItem()) {
            if ($page->isCached() || $this->isTimeout()) {
                continue;
            }

            if ($page->getVaryString() || $this->config->getWarmThreads() == 1) {
                $this->warmPage($page);

                if ($this->isValidPage($page)) {
                    yield $page;
                } else {
                    $page->delete();
                }
                continue;
            }

            $queue[] = $page;

            if (count($queue) >= $this->config->getWarmThreads()) {
                $this->warmPages($queue);

                foreach ($queue as $page) {
                    if ($this->isValidPage($page)) {
                        yield $page;
                    } else {
                        $page->delete();
                    }
                }

                $queue = [];
            }
        }
    }

    /**
     * @param Page[] $pages
     * @return void
     */
    public function warmPages($pages)
    {
        if (function_exists('curl_multi_init')) {
            $adapter = new \Magento\Framework\HTTP\Adapter\Curl();
            $options = [
                CURLOPT_USERAGENT      => self::USER_AGENT,
                CURLOPT_HEADER         => true,
                CURLOPT_SSL_VERIFYPEER => ($this->config->getVerifyPeer()) ? 0 : 1,
            ];

            $uri = [];

            foreach ($pages as $page) {
                $uri[] = $page->getUri();
            }

            $adapter->multiRequest($uri, $options);
            $adapter->close();

            usleep($this->config->getWarmDelay());
        } else {
            foreach ($pages as $page) {
                $this->warmPage($page);
            }
        }
    }

    /**
     * @param Page $page
     * @return bool
     * @throws \Zend_Http_Client_Exception
     */
    public function warmPage($page)
    {
        if ($this->config->getVerifyPeer()) {
            $adapter = new \Magento\Framework\HTTP\Adapter\Curl();
            $options = [
                CURLOPT_USERAGENT      => self::USER_AGENT,
                CURLOPT_HEADER         => true,
                CURLOPT_SSL_VERIFYPEER => ($this->config->getVerifyPeer()) ? 0 : 1,
            ];

            $uri[] = $page->getUri();

            $result = $adapter->multiRequest($uri, $options);
            $adapter->close();

            if (strpos($result[0], 'HTTP/1.1 200 OK') === false) {
                $page->delete();
            }

        } else {
            $client = new \Zend_Http_Client();
            $client->setUri($page->getUri());
            $client->setConfig([
                'maxredirects' => 0,
                'timeout'      => 30,
                'useragent'    => $this->varyDataHelper->getUserAgent($page->getVaryData()),
            ]);

            $client->setCookie('X-Magento-Vary', $page->getVaryString());

            $response = $client->request();

            if ($response->getStatus() != 200) {
                $page->delete();
            }
        }

        usleep($this->config->getWarmDelay());

        return true;
    }

    /**
     * @param string $url
     * @return true;
     */
    public function warmUrl($url)
    {
        $client = new \Zend_Http_Client();
        $client->setUri($url);
        $client->setConfig([
            'maxredirects' => 0,
            'timeout'      => 30,
            'useragent'    => $this->varyDataHelper->getUserAgent([]),
        ]);

        $client->setCookie('X-Magento-Vary', []);

        $client->request();

        return true;
    }

    /**
     * @param Page $page
     * @return bool
     */
    public function cleanPage($page)
    {
        if ($page->getCacheId()) {
            $this->cache->remove($page->getCacheId());
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isTimeout()
    {
        return (microtime(true) - $this->startTime) > $this->config->getWarmTime();
    }

    /**
     * @param Page $page
     * @return bool
     */
    public function isValidPage($page)
    {
        $page = $page->load($page->getId());

        if ($page->getCacheId()) {
            return true;
        }

        return false;
    }
}