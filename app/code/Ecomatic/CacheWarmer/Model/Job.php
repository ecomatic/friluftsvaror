<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime;
use Ecomatic\CacheWarmer\Model\Config\Source\WarmStrategy;
use Ecomatic\CacheWarmer\Helper\Data as DataHelper;

/**
 * @method int getPriority()
 * @method $this setPriority($priority)
 *
 * @method $this setFilter($array)
 *
 * @method string getStartedAt()
 * @method $this setStartedAt($startedAt)
 *
 * @method string getFinishedAt()
 * @method $this setFinishedAt($finishedAt)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Job extends AbstractModel
{
    const PRIORITY_NORMAL    = 1;
    const PRIORITY_EMERGENCY = 2;

    const STATUS_SCHEDULED = 1;
    const STATUS_RUNNING   = 2;
    const STATUS_COMPLETED = 3;

    /**
     * @var ResourceModel\Page\CollectionFactory
     */
    private $pageCollectionFactory;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Warmer
     */
    private $warmer;

    /**
     * @var DataHelper
     */
    private $dataHelper;

    public function __construct(
        ResourceModel\Page\CollectionFactory $jobCollectionFactory,
        Config $config,
        Warmer $warmer,
        DataHelper $dataHelper,
        Context $context,
        Registry $registry
    ) {
        $this->pageCollectionFactory = $jobCollectionFactory;
        $this->config = $config;
        $this->warmer = $warmer;
        $this->dataHelper = $dataHelper;

        parent::__construct($context, $registry);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ecomatic\CacheWarmer\Model\ResourceModel\Job');
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        if ($this->getStartedAt()) {
            if ($this->getFinishedAt()) {
                return self::STATUS_COMPLETED;
            }

            return self::STATUS_RUNNING;
        }

        return self::STATUS_SCHEDULED;
    }

    /**
     * @return array
     */
    public function getFilter()
    {
        if ($this->hasData('filter')) {
            return $this->getData('filter');
        }

        return unserialize($this->getData('filter_serialized'));
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        if ($this->hasData('info')) {
            return $this->getData('info');
        }

        try {
            return unserialize($this->getData('info_serialized'));
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @return bool
     */
    public function run()
    {
        set_error_handler(function ($type, $msg) {
            $info = $this->getInfo();
            $info['error'] = "$type: $msg";
            $this->setInfo($info)
                ->setFinishedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT))
                ->save();
        });

        $this->setStartedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT))
            ->save();

        $info = [
            'pages'     => [],
            'fill_rate' => $this->dataHelper->getFillRate() . '%',
        ];

        $ts = microtime(true);

        $filter = $this->getFilter();

        if (empty($filter)) {
            foreach ($this->warmer->warmCollection($this->getPageCollection()) as $page) {
                $info['pages'][] = $page->getId();
                $info['job_time'] = round(microtime(true) - $ts, 4);

                $this->setInfo($info)
                    ->save();
            }
        } elseif (isset($filter['url'])) {
            $this->warmer->warmUrl($filter['url']);
        }

        $info['job_time'] = round(microtime(true) - $ts, 4);
        $info['fill_rate'] .= ' - ' . $this->dataHelper->getFillRate() . '%';

        $this->setInfo($info)
            ->save();

        $this->setFinishedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT))
            ->save();

        restore_error_handler();

        return true;
    }

    /**
     * @return ResourceModel\Page\Collection
     */
    public function getPageCollection()
    {
        $collection = $this->pageCollectionFactory->create();
        $select = $collection->getSelect();

        if ($this->config->getWarmStrategy() == WarmStrategy::STRATEGY_POPULARITY) {
            $select->order('popularity desc');
        } else {
            $types = $this->config->getWarmByPageType();
            $types = array_map(function ($item) {
                return "'$item'";
            }, $types);

            $types = [0];
            $order = new \Zend_Db_Expr('FIELD(page_type, ' . implode(',', $types) . ') asc');
            $select->order($order);
        }

        return $collection;
    }
}