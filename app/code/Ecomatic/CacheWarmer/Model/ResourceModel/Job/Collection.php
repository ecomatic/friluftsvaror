<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Model\ResourceModel\Job;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\DB\Select;

class Collection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('Ecomatic\CacheWarmer\Model\Job', 'Ecomatic\CacheWarmer\Model\ResourceModel\Job');
    }

    /**
     * @return $this
     */
    public function runningJobs()
    {
        $this->addFieldToFilter('started_at', ['notnull' => true]);

        return $this;
    }
}
