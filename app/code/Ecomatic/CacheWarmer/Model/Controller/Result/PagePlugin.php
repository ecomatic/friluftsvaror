<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Model\Controller\Result;

use Magento\Framework\App\Response\Http as ResponseHttp;


class PagePlugin
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @param \Magento\Framework\App\Request\Http     $request
     * @param \Ecomatic\CacheWarmer\Model\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Ecomatic\CacheWarmer\Model\PageFactory $pageFactory
    ) {
        $this->request = $request;
        $this->pageFactory = $pageFactory;
    }

    /**
     * @param \Magento\Framework\Controller\ResultInterface $subject
     * @param \Closure                                      $proceed
     * @param ResponseHttp                                  $response
     * @return ResponseHttp
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundRenderResult(
        \Magento\Framework\Controller\ResultInterface $subject,
        \Closure $proceed,
        ResponseHttp $response
    ) {
        $this->pageFactory->create()->collect($this->request, $response);

        return $proceed($response);
    }
}
