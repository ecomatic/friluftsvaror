<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Ecomatic\CacheWarmer\Model\Config\Source\WarmStrategy;

class Config
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var array
     */
    protected $notCacheableBlocks = [];

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(
            'cache_warmer/general/enabled',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getWarmStrategy()
    {
        return $this->scopeConfig->getValue(
            'cache_warmer/general/warm_strategy',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return array
     */
    public function getWarmByPageType()
    {
        $data = unserialize($this->scopeConfig->getValue(
            'cache_warmer/general/warm_by_page_type',
            ScopeInterface::SCOPE_STORE
        ));

        $result = [];
        foreach ($data as $key => $info) {
            if (isset($info['is_active'])) {
                $result[] = $key;
            }
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getWarmThreads()
    {
        return $this->scopeConfig->getValue(
            'cache_warmer/performance/threads',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Delay in microseconds
     * @return int
     */
    public function getWarmDelay()
    {
        return $this->scopeConfig->getValue(
            'cache_warmer/performance/delay',
            ScopeInterface::SCOPE_STORE
        ) * 1000;
    }

    /**
     * @return int
     */
    public function getWarmTime()
    {
        return $this->scopeConfig->getValue(
            'cache_warmer/performance/job_time',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param Page $page
     * @return bool
     */
    public function isIgnoredPage($page)
    {
        if ($this->isIgnoredUri($page->getUri())) {
            return true;
        }

        if (in_array($page->getPageType(), ['cms_noroute_index', 'cms_noroute_index_*'])) {
            return true;
        }

        if ($this->getWarmStrategy() == WarmStrategy::STRATEGY_PAGE_TYPE) {
            if (!in_array($page->getPageType(), $this->getWarmByPageType())) {
                return true;
            }
        }

        return false;
    }


    /**
     * @param string $uri
     * @return bool
     */
    public function isIgnoredUri($uri)
    {
        foreach ($this->getIgnoredUriExpressions() as $expression) {
            if (@preg_match($expression, $uri)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getIgnoredUriExpressions()
    {
        $expressions = [];
        $config = $this->scopeConfig->getValue(
            'cache_warmer/general/ignored_uri_expressions',
            ScopeInterface::SCOPE_STORE
        );
        $config = unserialize($config);
        if (is_array($config)) {
            foreach ($config as $item) {
                $expressions[] = $item['expression'];
            }
        }

        return $expressions;
    }

    /**
     * @return string
     */
    public function getCacheType()
    {
        return $this->scopeConfig->getValue(\Magento\PageCache\Model\Config::XML_PAGECACHE_TYPE);
    }

    /**
     * @return int
     */
    public function getCacheTtl()
    {
        return $this->scopeConfig->getValue(\Magento\PageCache\Model\Config::XML_PAGECACHE_TTL);
    }

    /**
     * Retrieve whether SSL certificates are validated for requests sent over a HTTPS connection.
     *
     * @return integer
     */
    public function getVerifyPeer()
    {
        return $this->scopeConfig->getValue(
            'cache_warmer/extended/verify_peer',
            ScopeInterface::SCOPE_STORE
        );
    }

    //Debug

    /**
     * @return integer
     */
    public function isInfoBlockEnabled()
    {
        if (!$this->isDebugAllowed()) {
            return false;
        }

        return $this->scopeConfig->getValue(
            'cache_warmer/debug/info',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function isDebugAllowed()
    {
        $ips = $this->scopeConfig->getValue(
            'cache_warmer/debug/allowed_ip',
            ScopeInterface::SCOPE_STORE
        );

        if ($ips == '') {
            return true;
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $clientIp = $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $clientIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $clientIp = $_SERVER['REMOTE_ADDR'];
        }

        if (!$clientIp) {
            return false;
        }

        $ips = explode(',', $ips);
        $ips = array_map('trim', $ips);

        return in_array($clientIp, $ips);
    }

    /**
     * @param string $class
     * @param string $name
     * @return $this
     */
    public function storeNotCacheableBlock($class, $name)
    {
        $this->notCacheableBlocks[$class] = $name;

        return $this;
    }

    /**
     * @return array
     */
    public function getNotCacheableBlocks()
    {
        return $this->notCacheableBlocks;
    }
}
