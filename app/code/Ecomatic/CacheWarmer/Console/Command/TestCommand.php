<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Console\Command;

use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('ecomatic:cache-warmer:test')
            ->setDescription('Test cache warmer functionality');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \Ecomatic\CacheWarmer\Model\ResourceModel\Page\Collection $job */
        $collection = $this->objectManager->create('Ecomatic\CacheWarmer\Model\ResourceModel\Page\Collection');

        /** @var \Ecomatic\CacheWarmer\Model\Warmer $warmer */
        $warmer = $this->objectManager->create('Ecomatic\CacheWarmer\Model\Warmer');

        /** @var \Ecomatic\CacheWarmer\Model\Page $page */
        foreach ($collection as $page) {
            $status = $page->isCached() ? 'Cached' : 'Not cached';

            $warmer->warmPage($page);

            if ($page->isCached()) {
                $output->writeln("<comment>$status</comment> <info>WARMED</info> {$page->getUri()}");
            } else {
                $output->writeln("<comment>$status</comment> <error>NOT WARMED</error> {$page->getUri()}");
            }
        }
    }
}
