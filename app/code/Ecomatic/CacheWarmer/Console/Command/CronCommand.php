<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Console\Command;

use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\State;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;


class CronCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param State                  $appState
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        State $appState,
        ObjectManagerInterface $objectManager
    ) {
        $this->appState = $appState;
        $this->objectManager = $objectManager;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('ecomatic:cache-warmer:cron')
            ->setDescription('Run module cronjobs');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('Fill rate....');
        /** @var \Ecomatic\CacheWarmer\Cron\FillRate $fillRate */
        $fillRate = $this->objectManager->create('Ecomatic\CacheWarmer\Cron\FillRate');
        $fillRate->execute();
        $output->writeln('<info>done</info>');

        $output->write('Clean....');
        /** @var \Ecomatic\CacheWarmer\Cron\Clean $cleanJob */
        $cleanJob = $this->objectManager->create('Ecomatic\CacheWarmer\Cron\Clean');
        $cleanJob->execute();
        $output->writeln('<info>done</info>');

        $output->write('Schedule....');
        /** @var \Ecomatic\CacheWarmer\Cron\ScheduleJob $cronClean */
        $scheduleJob = $this->objectManager->create('Ecomatic\CacheWarmer\Cron\ScheduleJob');
        $scheduleJob->execute();
        $output->writeln('<info>done</info>');

        $output->write('Run....');
        /** @var \Ecomatic\CacheWarmer\Cron\ScheduleJob $runJob */
        $runJob = $this->objectManager->create('Ecomatic\CacheWarmer\Cron\RunJob');
        $runJob->execute();
        $output->writeln('<info>done</info>');
    }
}
