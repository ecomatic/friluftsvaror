<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Console\Command;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\State;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Store\Model\StoreFactory;

class CrawlCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Ecomatic\CacheWarmer\Model\Config
     */
    protected $config;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var StoreFactory
     */
    protected $storeFactory;

    /**
     * @var array
     */
    protected $pool = [];

    public function __construct(
        \Ecomatic\CacheWarmer\Model\Config $config,
        State $appState,
        ObjectManagerInterface $objectManager,
        StoreFactory $storeFactory
    ) {
        $this->config = $config;
        $this->appState = $appState;
        $this->objectManager = $objectManager;
        $this->storeFactory = $storeFactory;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('ecomatic:cache-warmer:crawl')
            ->setDescription('Crawl all pages');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode('frontend');
        $store = $this->storeFactory->create()->load(1);
        $baseUrl = rtrim($store->getBaseUrl(), '/');
        $isNotVerifyPeer = (integer)$this->config->getVerifyPeer();
        if ($isNotVerifyPeer) {
            $baseUrl = str_replace('http:', 'https:', $baseUrl);
        }

        $this->pool[$baseUrl] = 1;
        $idx = 1;

        while (true) {
            foreach ($this->pool as $url => $depth) {

                if ($depth == 0) {
                    continue;
                }

                $cnt = count($this->pool);
                $output->writeln("$idx/$cnt <info>$url</info> $depth");

                $this->pool[$url] = 0;

                if ($isNotVerifyPeer) {
                    $contextOptions = ["ssl" => [
                        "verify_peer"      => false,
                        "verify_peer_name" => false,
                    ],
                    ];
                    $url = str_replace('http:', 'https:', $url);

                    $content = @file_get_contents($url, false, stream_context_create($contextOptions));
                } else {
                    $content = @file_get_contents($url);
                }

                $dom = new \DOMDocument;

                @$dom->loadHTML($content);

                $links = $dom->getElementsByTagName('a');

                foreach ($links as $link) {
                    $href = $link->getAttribute('href');
                    if (strpos($href, $baseUrl) !== false) {
                        $this->pool[$href] = $depth + 1;
                    }
                }

                $idx++;
            }
        }
    }
}
