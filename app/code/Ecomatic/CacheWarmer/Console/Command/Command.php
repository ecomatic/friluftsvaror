<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Console\Command;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable\OptionValue;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\State;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Ecomatic\CacheWarmer\Model\Job;
use Ecomatic\CacheWarmer\Model\JobFactory;

class Command extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var JobFactory
     */
    protected $jobFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param JobFactory             $jobFactory
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        JobFactory $jobFactory,
        ObjectManagerInterface $objectManager
    ) {
        $this->jobFactory = $jobFactory;
        $this->objectManager = $objectManager;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('ecomatic:cache-warmer')
            ->setDescription('Warm cache');

        $this->addOption('warm', null, null, 'Run warmer job');
        $this->addOption('remove-all-pages', null, null, 'Remove all pages');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('warm')) {
            $job = $this->jobFactory->create()
                ->setPriority(Job::PRIORITY_NORMAL)
                ->setFilter([])
                ->save();

            $job->run();

            $output->writeln('done');
        }

        if ($input->getOption('remove-all-pages')) {
            /** @var \Ecomatic\CacheWarmer\Model\ResourceModel\Page\Collection $collection */
            $collection = $this->objectManager->get('Ecomatic\CacheWarmer\Model\ResourceModel\Page\Collection');
            foreach ($collection as $page) {
                $page->delete();
            }

            $output->writeln('done');
        }
    }
}
