<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.0-alpha15
 * @copyright Copyright (C) 2016 Ecomatic (https://ecomatic.com/)
 */

if (isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT'])
    && $_SERVER['HTTP_USER_AGENT'] == \Ecomatic\CacheWarmer\Model\Warmer::STATUS_USER_AGENT
) {
    header('HTTP/1.1 500 Internal Server Error');
    echo '*';
    exit;
}

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Ecomatic_CacheWarmer',
    __DIR__
);
