<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\App\Emulation as AppEmulation;
use Magento\Framework\App\Area;
use Ecomatic\CacheWarmer\Model\JobFactory;

class OnModelSaveAfter implements ObserverInterface
{
    /**
     * @var AppEmulation
     */
    private $appEmulation;

    /**
     * @var JobFactory
     */
    private $jobFactory;

    public function __construct(
        AppEmulation $appEmulation,
        JobFactory $jobFactory
    ) {
        $this->appEmulation = $appEmulation;
        $this->jobFactory = $jobFactory;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $object = $observer->getEvent()->getData('object');
        if ($object && is_object($object)) {
            if ($object instanceof \Magento\Catalog\Model\Product) {
                foreach ($object->getStoreIds() as $storeId) {
                    if ($storeId == 0) {
                        continue;
                    }

                    $this->appEmulation->startEnvironmentEmulation($storeId, Area::AREA_FRONTEND, false);

                    $object->setStoreId($storeId);
                    $this->scheduleUrl($object->getProductUrl());

                    $this->appEmulation->stopEnvironmentEmulation();
                }
            } elseif ($object instanceof \Magento\Catalog\Model\Category) {
                foreach ($object->getStoreIds() as $storeId) {
                    if ($storeId == 0) {
                        continue;
                    }

                    $this->appEmulation->startEnvironmentEmulation($storeId, Area::AREA_FRONTEND, false);

                    $object->setStoreId($storeId);
                    $this->scheduleUrl($object->getUrl());

                    $this->appEmulation->stopEnvironmentEmulation();
                }
            }
        }
    }

    /**
     * @param string $url
     * @return void
     */
    public function scheduleUrl($url)
    {
        $job = $this->jobFactory->create();
        $job->setData('filter', ['url' => $url]);
        $job->save();
    }
}
