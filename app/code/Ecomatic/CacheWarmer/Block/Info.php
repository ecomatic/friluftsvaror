<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Info extends Template
{
    /**
     * @var string
     */
    protected $_template = 'Ecomatic_CacheWarmer::info.phtml';

    /**
     * @var \Ecomatic\CacheWarmer\Model\Config
     */
    protected $config;

    /**
     * @var Context
     */
    protected $context;

    public function __construct(
        \Ecomatic\CacheWarmer\Model\Config $config,
        Context $context,
        array $data = []
    ) {
        $this->config = $config;
        $this->context = $context;

        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getCacheStatus()
    {
        $hit = $this->getData('hit');
        $infoText = ($hit) ? 'hit' : 'miss';

        return $infoText;
    }

    /**
     * @return array
     */
    public function getNotCacheableBlocks()
    {
        return $this->config->getNotCacheableBlocks();
    }
}