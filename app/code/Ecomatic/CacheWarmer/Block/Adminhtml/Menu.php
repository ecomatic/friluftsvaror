<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Block\Adminhtml;

use Magento\Framework\DataObject;
use Magento\Backend\Block\Template\Context;
use Ecomatic\Core\Block\Adminhtml\AbstractMenu;

class Menu extends AbstractMenu
{
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        $this->visibleAt(['cache_warmer']);

        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function buildMenu()
    {
        $this->addItem([
            'resource' => 'Ecomatic_CacheWarmer::cache_warmer_page',
            'title'    => __('Pages'),
            'url'      => $this->urlBuilder->getUrl('cache_warmer/page'),
        ])->addItem([
            'resource' => 'Ecomatic_CacheWarmer::cache_warmer_job',
            'title'    => __('Jobs'),
            'url'      => $this->urlBuilder->getUrl('cache_warmer/job'),
        ]);

        $this->addSeparator();

        $this->addItem([
            'resource' => 'Ecomatic_CacheWarmer::cache_warmer_config',
            'title'    => __('Settings'),
            'url'      => $this->urlBuilder->getUrl('adminhtml/system_config/edit/section/cache_warmer'),
        ]);

        return $this;
    }
}
