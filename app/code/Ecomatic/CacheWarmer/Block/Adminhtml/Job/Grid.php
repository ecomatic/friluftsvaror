<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Block\Adminhtml\Job;

use Magento\Backend\Block\Widget\Grid\Extended as GridExtended;
use Ecomatic\CacheWarmer\Model\ResourceModel\Job\CollectionFactory as JobCollectionFactory;
use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Helper\Data as BackendHelper;

class Grid extends GridExtended
{
    /**
     * @var JobCollectionFactory
     */
    protected $jobCollectionFactory;

    /**
     * @param JobCollectionFactory $jobCollectionFactory
     * @param Context              $context
     * @param BackendHelper        $backendHelper
     */
    public function __construct(
        JobCollectionFactory $jobCollectionFactory,
        Context $context,
        BackendHelper $backendHelper
    ) {
        $this->jobCollectionFactory = $jobCollectionFactory;

        parent::__construct($context, $backendHelper);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('cache_warmer_job_grid');
        $this->setDefaultSort('job_id');
        $this->setDefaultDir('DESC');

        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->jobCollectionFactory->create();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('job_id', [
            'header' => __('#'),
            'type'   => 'number',
            'index'  => 'job_id'
        ]);

        $this->addColumn('status', [
            'header'   => __('Status'),
            'index'    => 'job_id',
            'renderer' => 'Ecomatic\CacheWarmer\Block\Adminhtml\Job\Grid\Renderer\Status',
            'filter'   => false,
            'sortable' => false,
        ]);

        $this->addColumn('created_at', [
            'header' => __('Created At'),
            'type'   => 'datetime',
            'index'  => 'created_at'
        ]);

        $this->addColumn('started_at', [
            'header' => __('Started At'),
            'type'   => 'datetime',
            'index'  => 'started_at'
        ]);

        $this->addColumn('finished_at', [
            'header' => __('Finished At'),
            'type'   => 'datetime',
            'index'  => 'finished_at'
        ]);

        $this->addColumn('info', [
            'header'   => __('Information'),
            'index'    => 'info',
            'renderer' => 'Ecomatic\CacheWarmer\Block\Adminhtml\Job\Grid\Renderer\Info',
            'filter'   => false,
            'sortable' => false,
        ]);

        return parent::_prepareColumns();
    }
}
