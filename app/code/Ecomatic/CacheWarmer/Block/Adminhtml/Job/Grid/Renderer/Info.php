<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Block\Adminhtml\Job\Grid\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class Info extends AbstractRenderer
{
    /**
     * {@inheritdoc}
     */
    public function render(DataObject $row)
    {
        /** @var \Ecomatic\CacheWarmer\Model\Job $row */
        $info = $row->getInfo();
        $filter = $row->getFilter();
        $data = [];

        if (isset($info['job_time'])) {
            $data['Warmed pages'] = count($info['pages']);
            $data['Execution time'] = round($info['job_time'], 1).' s.';
        }

        if (isset($info['error'])) {
            $data['Error'] = $info['error'];
        }

        if (isset($filter['url'])) {
            $data['Scheduled'] = $filter['url'];
        }

        if (isset($info['fill_rate'])) {
            $data['Fill Rate (before - after)'] = $info['fill_rate'];
        }

        return $this->arrayToTable($data);
    }

    protected function arrayToTable($data)
    {
        $html = '<table>';

        foreach ($data as $key => $value) {
            if ($value) {
                $html .= '<tr>';
                $html .= '<td>' . $key . '</td>';
                $html .= '<td>' . $value . '</td>';
                $html .= '</tr> ';
            }
        }
        $html .= '</table > ';

        return $html;
    }
}