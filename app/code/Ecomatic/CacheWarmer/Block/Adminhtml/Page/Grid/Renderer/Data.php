<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Block\Adminhtml\Page\Grid\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class Data extends AbstractRenderer
{
    /**
     * {@inheritdoc}
     */
    public function render(DataObject $row)
    {
        /** @var \Ecomatic\CacheWarmer\Model\Page $row */
        $data = $row->getVaryData();
        $data['product_id'] = $row->getProductId();
        $data['category_id'] = $row->getCategoryId();

        return $this->arrayToTable($data);
    }

    protected function arrayToTable($data)
    {
        $html = '<table>';

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = implode(', ', $value);
            }
            
            if ($value) {
                $html .= '<tr>';
                $html .= '<td>' . $key . '</td>';
                $html .= '<td>' . $value . '</td>';
                $html .= '</tr> ';
            }
        }
        $html .= '</table > ';

        return $html;
    }
}