<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Block\Adminhtml\Page\Grid\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class Status extends AbstractRenderer
{
    /**
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        /** @var \Ecomatic\CacheWarmer\Model\Page $row */
        if ($row->getCacheId()) {
            if ($row->isCached()) {
                return '<div class="cache-warmer__status in-cache">' . __('In Cache') . '</div>';
            }
        } else {
            return '<div class="cache-warmer__status unknown">' . __('Unknown') . '</div>';
        }

        return '<div class="cache-warmer__status pending">' . __('Pending') . '</div>';
    }
}