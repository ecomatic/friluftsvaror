<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\CacheWarmer\Block\Adminhtml\Page;

use Magento\Backend\Block\Widget\Grid\Extended as GridExtended;
use Ecomatic\CacheWarmer\Model\ResourceModel\Page\CollectionFactory as PageCollectionFactory;
use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Helper\Data as BackendHelper;
use Ecomatic\CacheWarmer\Model\Config\Source\PageType as PageTypeSource;

class Grid extends GridExtended
{
    /**
     * @var PageCollectionFactory
     */
    protected $pageCollectionFactory;

    /**
     * @var PageTypeSource
     */
    protected $pageTypeSource;

    /**
     * @param PageCollectionFactory $jobCollectionFactory
     * @param PageTypeSource        $pageTypeSource
     * @param Context               $context
     * @param BackendHelper         $backendHelper
     */
    public function __construct(
        PageCollectionFactory $jobCollectionFactory,
        PageTypeSource $pageTypeSource,
        Context $context,
        BackendHelper $backendHelper
    ) {
        $this->pageCollectionFactory = $jobCollectionFactory;
        $this->pageTypeSource = $pageTypeSource;

        parent::__construct($context, $backendHelper);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('cache_warmer_page_grid');
        $this->setDefaultSort('popularity');
        $this->setDefaultDir('DESC');
        $this->setVarNamePage('p');

        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->pageCollectionFactory->create();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('page_id', [
            'header' => __('#'),
            'type'   => 'number',
            'index'  => 'page_id',
        ]);

        $this->addColumn('status', [
            'header'   => __('Status'),
            'index'    => 'page_id',
            'renderer' => 'Ecomatic\CacheWarmer\Block\Adminhtml\Page\Grid\Renderer\Status',
            'filter'   => false,
            'sortable' => false,
        ]);

        $this->addColumn('uri', [
            'header' => __('URI'),
            'index'  => 'uri',
        ]);

        $this->addColumn('page_type', [
            'header'  => __('Page Type'),
            'index'   => 'page_type',
            'type'    => 'options',
            'options' => $this->pageTypeSource->toOptionArray(),
        ]);

        $this->addColumn('popularity', [
            'type'         => 'number',
            'header'       => __('Popularity'),
            'index'        => 'popularity',
            'filter_index' => 'popularity',
        ]);

        $this->addColumn('vary_data', [
            'header'   => __('Data'),
            'index'    => 'vary_data',
            'renderer' => 'Ecomatic\CacheWarmer\Block\Adminhtml\Page\Grid\Renderer\Data',
        ]);

        $this->addColumn('action', [
            'header'    => __('Action'),
            'width'     => '100',
            'type'      => 'action',
            'getter'    => 'getId',
            'actions'   => [
                [
                    'caption' => __('Warm Cache'),
                    'url'     => ['base' => '*/*/warm'],
                    'field'   => 'id',
                ],
                [
                    'caption' => __('Clean Cache'),
                    'url'     => ['base' => '*/*/clean'],
                    'field'   => 'id',
                ],
                [
                    'caption' => __('Remove'),
                    'url'     => ['base' => '*/*/delete'],
                    'field'   => 'id',
                    'confirm' => __('Are you sure?'),
                ],
            ],
            'filter'    => false,
            'sortable'  => false,
            'is_system' => true,
        ]);

        return parent::_prepareColumns();
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('page_id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem('warm', [
            'label'   => __('Warm Cache'),
            'url'     => $this->getUrl('*/*/warm'),
            'confirm' => __('Are you sure?'),
        ]);

        $this->getMassactionBlock()->addItem('clean', [
            'label'   => __('Clean Cache'),
            'url'     => $this->getUrl('*/*/clean'),
            'confirm' => __('Are you sure?'),
        ]);

        $this->getMassactionBlock()->addItem('delete', [
            'label'   => __('Remove'),
            'url'     => $this->getUrl('*/*/delete'),
            'confirm' => __('Are you sure?'),
        ]);

        return $this;
    }
}
