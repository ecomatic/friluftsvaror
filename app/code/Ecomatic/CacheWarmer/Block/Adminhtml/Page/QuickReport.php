<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Block\Adminhtml\Page;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Ecomatic\CacheWarmer\Model\Config;
use Ecomatic\CacheWarmer\Helper\Data as DataHelper;
use Magento\Variable\Model\VariableFactory;

class QuickReport extends Template
{
    /**
     * @var string
     */
    protected $_template = 'Ecomatic_CacheWarmer::page/quick_report.phtml';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var DataHelper
     */
    private $dataHelper;

    /**
     * @var VariableFactory
     */
    private $variableFactory;

    public function __construct(
        DataHelper $dataHelper,
        Config $config,
        VariableFactory $variableFactory,
        Context $context
    ) {
        $this->dataHelper = $dataHelper;
        $this->config = $config;
        $this->variableFactory = $variableFactory;

        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getReport()
    {
        return $this->dataHelper->getFillRates();
    }

    /**
     * @return string
     */
    public function getCacheType()
    {
        return $this->config->getCacheType();
    }

    /**
     * @return int
     */
    public function getCacheTtl()
    {
        return $this->config->getCacheTtl();
    }

    /**
     * @return array
     */
    public function getFillRates()
    {
        $variable = $this->variableFactory->create()
            ->loadByCode('cache_warmer_fill_rate');

        $value = $variable->getValue();

        if ($value) {
            $value = \Zend_Json::decode($value);
        } else {
            $value = [];
        }

        return $value;
    }
}