<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Block\Adminhtml\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\DataObject;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Ecomatic\CacheWarmer\Model\Config\Source\PageType as PageTypeSource;

/**
 * @method AbstractElement getElement()
 * @method $this setElement(AbstractElement $element)
 */
class PageType extends Field
{
    /**
     * @var PageTypeSource
     */
    protected $pageTypeSource;

    /**
     * @param PageTypeSource $jobTypeSource
     * @param Context        $context
     */
    public function __construct(
        PageTypeSource $jobTypeSource,
        Context $context
    ) {
        $this->pageTypeSource = $jobTypeSource;
        return parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->setTemplate('Ecomatic_CacheWarmer::config/form/field/page_type.phtml');
    }

    /**
     * {@inheritdoc}
     */
    public function render(AbstractElement $element)
    {
        $this->setElement($element);

        return $this->_toHtml();
    }

    /**
     * @return DataObject[]
     */
    public function getPageTypes()
    {
        $result = [];

        $types = $this->pageTypeSource->toOptionArray();

        foreach ($types as $key => $label) {
            if (!$key) {
                continue;
            }

            $type = new DataObject();
            $type->addData([
                'code'       => $key,
                'label'      => $label,
                'is_active'  => $this->getValue($key, 'is_active'),
                'importance' => $this->getValue($key, 'importance'),
                'order'      => $this->getValue($key, 'order') ? $this->getValue($key, 'order') : 1000,
            ]);

            $result[] = $type;
        }

        usort($result, function ($a, $b) {
            return $a->getData('order') - $b->getData('order');
        });

        return $result;
    }

    /**
     * @param DataObject $type
     * @return string
     */
    public function getNamePrefix($type)
    {
        return $this->getElement()->getName() . '[' . $type->getData('code') . ']';
    }

    /**
     * @param string $key
     * @param string $option
     * @return string
     */
    public function getValue($key, $option)
    {
        if ($this->getElement()->getData('value') && is_array($this->getElement()->getData('value'))) {
            $values = $this->getElement()->getData('value');
            if (isset($values[$key]) && isset($values[$key][$option])) {
                return $values[$key][$option];
            }
        }

        return false;
    }
}
