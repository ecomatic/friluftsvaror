<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Helper;

use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Cache\StateInterface as CacheStateInterface;
use Ecomatic\Core\Helper\Cron as CronHelper;
use Ecomatic\CacheWarmer\Model\ResourceModel\Page\CollectionFactory as PageCollectionFactory;

class Data extends AbstractHelper
{
    /**
     * @var CacheStateInterface
     */
    private $cacheState;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var CronHelper
     */
    private $cronHelper;

    /**
     * @var PageCollectionFactory
     */
    private $pageCollectionFactory;

    public function __construct(
        CacheStateInterface $cacheState,
        CacheInterface $cache,
        CronHelper $cronHelper,
        PageCollectionFactory $pageCollectionFactory,
        Context $context
    ) {
        $this->cacheState = $cacheState;
        $this->cache = $cache;
        $this->cronHelper = $cronHelper;
        $this->pageCollectionFactory = $pageCollectionFactory;

        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function isPageCacheEnabled()
    {
        return $this->cacheState->isEnabled(\Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER);
    }

    /**
     * @return bool
     */
    public function checkCronStatus()
    {
        return $this->cronHelper->checkCronStatus(null)[0];
    }

    /**
     * @return \Ecomatic\CacheWarmer\Model\ResourceModel\Page\Collection
     */
    private function getIndicatingPages()
    {
        $identifier = 'cache_warmer_indicating_ids';

        $ids = $this->cache->load($identifier);

        if ($ids) {
            $ids = \Zend_Json::decode($ids);
        } else {
            $collection = $this->pageCollectionFactory->create();
            $collection->getSelect()
                ->limit(2000)
                ->orderRand();

            $ids = $collection->getColumnValues('page_id');

            $this->cache->save(\Zend_Json::encode($ids), $identifier, [], 600);
        }

        $ids[] = 0;

        $collection = $this->pageCollectionFactory->create();
        $collection->getSelect()->where('page_id IN(' . implode(',', $ids) . ')');

        return $collection;
    }

    /**
     * @return array
     */
    public function getFillRates()
    {
        $result = [
            'in_cache' => 0,
            'pending'  => 0,
            'unknown'  => 0,
            'total'    => 0,
        ];


        $collection = $this->getIndicatingPages();

        $ts = null;
        /** @var \Ecomatic\CacheWarmer\Model\Page $page */
        while ($page = $collection->fetchItem()) {
            if ($ts == null) {
                $ts = microtime(true);
            }

            if ($page->getCacheId()) {
                if ($page->isCached()) {
                    $result['in_cache']++;
                } else {
                    $result['pending']++;
                }
            } else {
                $result['unknown']++;
            }

            $result['total']++;

            if (microtime(true) - $ts > 1 && $result['total'] > 20) {
                break;
            }
        }

        if ($result['total'] == 0) {
            $result['total'] = 1;
        }

        return $result;
    }

    /**
     * @return float
     */
    public function getFillRate()
    {
        $rates = $this->getFillRates();

        return round(($rates['in_cache'] / $rates['total']) * 100, 2);
    }
}