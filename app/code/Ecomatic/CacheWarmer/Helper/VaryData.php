<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Ecomatic\CacheWarmer\Model\Warmer;
use Magento\Framework\HTTP\Header as HttpHeader;

class VaryData extends AbstractHelper
{
    const WARMER_UNIQUE_VALUE = 'cache_warmer/unique_value';

    /**
     * @var HttpHeader
     */
    protected $httpHeader;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $resourceConfig;

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $cacheTypeList;

    /**
     * @var null|string $userAgentFirstPart
     */
    protected static $userAgentFirstPart = null;

    public function __construct(
        Context $context,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
    ) {
        $this->httpHeader = $context->getHttpHeader();
        $this->scopeConfig = $context->getScopeConfig();
        $this->resourceConfig = $resourceConfig;
        $this->cacheTypeList = $cacheTypeList;

        parent::__construct($context);
    }

    /**
     * @param array $varyData
     * @return string
     */
    public function getUserAgent($varyData)
    {
        $agent = $this->getUserAgentFirstPart();
        $agent .= base64_encode(serialize($varyData));

        return $agent;
    }

    /**
     * @return bool|array
     */
    public function getVaryData()
    {
        $agent = $this->httpHeader->getHttpUserAgent();

        if (strpos($agent, $this->getUserAgentFirstPart()) !== false) {
            $data = str_replace($this->getUserAgentFirstPart(), '', $agent);
            $data = unserialize(base64_decode($data));

            return $data;
        }

        return false;
    }

    /**
     * @return null|string
     */
    public function getUserAgentFirstPart()
    {
        if (self::$userAgentFirstPart === null) {
            self::$userAgentFirstPart = Warmer::USER_AGENT . $this->getWarmerUniquePart() . ':';
        }

        return self::$userAgentFirstPart;
    }

    /**
     * @return string
     */
    public function getWarmerUniquePart()
    {
        if ($uniqueValue = $this->scopeConfig->getValue(self::WARMER_UNIQUE_VALUE)) {
            return $uniqueValue;
        }

        $uniqueValue = uniqid();
            $this->resourceConfig->saveConfig(
                self::WARMER_UNIQUE_VALUE,
                $uniqueValue,
                'default',
                0
            );
        //need apply unique part
        $this->cacheTypeList->cleanType('config');

        return $uniqueValue;
    }
}