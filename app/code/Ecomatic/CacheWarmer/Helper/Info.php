<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-cache-warmer
 * @version   1.0.19
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\CacheWarmer\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context as Context;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Framework\View\LayoutInterface;

class Info extends AbstractHelper
{
    /**
     * @var \Ecomatic\CacheWarmer\Model\Config
     */
    protected $config;

    /**
     * @var \Magento\PageCache\Model\Config
     */
    protected $pageCacheConfig;

    /**
     * @var \Magento\Framework\View\Layout
     */
    protected $layout;

    /**
     * Constructor
     *
     * @param \Ecomatic\CacheWarmer\Model\Config $config
     * @param \Magento\PageCache\Model\Config    $pageCacheConfig
     * @param Context                            $context
     * @param TemplateContext                    $templateContext
     */
    public function __construct(
        \Ecomatic\CacheWarmer\Model\Config $config,
        \Magento\PageCache\Model\Config $pageCacheConfig,
        Context $context,
        TemplateContext $templateContext
    ) {
        $this->config = $config;
        $this->pageCacheConfig = $pageCacheConfig;
        $this->layout = $templateContext->getLayout();

        parent::__construct($context);
    }

    /**
     * @param string &$result
     * @param bool   $hit
     * @return void
     */
    public function addInfoBlock(&$result, $hit = true)
    {
        if ($this->config->isInfoBlockEnabled()) {
            $body = $result->getBody();
            if ($this->isJson($body)) {
                return;
            }

            $info = $this->getInfo($hit);
            $body = $body . $info;
            $result->setBody($body);
        }
    }

    /**
     * @param bool $hit
     * @return string
     */
    public function getInfo($hit)
    {
        $info = $this->layout
            ->createBlock('Ecomatic\CacheWarmer\Block\Info')
            ->setData('hit', $hit);

        return $info->toHtml();
    }

    /**
     * @param string $string
     * @return bool
     */
    public function isJson($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }
}