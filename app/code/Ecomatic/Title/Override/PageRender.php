<?php
namespace Ecomatic\Title\Override;

class PageRender extends \Magento\Framework\View\Page\Config\Renderer {
    /**
    * @return string
    */
    public function renderHeadContent()
    {
        $result = '';
        $result .= $this->renderTitle();
        $result .= $this->renderMetadata();
        $this->prepareFavicon();
        $result .= $this->renderAssets($this->getAvailableResultGroups());
        $result .= $this->pageConfig->getIncludes();
        return $result;
    }
}
