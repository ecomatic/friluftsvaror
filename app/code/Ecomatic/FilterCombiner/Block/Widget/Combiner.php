<?php


namespace Ecomatic\FilterCombiner\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;

class Combiner extends Template implements BlockInterface {
	/**
	 * @param Context $context
	 */
	public function __construct( Context $context ) {
		parent::__construct( $context );
	}
}
