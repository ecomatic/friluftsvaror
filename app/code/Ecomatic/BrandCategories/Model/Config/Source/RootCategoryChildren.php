<?php

namespace Ecomatic\BrandCategories\Model\Config\Source;

class RootCategoryChildren implements \Magento\Framework\Option\ArrayInterface
{
	
	public function toOptionArray() {
		$options = [];
		foreach($this->getRootChildren() as $category) {
			$options[] = ['value' => $category->getId(), 'label' => $category->getName()];
		}
		return $options;
	}
	
	public function toArray() {
		$options = [];
		foreach($this->getRootChildren() as $category) {
			$options[$category->getId()] = $category->getName();
		}
		return $options;
	}
	
	public function getRootChildren() {		
		$children = \Magento\Framework\App\ObjectManager::getInstance()
			->create('Magento\Catalog\Model\Category')
			->load(\Magento\Catalog\Model\Category::TREE_ROOT_ID)
			->getChildrenCategories();
		//TODO: Find another way to get store root category
		foreach($children as $child) {
			return $child->getChildrenCategories();
		}
		
		return [];
	}
}