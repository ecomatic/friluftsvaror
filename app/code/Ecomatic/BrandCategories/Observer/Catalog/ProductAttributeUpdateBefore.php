<?php


namespace Ecomatic\BrandCategories\Observer\Catalog;

class ProductAttributeUpdateBefore implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {	
		$product = $observer->getProduct();
		$brand = $product->getAttributeText('manufacturer');
		
		if(strlen($brand) == 0) {
			file_put_contents("test.txt", "No brand! done\n", FILE_APPEND);
			return false;
		}
		
		$om = \Magento\Framework\App\ObjectManager::getInstance();
		
		$brandsCatId = $om->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('brands_categories_section/brands_categpories_group/brands_root_category');
		$brandsCategory = $om->create('Magento\Catalog\Model\Category')->load($brandsCatId);
		$newBrandCategory = $om->create('Magento\Catalog\Model\Category');
		
		//Check exist category
		$cate = $newBrandCategory->getCollection()
					->addAttributeToFilter('name', $brand)
					->getFirstItem();

		if($cate->getId() == null) {
			$newBrandCategory->setPath($brandsCategory->getPath())
				->setParentId($brandsCatId)
				->setName($brand)
				->setIsActive(true);
			$newBrandCategory->save();
			$cate = $newBrandCategory;
		}
		
		$prodCategoryIds = $product->getCategoryIds();
		$newCategoryIds = [];
		foreach($prodCategoryIds as $pcId) {
			if($om->create('Magento\Catalog\Model\Category')->load($pcId)->getParentId() !== $brandsCatId) {
				$newCategoryIds[] = $pcId;
			}
		}
		$newCategoryIds[] = $cate->getId();
		
		file_put_contents("test.txt", "newCategoryIds dump:\n", FILE_APPEND);
		foreach($newCategoryIds as $id) {
			file_put_contents("test.txt", $id . ' ', FILE_APPEND);
		}
		file_put_contents("test.txt", "\n", FILE_APPEND);
		
		
		$om->get('Magento\Catalog\Api\CategoryLinkManagementInterface')->assignProductToCategories(
			$product->getSku(),
			$newCategoryIds
		);
		
		file_put_contents("test.txt", "Observer done\n", FILE_APPEND);
    }
}