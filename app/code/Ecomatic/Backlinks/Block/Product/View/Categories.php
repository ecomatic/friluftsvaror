<?php

namespace Ecomatic\Backlinks\Block\Product\View;

use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Catalog\Block\Product\Context;

class Categories extends AbstractProduct
{

    public function __construct(Context $context,
                                array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getCategories()
    {
        $product = $this->getProduct();
        $cats = $product->getCategoryCollection()
            ->addAttributeToSelect("name")
            ->addIsActiveFilter();
        return $cats;
    }
}
?>