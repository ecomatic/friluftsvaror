<?php
/**
 * Loading csv and generating url rewrites
 * Copyright (C) 2017 Ecomatic
 * 
 * This file is part of Ecomatic/AddUrlRewrites.
 * 
 * Ecomatic/AddUrlRewrites is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ecomatic\AddUrlRewrites\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
	protected $urlRewriteFactory;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\UrlRewrite\Model\ResourceModel\UrlRewriteFactory $_urlRewriteFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
		$this->urlRewriteFactory = $_urlRewriteFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute(){
		$file = file_get_contents(__DIR__ . "/test.csv");
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$csv = explode("\n", file_get_contents(__DIR__ . "/test.csv"));
		foreach ($csv as $key => $line){
			$csv[$key] = str_getcsv($line);
		}
		foreach ($csv as $tmp){
			try {
				$model = $objectManager->create(\Magento\UrlRewrite\Model\UrlRewrite::class);
				$model->load(0);
				$model->setEntityType('custom')
					->setRequestPath($tmp[0])
					->setTargetPath($tmp[1])
					->setRedirectType("301")
					->setStoreId(1)
					->setDescription("");
				$model->save();
			}
			catch (\Exception $e){}
		}
        return $this->resultPageFactory->create();
    }
}