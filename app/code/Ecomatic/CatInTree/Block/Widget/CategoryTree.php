<?php


namespace Ecomatic\CatInTree\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class CategoryTree extends Template implements BlockInterface
{

    protected $_template = "widget/CategoryTree.phtml";
	protected $_categoryHelper;
    protected $_objectManager;
	protected $_category;
	
	/**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Category $categoryHelper
    )
    {
        $this->_categoryHelper = $categoryHelper;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$this->_category = $this->_objectManager->get('Magento\Framework\Registry')->registry('current_category');
		
        parent::__construct($context);
    }
	
	public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)
    {
        return $this->_categoryHelper->getStoreCategories($sorted, $asCollection, $toLoad);
    }
	
	public function getCategoryName(){
		return $this->_category->getName();
	}
	
	public function buildBranch($category = null) {
		
		$out = '';
		if($category == null) {
			$category = array_values($this->_category->getParentCategories())[0];
		}
		$category = $this->_objectManager->get('\Magento\Catalog\Model\Category')->load($this->_category->getId());
	//	$out .= '<button class="btn-expander">-</button>';
		if($category->hasChildren()) {
			$children = $category->getChildrenCategories();
			$out .= '<div class="cat-items">';
			foreach($children as $c) {
				$out .= '<p class="cat-item"><a href="';
				$out .= $this->_categoryHelper->getCategoryUrl($c);
				$out .= '" class="';
				$out .= $c->getId() == $this->_category->getId() ? 'current' : '';
				$out .= '">';
				//$out .= $c->getName() . ' (' . $c->getProductCount() . ')';
				$out .= $c->getName();
				$out .= '</a></p>';
			}
			$out .= '</div>';
		}
		else {
			$cats = $category->getParentCategory();
			if (is_array($cats)){
				file_put_contents("test", "cats is array\n", FILE_APPEND);
				$cats = $cats[$category->getId()];
			}
			
			ob_start();
			var_dump($cats->hasChildren());
			echo "\n" . get_class($cats) . " " . $cats->getId() . " " . get_class($this->_category) . " " . $this->_category->getId() . "\n";
			file_put_contents("test", "categories has children: " . ob_get_clean() . "\n", FILE_APPEND);
			$cats = $cats->getChildrenCategories();
			$out .= '<div class="cat-items">';
			foreach ($cats as $cat){
				$out .= '<p class="cat-item"><a href="';
				$out .= $this->_categoryHelper->getCategoryUrl($cat);
				$out .= '" class="';
				$out .= $cat->getId() == $this->_category->getId() ? 'current' : '';
				$out .= '">';
			//	$out .= $cat->getName() . ' (' . $cat->getProductCount() . ')';
			   $out .= $cat->getName();
				$out .= '</a></p>';
			}
			$out .= '</div>';
		}
		
		return $out;
	}

}
