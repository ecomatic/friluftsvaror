<?php
namespace Ecomatic\Tableratemaxweight\Model\Carrier;
class Tablerate extends \Magento\OfflineShipping\Model\Carrier\Tablerate {
	
	public function proccessAdditionalValidation(\Magento\Framework\DataObject $request){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
		$items = $cart->getQuote()->getAllItems();

		$weight = 0;
		foreach($items as $item) {
			$weight += ($item->getWeight() * $item->getQty()) ;        
		}
		$max = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface')->getValue('carriers/tablerate/maxweight', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		file_put_contents("var/log/dev.log", $max . "\n" . $weight . "\n\n\n", FILE_APPEND);
		if ($weight < $max){
			return $this;
		}
		else {
			return false;
		}
    }
}