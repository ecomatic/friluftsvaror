<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-core
 * @version   1.2.24
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\Core\Controller\Lc;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\ModuleList;

class Index extends Action
{
    /**
     * @var ModuleList
     */
    protected $moduleList;

    public function __construct(
        ModuleList $moduleList,
        Context $context
    ) {
        $this->moduleList = $moduleList;

        parent::__construct($context);
    }
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD)
     */
    public function execute()
    {
        echo '<pre>';
        foreach ($this->moduleList->getNames() as $name) {
            if (substr($name, 0, 9) == 'Ecomatic_') {
                echo substr($name, 9).PHP_EOL;
            }
        }

        exit;
    }
}
