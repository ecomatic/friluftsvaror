<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-core
 * @version   1.2.24
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\Core\Model;

use Magento\Framework\FlagFactory;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\ProductMetadata;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Config\ConfigOptionsListConstants;
use Magento\Framework\Module\Dir\Reader as DirReader;

/**
 * @SuppressWarnings(PHPMD)
 */
class License
{
    const EDITION_EE = 'EE';
    const EDITION_CE = 'CE';

    const STATUS_ACTIVE  = 'active';
    const STATUS_LOCKED  = 'locked';
    const STATUS_INVALID = 'invalid';

    /**
     * @var UrlInterface
     */
    protected $urlManager;

    /**
     * @var ModuleListInterface
     */
    protected $moduleList;

    /**
     * @var CurlFactory
     */
    protected $curlFactory;

    /**
     * @var FlagFactory
     */
    protected $flagFactory;

    /**
     * @var ProductMetadata
     */
    protected $productMetadata;

    /**
     * @var DeploymentConfig
     */
    protected $deploymentConfig;

    /**
     * @var DirReader
     */
    protected $dirReader;

    /**
     * @var string
     */
    private $license;

    /**
     * @var string
     */
    private $key;

    public function __construct(
        UrlInterface $urlManager,
        ModuleListInterface $moduleList,
        CurlFactory $curlFactory,
        FlagFactory $flagFactory,
        ProductMetadata $productMetadata,
        DeploymentConfig $deploymentConfig,
        DirReader $dirReader
    ) {
        $this->urlManager = $urlManager;
        $this->moduleList = $moduleList;
        $this->curlFactory = $curlFactory;
        $this->flagFactory = $flagFactory;
        $this->productMetadata = $productMetadata;
        $this->deploymentConfig = $deploymentConfig;
        $this->dirReader = $dirReader;
    }

    /**
     * License status
     *
     * @param string $className
     *
     * @return true|string
     */
    public function getStatus($className = '')
    {
        return true;
    }

    /**
     * @param string $className
     * @return false|string
     */
    private function getModuleDirByClass($className)
    {
        $module = $this->getModuleByClass($className);
        if ($module) {
            return $this->dirReader->getModuleDir("", $module);
        }

        return false;
    }

    /**
     * @param string $className
     * @return false|string
     */
    private function getModuleByClass($className)
    {
        $class = explode('\\', $className);
        if (isset($class[1])) {
            $module = 'Ecomatic_' . $class[1];

            return $module;
        }

        return false;
    }

    /**
     * Send request with all required data
     *
     * @return $this
     */
    public function request()
    {
        $params = [];
        $params['v'] = 3;
        $params['d'] = $this->getDomain();
        $params['ip'] = $this->getIP();
        $params['mv'] = $this->getVersion();
        $params['me'] = $this->getEdition();
        $params['l'] = $this->license;
        $params['k'] = $this->key;
        $params['uid'] = $this->getUID();

        $result = $this->sendRequest('http://ecomatic.com/lc/check/', $params);

        $result['time'] = time();
        $this->saveFlagData($this->license, $result);

        return $this;
    }

    /**
     * Save request result to flag
     *
     * @param string $license
     * @param array  $data
     * @return $this
     */
    protected function saveFlagData($license, $data)
    {
        $flag = $this->flagFactory->create(['data' => ['flag_code' => "m" . $license]])
            ->loadSelf();

        $flag->setFlagData(base64_encode(serialize($data)))
            ->save();

        return $this;
    }

    /**
     * Return last request result
     *
     * @param string $license
     * @return array
     */
    protected function getFlagData($license)
    {
        $flag = $this->flagFactory->create(['data' => ['flag_code' => "m" . $license]])
            ->loadSelf();

        if ($flag->getFlagData()) {
            $data = @unserialize(@base64_decode($flag->getFlagData()));

            if (is_array($data)) {
                return $data;
            }
        }

        return [];
    }

    /**
     * Send http request
     *
     * @param string $endpoint
     * @param array  $params
     * @return array
     */
    public function sendRequest($endpoint, $params)
    {
        $curl = $this->curlFactory->create();
        $config = ['timeout' => 10];

        $curl->setConfig($config);
        $curl->write(
            \Zend_Http_Client::POST,
            $endpoint,
            '1.1',
            [],
            http_build_query($params, '', '&')
        );
        $response = $curl->read();

        $response = preg_split('/^\r?$/m', $response, 2);
        $response = trim($response[1]);

        $response = @unserialize($response);

        if (is_array($response)) {
            return $response;
        }

        return [];
    }

    /**
     * Backend domain
     *
     * @return string
     */
    private function getDomain()
    {
        return $this->urlManager->getCurrentUrl();
    }

    /**
     * Server IP
     *
     * @return string|bool
     */
    private function getIP()
    {
        return array_key_exists('SERVER_ADDR', $_SERVER)
            ? $_SERVER['SERVER_ADDR']
            : (array_key_exists('LOCAL_ADDR', $_SERVER) ? $_SERVER['LOCAL_ADDR'] : false);
    }

    /**
     * Magento edition
     *
     * @return string
     */
    private function getEdition()
    {
        if ($this->moduleList->has('Magento_Enterprise') || $this->moduleList->has('Magento_CustomerSegment')) {
            return 'EE';
        }

        return 'CE';
    }

    /**
     * Magento version
     *
     * @return string
     */
    private function getVersion()
    {
        return $this->productMetadata->getVersion();
    }

    /**
     * Unique installation key
     *
     * @return string
     */
    private function getUID()
    {
        $db = $this->deploymentConfig->get(
            ConfigOptionsListConstants::CONFIG_PATH_DB_CONNECTION_DEFAULT
            . '/' . ConfigOptionsListConstants::KEY_NAME);
        $host = $this->deploymentConfig->get(
            ConfigOptionsListConstants::CONFIG_PATH_DB_CONNECTION_DEFAULT
            . '/' . ConfigOptionsListConstants::KEY_HOST);

        return md5($db . $host);
    }
}
