<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-core
 * @version   1.2.24
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\Core\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * @method string getUrlKey()
 * @method $this setUrlKey($key)
 *
 * @method string getType()
 * @method $this setType($type)
 *
 * @method string getModule()
 * @method $this setModule($module)
 *
 * @method int getEntityId()
 */
class UrlRewrite extends AbstractModel
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('Ecomatic\Core\Model\ResourceModel\UrlRewrite');
    }
}
