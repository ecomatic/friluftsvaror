<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-core
 * @version   1.2.24
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */



namespace Ecomatic\Core\Model;

use Magento\AdminNotification\Model\Feed;

class NotificationFeed extends Feed
{
    /**
     * @var string
     */
    protected $feedUrl;

    /**
     * {@inheritdoc}
     */
    public function getFeedUrl()
    {
        if ($this->feedUrl === null) {
            $this->feedUrl = 'https://ecomatic.com/blog/category/magento-2-feed/feed/';
        }

        return $this->feedUrl;
    }
}