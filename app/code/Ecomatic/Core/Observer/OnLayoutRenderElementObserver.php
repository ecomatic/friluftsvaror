<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-core
 * @version   1.2.24
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\Core\Observer;

use Magento\Framework\App\Response\Http as HttpResponse;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Ecomatic\Core\Model\LicenseFactory;

class OnLayoutRenderElementObserver implements ObserverInterface
{
    /**
     * @var LicenseFactory
     */
    protected $licenseFactory;

    /**
     * @param LicenseFactory $licenseFactory
     */
    public function __construct(
        LicenseFactory $licenseFactory
    ) {
        $this->licenseFactory = $licenseFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(EventObserver $observer)
    {
        $event = $observer->getEvent();
        /** @var \Magento\Framework\View\Layout $layout */
        $layout = $event->getData('layout');
        $name = $event->getData('element_name');

        if ($name) {
            /** @var \Magento\Framework\View\Element\AbstractBlock $block */
            $block = $layout->getBlock($name);

            if (is_object($block) && substr(get_class($block), 0, 9) == 'Ecomatic\\') {
                if ($block instanceof \Ecomatic\Core\Block\Adminhtml\Menu) {
                    return;
                }

                $status = $this->licenseFactory->create()->getStatus(get_class($block));

                if ($status === true) {
                    return;
                }
                $transport = $event->getData('transport');

                $transport->setData('output', "<div class='message message-warning warning'>$status</div>");
            }
        }
    }
}