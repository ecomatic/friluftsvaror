<?php
/**
 * Ecomatic
 *
 * This source file is subject to the Ecomatic Software License, which is available at https://ecomatic.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Ecomatic
 * @package   ecomatic/module-core
 * @version   1.2.24
 * @copyright Copyright (C) 2017 Ecomatic (https://ecomatic.com/)
 */


namespace Ecomatic\Core\Observer;

use Magento\Framework\App\Response\Http as HttpResponse;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Ecomatic\Core\Model\LicenseFactory;
use Ecomatic\Core\Model\NotificationFeedFactory;

class OnActionPredispatchObserver implements ObserverInterface
{
    /**
     * @var NotificationFeedFactory
     */
    protected $feedFactory;

    /**
     * @param NotificationFeedFactory $feedFactory
     */
    public function __construct(
        NotificationFeedFactory $feedFactory
    ) {
        $this->feedFactory = $feedFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(EventObserver $observer)
    {
        $feedModel = $this->feedFactory->create();
        $feedModel->checkUpdate();
    }
}