<?php declare(strict_types=1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Service;

use Magento\Framework\App\Request\Http as HttpRequest;
use Addwish\Awext\Model\ProductFeedFactory;
use Addwish\Awext\Model\OrderFeedFactory;
use Addwish\Awext\Model\Info as InfoModel;
use Addwish\Awext\Helper\Config as ConfigHelper;
use Addwish\Awext\Api\Service\ManagementInterface;
use Magento\Framework\HTTP\PhpEnvironment\Request;

/**
 * Class AwextManagement
 */
class AwextManagement implements ManagementInterface
{

    /**
     * @var ProductFeedFactory
     */
    protected $productFeed;
    /**
     * @var OrderFeedFactory
     */
    protected $orderFeed;
    /**
     * @var HttpRequest
     */
    protected $httpRequest;
    /**
     * @var ConfigHelper
     */
    protected $configHelper;
    /**
     * @var InfoModel
     */
    protected $infoModel;
    /**
     * @var Request
     */
    private $request;

    /**
     * AwextManagement constructor.
     *
     * @param ProductFeedFactory $productFeed
     * @param OrderFeedFactory   $orderFeed
     * @param HttpRequest        $httpRequest
     * @param ConfigHelper       $configHelper
     * @param InfoModel          $infoModel
     * @param Request            $request
     */
    public function __construct(
        ProductFeedFactory $productFeed,
        OrderFeedFactory $orderFeed,
        HttpRequest $httpRequest,
        ConfigHelper $configHelper,
        InfoModel $infoModel,
        Request $request
    ) {
        $this->productFeed   = $productFeed;
        $this->orderFeed     = $orderFeed;
        $this->httpRequest   = $httpRequest;
        $this->configHelper  = $configHelper;
        $this->infoModel     = $infoModel;
        $this->request = $request;
    }

    /**
     * Gets product feed
     *
     * @return array
     */
    public function getProductFeed(): array
    {
        $ipWhitelist   = $this->configHelper->getWhitelistIPAddresses();
        $remoteAddress = $this->request->getClientIp();
        $response      = [];

        if (!in_array($remoteAddress, $ipWhitelist, true)) {
            return [sprintf('Access denied from %s', $remoteAddress)];
        }

        $currentPage = (int) $this->httpRequest->getParam('page');
        $pageSize    = (int) $this->httpRequest->getParam('pageSize');

        $isEnabled = $this->configHelper->isProductFeedEnabled();

        if ($isEnabled) {
            $productFeet = $this->productFeed
                ->create()
                ->execute($currentPage, $pageSize);

            if ($currentPage <= $productFeet->getLastPageNumber()) {
                $response = $productFeet->getFeed();
            }
        }

        return $response;
    }

    /**
     * Gets order feed
     *
     * @return array
     */
    public function getOrderFeed(): array
    {
        $ipWhitelist   = $this->configHelper->getWhitelistIPAddresses();
        $remoteAddress = $this->request->getClientIp();

        $response = [];

        if (!in_array($remoteAddress, $ipWhitelist, true)) {
            return [sprintf('Access denied from %s', $remoteAddress)];
        }

        $dateFrom  = (string) $this->httpRequest->getParam('exportFromDate');
        $dateTo    = (string) $this->httpRequest->getParam('exportToDate');
        $isEnabled = $this->configHelper->isOrderFeedEnabled();

        if ($isEnabled) {
            $orderFeed = $this->orderFeed
                ->create()
                ->execute($dateFrom, $dateTo);
            $response  = $orderFeed->getFeed();
        }

        return $response;
    }

    /**
     * Gets module info
     *
     * @return array
     */
    public function getModuleInfo(): array
    {
        return $this->infoModel->getModuleInfo();
    }
}
