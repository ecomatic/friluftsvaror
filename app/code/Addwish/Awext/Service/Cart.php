<?php
/**
 * Created by PhpStorm.
 * User: waldek
 * Date: 13.04.18
 * Time: 10:07
 */

namespace Addwish\Awext\Service;

use Addwish\Awext\Api\Service\CartInterface;
use Addwish\Awext\Model\AddwishCart;
use Addwish\Awext\Model\AddwishCartFactory;
use \Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote;

/**
 * Class Cart
 * @package Addwish\Awext\Service
 */
class Cart implements CartInterface
{
    /**
     * @var AddwishCartFactory
     */
    private $addwishCartFactory;

    /**
     * @var Session
     */
    private $session;

    /**
     * Cart constructor.
     * @param AddwishCartFactory $addwishCartFactory
     * @param Session $session
     */
    public function __construct(AddwishCartFactory $addwishCartFactory, Session $session)
    {
        $this->session = $session;
        $this->addwishCartFactory = $addwishCartFactory;
    }

    /**
     * @return string
     */
    public function getCart(): string
    {
        /** @var AddwishCart $cart */
        $cart = $this->addwishCartFactory->create();

        if (null !== $this->getQuote()->getGrandTotal()) {
            $cart
                ->setTotal($this->getQuote()->getSubtotal())
                ->setEmail($this->getQuote()->getCustomerEmail());

            foreach ($this->getQuote()->getAllVisibleItems() as $quoteItem) {
                $cart->addProductNumber($quoteItem->getSku());
            }
        }

        return $cart->toJson();
    }

    /**
     * @return \Magento\Quote\Model\Quote
     */
    protected function getQuote(): Quote
    {
        return $this->session->getQuote();
    }
}
