<?php
/**
 * @package   Addwish\Awext
 * @author    Mateusz Bukowski <mbukowski@divante.pl>
 * @copyright 2018 Divante Sp. z o.o.
 * @license   See LICENSE_DIVANTE.txt for license details.
 */

namespace Addwish\Awext\Controller\Tracking;

use Addwish\Awext\Block\TrackConversion;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Class Code
 */
class Conversion extends Action
{

    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * Render constructor.
     *
     * @param Context     $context
     * @param JsonFactory $jsonFactory
     */
    public function __construct(Context $context, JsonFactory $jsonFactory)
    {
        parent::__construct($context);

        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return Json
     */
    public function execute(): Json
    {
        $resultJson = $this->jsonFactory->create();

        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $resultJson->setHttpResponseCode(400);
        }

        $html = $this->_view->getLayout()->createBlock(TrackConversion::class)->toHtml();

        return $resultJson->setData(['html' => $html]);
    }
}
