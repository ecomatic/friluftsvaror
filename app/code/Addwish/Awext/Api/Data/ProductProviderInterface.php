<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Api\Data;

/**
 * Interface ProductProviderInterface
 */
interface ProductProviderInterface extends ProviderInterface
{
    /**
     * Default product nodes
     */
    const TITLE_XML_NODE_NAME          = 'title';
    const URL_XML_NODE_NAME            = 'url';
    const PRODUCT_NUMBER_XML_NODE_NAME = 'productnumber';
    const PRICE_XML_NODE_NAME          = 'price';
    const PREVIOUS_PRICE_XML_NODE_NAME = 'previousprice';
    const IN_STOCK_XML_NODE_NAME       = 'instock';
    const QTY_XML_NODE_NAME            = 'qty';
    const DESCRIPTION_XML_NODE_NAME    = 'description';
    const IMG_URL_XML_NODE_NAME        = 'imgurl';
    const KEYWORDS_XML_NODE_NAME       = 'keywords';
    const BRAND_XML_NODE_NAME          = 'brand';
    const EAN_XML_NODE_NAME            = 'ean';
    const HIERARCHIES_XML_NODE_NAME    = 'hierarchies';

    /**
     * Default product attributes
     */
    const ENTITY_ID_ATTRIBUTE_NAME = 'entity_id';
    const NAME_ATTRIBUTE_NAME      = 'name';
    const PATH_ATTRIBUTE_NAME      = 'path';
    const EAN_ATTRIBUTE_NAME       = 'ean';
    const BRAND_ATTRIBUTE_NAME     = 'brand';
}
