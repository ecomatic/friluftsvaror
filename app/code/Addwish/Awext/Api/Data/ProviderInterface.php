<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Api\Data;

/**
 * Interface ProviderInterface
 */
interface ProviderInterface
{
    /**
     * Generating data
     *
     * @param array $dataArray
     *
     * @return array
     */
    public function generate(array $dataArray): array;
}
