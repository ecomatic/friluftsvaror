<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Api\Data;

/**
 * Interface OrderProviderInterface
 */
interface OrderProviderInterface extends ProviderInterface
{
    /**
     * Default order nodes
     */
    const ORDER_NUMBER_NODE_NAME       = 'order_number';
    const TOTAL_NODE_NAME              = 'total';
    const DATE_NODE_NAME               = 'date';
    const CUSTOMER_EMAIL_NODE_NAME     = 'email';
    const PRODUCTS_NODE_NAME           = 'products';
    const PRODUCT_NUMBER_XML_NODE_NAME = 'productnumber';
}
