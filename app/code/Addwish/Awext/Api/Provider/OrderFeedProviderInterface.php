<?php
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE_ADDWISH.txt for license details.
 */

namespace Addwish\Awext\Api\Provider;

/**
 * Interface OrderFeedProviderInterface
 */
interface OrderFeedProviderInterface extends ProviderInterface
{
}
