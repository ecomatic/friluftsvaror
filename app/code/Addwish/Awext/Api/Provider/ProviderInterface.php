<?php
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE_ADDWISH.txt for license details.
 */

namespace Addwish\Awext\Api\Provider;

/**
 * Interface ProviderInterface
 */
interface ProviderInterface
{
    /**
     * Creating feed data
     *
     * @param array $dataArrays
     *
     * @return array
     */
    public function generateData(array $dataArray): array;
}
