<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Api\Service;

/**
 * Interface ManagementInterface
 * @api
 */
interface ManagementInterface
{
    /**
     * Gets product feed
     *
     * @return array
     */
    public function getProductFeed(): array;

    /**
     * Gets order feed
     *
     * @return array
     */
    public function getOrderFeed(): array;

    /**
     * Gets module info
     *
     * @return array
     */
    public function getModuleInfo(): array;
}
