<?php
/**
 * Created by PhpStorm.
 * User: waldek
 * Date: 13.04.18
 * Time: 10:08
 */
namespace Addwish\Awext\Api\Service;

/**
 * Interface CartInterface
 * @package Addwish\Awext\Api\Service
 */
interface CartInterface
{
    /**
     * @return string
     */
    public function getCart(): string ;
}
