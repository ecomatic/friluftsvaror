<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Addwish\Awext\Helper\Config as ConfigHelper;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class TrackCode
 */
class TrackCode extends Template
{
    /**
     * @var ConfigHelper
     */
    protected $configHelper;
    /**
     * @var string
     */
    protected $_template = 'Addwish_Awext::track-code.phtml';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Track constructor.
     *
     * @param Context               $context
     * @param ConfigHelper          $configHelper
     * @param StoreManagerInterface $storeManager
     * @param array                 $data
     */
    public function __construct(
        Context $context,
        ConfigHelper $configHelper,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->configHelper = $configHelper;
        $this->_storeManager = $storeManager;

        parent::__construct($context, $data);
    }

    /**
     * Checking if module is enabled
     *
     * @return bool
     */
    public function isModuleEnabled(): bool
    {
        return $this->configHelper->isModuleOutputEnabled();
    }

    /**
     * Gets addwish id
     *
     * @return string
     */
    public function getAddwishId(): string
    {
        return $this->configHelper->getAddwishId();
    }

    /**
     * Check if current requested URL is secure
     *
     * @return bool
     */
    public function isCurrentlySecure(): bool
    {
        return $this->_storeManager->getStore()->isCurrentlySecure();
    }

    /**
     * Return Addwish api url
     *
     * @return string
     */
    public function getAddwishAwAddGiftUrl(): string
    {
        $url = 'http://cdn.addwish.com';

        if ($this->isCurrentlySecure()) {
            $url = 'https://d1pna5l3xsntoj.cloudfront.net';
        }

        $url .= '/scripts/company/awAddGift.js#' . $this->getAddwishId();

        return $url;
    }
}
