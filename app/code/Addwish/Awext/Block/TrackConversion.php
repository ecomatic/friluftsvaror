<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Block;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Addwish\Awext\Helper\Config as ConfigHelper;

/**
 * Class TrackConversion
 */
class TrackConversion extends Template
{
    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var Order
     */
    protected $order;
    /**
     * @var string
     */
    protected $_template = 'Addwish_Awext::track-conversion.phtml';

    /**
     * TrackConversion constructor.
     *
     * @param Context                    $context
     * @param ConfigHelper               $configHelper
     * @param CheckoutSession            $checkoutSession
     * @param OrderFactory               $orderFactory
     * @param ProductRepositoryInterface $productRepository
     * @param array                      $data
     */
    public function __construct(
        Context $context,
        ConfigHelper $configHelper,
        CheckoutSession $checkoutSession,
        OrderFactory $orderFactory,
        ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        $this->configHelper      = $configHelper;
        $this->checkoutSession   = $checkoutSession;
        $this->orderFactory      = $orderFactory;
        $this->productRepository = $productRepository;

        parent::__construct($context, $data);
    }

    /**
     * Checking if tracking conversion is enabled
     *
     * @return bool
     */
    public function isConversionTrackingEnabled(): bool
    {
        return $this->configHelper->isConversionTrackingEnabled();
    }

    /**
     * Gets order all visible items
     *
     * @return array
     */
    public function getOrderItems(): array
    {
        $order = $this->getLastRealOrder();

        if ($order instanceof Order) {
            return $order->getAllVisibleItems();
        }

        return [];
    }

    /**
     * Gets product url
     *
     * @param OrderItemInterface $item
     *
     * @return string
     */
    public function getProductUrl(OrderItemInterface $item): string
    {
        $url = '';

        try {
            $productId = $item->getProductId();
            $product   = $this->productRepository->getById($productId);

            if ($product instanceof Product) {
                $url = $product->getProductUrl();
            }
        } catch (NoSuchEntityException $e) {
            $this->_logger->error($e);
        }

        return $url;
    }

    /**
     * Gets order increment id
     *
     * @return string
     */
    public function getIncrementId(): string
    {
        return $this->order->getIncrementId();
    }

    /**
     * Gets subtotal
     *
     * @return float
     */
    public function getSubtotal(): float
    {
        return (float) $this->order->getSubtotal();
    }

    /**
     * Gets total qty ordered
     *
     * @return float
     */
    public function getTotalQtyOrdered(): float
    {
        return (float) $this->order->getTotalQtyOrdered();
    }

    /**
     * Gets customer email
     *
     * @return string
     */
    public function getCustomerEmail(): string
    {
        return $this->order->getCustomerEmail();
    }

    /**
     * Gets last real order
     *
     * @return OrderInterface
     */
    protected function getLastRealOrder(): OrderInterface
    {
        if (!$this->order instanceof OrderInterface) {
            $this->order = $this->checkoutSession->getLastRealOrder();
        }

        return $this->order;
    }
}
