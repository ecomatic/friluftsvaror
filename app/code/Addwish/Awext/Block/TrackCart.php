<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Block;

use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Addwish\Awext\Helper\Config as ConfigHelper;

/**
 * Class TrackCart
 */
class TrackCart extends Template
{
    /**
     * @var ConfigHelper
     */
    protected $configHelper;
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;
    /**
     * @var CustomerSession
     */
    protected $customerSession;
    /**
     * @var string
     */
    protected $_template = 'Addwish_Awext::track-cart.phtml';

    /**
     * TrackCart constructor.
     *
     * @param Context         $context
     * @param ConfigHelper    $configHelper
     * @param CheckoutSession $checkoutSession
     * @param CustomerSession $customerSession
     * @param array           $data
     */
    public function __construct(
        Context $context,
        ConfigHelper $configHelper,
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        array $data = []
    ) {
        $this->configHelper    = $configHelper;
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;

        parent::__construct($context, $data);
    }

    /**
     * Checking if module is enabled
     *
     * @return bool
     */
    public function isCartTrackingEnabled(): bool
    {
        return $this->configHelper->isCartTrackingEnabled();
    }

    /**
     * Gets customer email from session
     *
     * @return string
     */
    public function getCustomerEmail(): string
    {
        return (string) $this->customerSession->getCustomer()->getEmail();
    }

    /**
     * Gets product url
     *
     * @param CartItemInterface $item
     *
     * @return string
     */
    public function getProductUrl(CartItemInterface $item): string
    {
        return $item->getProduct()->getProductUrl();
    }

    /**
     * Gets cart subtotal
     *
     * @return float
     */
    public function getSubtotal(): float
    {
        return (float) $this->checkoutSession->getQuote()->getSubtotal();
    }

    /**
     * Gets cart items
     *
     * @return \Magento\Quote\Model\Quote\Item[]
     */
    public function getCartItems(): array
    {
        return $this->checkoutSession->getQuote()->getAllVisibleItems();
    }
}
