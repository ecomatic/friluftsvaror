<?php declare(strict_types=1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Helper;

use DateTime;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 */
class Config extends AbstractHelper
{

    /**
     * Default enabled value
     */
    const DEFAULT_ENABLED_VALUE = "1";
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var ModuleListInterface
     */
    protected $moduleList;
    /**
     * @var TimezoneInterface
     */
    protected $timezone;
    /**
     * Module config
     */
    const CONFIG_ADDWISH_ID                     = 'addwish_configuration/general/addwish_id';
    const CONFIG_IS_PRODUCT_FEED_ENABLED        = 'addwish_configuration/feed/is_active_product';
    const CONFIG_IS_ORDER_FEED_ENABLED          = 'addwish_configuration/feed/is_active_order';
    const CONFIG_IS_FEED_IP_WHITELIST           = 'addwish_configuration/feed/ip_whitelist';
    const CONFIG_IS_CART_TRACKING_ENABLED       = 'addwish_configuration/tracking/is_active_cart_tracking';
    const CONFIG_IS_CONVERSION_TRACKING_ENABLED = 'addwish_configuration/tracking/is_active_conversion_tracking';

    /**
     * Config constructor.
     *
     * @param Context               $context
     * @param StoreManagerInterface $storeManager
     * @param ModuleListInterface   $moduleList
     * @param TimezoneInterface     $timezone
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        ModuleListInterface $moduleList,
        TimezoneInterface $timezone
    ) {
        $this->storeManager = $storeManager;
        $this->moduleList   = $moduleList;
        $this->timezone     = $timezone;

        parent::__construct($context);
    }

    /**
     * Gets addwish id
     *
     * @return string
     */
    public function getAddwishId(): string
    {
        return (string) $this->getConfigValue(self::CONFIG_ADDWISH_ID);
    }

    /**
     * Checking if it's product feed enabled
     *
     * @return bool
     */
    public function isProductFeedEnabled(): bool
    {
        $value = $this->getConfigValue(self::CONFIG_IS_PRODUCT_FEED_ENABLED);

        if ($value == self::DEFAULT_ENABLED_VALUE) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getWhitelistIPAddresses()
    {
        $value = $this->getConfigValue(self::CONFIG_IS_FEED_IP_WHITELIST);

        return explode(',', $value);
    }

    /**
     * Checking if it's order feed enabled
     *
     * @return bool
     */
    public function isOrderFeedEnabled(): bool
    {
        $value = $this->getConfigValue(self::CONFIG_IS_ORDER_FEED_ENABLED);

        if ($value == self::DEFAULT_ENABLED_VALUE) {
            return true;
        }

        return false;
    }

    /**
     * Checking if it's cart tracking enabled
     *
     * @return bool
     */
    public function isCartTrackingEnabled(): bool
    {
        $value = $this->getConfigValue(self::CONFIG_IS_CART_TRACKING_ENABLED);

        if ($value == self::DEFAULT_ENABLED_VALUE) {
            return true;
        }

        return false;
    }

    /**
     * Checking if it's conversion tracking enabled
     *
     * @return bool
     */
    public function isConversionTrackingEnabled(): bool
    {
        $value = $this->getConfigValue(self::CONFIG_IS_CONVERSION_TRACKING_ENABLED);

        if ($value == self::DEFAULT_ENABLED_VALUE) {
            return true;
        }

        return false;
    }

    /**
     * Gets store id
     *
     * @return int
     */
    public function getStoreId(): int
    {
        return (int) $this->storeManager->getStore()->getId();
    }

    /**
     * Gets Module Version
     *
     * @return string
     */
    public function getModuleVersion(): string
    {
        $data = $this->moduleList->getOne($this->_getModuleName());

        if (array_key_exists('setup_version', $data)) {
            return $data['setup_version'];
        }

        return '';
    }

    /**
     * Gets current date
     *
     * @return DateTime
     */
    public function getCurrentDate(): DateTime
    {
        return $this->timezone->date();
    }

    /**
     * @param string $date
     * @param string $format
     *
     * @return string
     */
    public function formatDate(string $date, string $format)
    {
        return $this->timezone->date($date)->format($format);
    }

    /**
     * Gets config value
     *
     * @param string $path
     *
     * @return string
     */
    protected function getConfigValue(string $path): string
    {
        $value = $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            null
        );

        return $value ?? '';
    }
}
