## Overview
 
The Awext module allows to:

- gets product feed by rest api method (xml/json)
- gets order feed by rest api method (xml/json)
- gets information about module by rest api method (xml/json)
- adding Addwish tracking code
- conversion tracking
- cart tracking

 
## Installation details
 
Simply require module in your project using composer.
* `composer require addwish/awext`
* `php bin/magento module:enable Addwish_Awext`
* `php bin/magento setup:upgrade`

## Additional information
 
*There are three scope types default, websites, stores*
* If scope is set to default then scope_id is always 0
* If scope is set to websites then scope_id is website_id.
* If scope is set to stores then scope_id is store_id(store view).