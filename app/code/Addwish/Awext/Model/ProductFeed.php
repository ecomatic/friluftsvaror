<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Addwish\Awext\Model\Search\ProductCriteria;
use Addwish\Awext\Model\Data\ProductProvider;
use Addwish\Awext\Helper\Config as ConfigHelper;
use Addwish\Awext\Api\ProductFeedInterface;

/**
 * Class ProductFeed
 */
class ProductFeed extends AbstractFeed implements ProductFeedInterface
{
    /**
     * @var int
     */
    protected $currentPage = 1;

    /**
     * @var int
     */
    protected $pageSize = 20;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var ProductCriteria
     */
    protected $productCriteria;

    /**
     * @var ProductProvider
     */
    protected $productProvider;

    /**
     * ProductFeed constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param ConfigHelper               $configHelper
     * @param Context                    $context
     * @param Registry                   $registry
     * @param ProductCriteria            $productCriteria
     * @param ProductProvider            $productProvider
     * @param AbstractResource|null      $resource
     * @param AbstractDb|null            $resourceCollection
     * @param array                      $data
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        ConfigHelper $configHelper,
        Context $context,
        Registry $registry,
        ProductCriteria $productCriteria,
        ProductProvider $productProvider,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->productRepository = $productRepository;
        $this->productProvider   = $productProvider;
        $this->productCriteria   = $productCriteria;

        parent::__construct(
            $context,
            $registry,
            $configHelper,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Execute
     *
     * @param int|null $currentPage
     * @param int|null $pageSize
     *
     * @return ProductFeedInterface
     */
    public function execute(int $currentPage = null, int $pageSize = null): ProductFeedInterface
    {
        $this->init($currentPage, $pageSize);

        $searchCriteria = $this->productCriteria->getSearchCriteria($this->currentPage, $this->pageSize);
        $productList    = $this->getProductList($searchCriteria);

        if ($currentPage <= $this->getLastPageNumber()) {
            $productData = $this->productProvider->generate($productList);

            $this->populateFeedData($productData);
        }

        return $this;
    }

    /**
     * Initialize data
     *
     * @param int|null $currentPage
     * @param int|null $pageSize
     *
     * @return ProductFeedInterface
     */
    protected function init(int $currentPage = null, int $pageSize = null): ProductFeedInterface
    {
        $this->currentPage = $currentPage ? $currentPage + 1 : $this->currentPage;
        $this->pageSize    = $pageSize > 0 ? $pageSize : null;

        return $this;
    }

    /**
     * Gets product list
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    protected function getProductList(SearchCriteriaInterface $searchCriteria): array
    {
        $searchResults = $this->productRepository->getList($searchCriteria);

        $this->setLastPageNumber($searchResults);

        return $searchResults->getItems();
    }

    /**
     * @param SearchResultsInterface $searchResult
     *
     * @return ProductFeed
     */
    public function setLastPageNumber(SearchResultsInterface $searchResult): AbstractFeed
    {
        if ($searchResult->getSearchCriteria()->getPageSize()) {
            $pageSize             = $searchResult->getSearchCriteria()->getPageSize();
            $totalCount           = $searchResult->getTotalCount();
            $this->lastPageNumber = ceil($totalCount / $pageSize) - 1;
        }

        if ($this->lastPageNumber < 0) {
            $this->lastPageNumber = 0;
        }

        return $this;
    }
}
