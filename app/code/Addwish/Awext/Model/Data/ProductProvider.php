<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Model\Data;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Registry;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\Product\Media\Config as MediaConfig;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\Data\StockItemInterfaceFactory;
use Magento\Catalog\Model\Product;
use Addwish\Awext\Api\Data\ProductProviderInterface;

/**
 * Class ProductProvider
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductProvider extends AbstractModel implements ProductProviderInterface
{
    /**
     * @var int
     */
    protected $rootCategoryId;

    /**
     * @var array
     */
    protected $productArray = [];

    /**
     * @var array
     */
    protected $categoryArray = [];

    /**
     * @var StockItemRepository
     */
    protected $stockItemRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var MediaConfig
     */
    protected $mediaConfig;

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollection;

    /**
     * @var StockItemInterfaceFactory
     */
    protected $stockItemFactory;

    /**
     * ProductProvider constructor.
     *
     * @param Context                   $context
     * @param Registry                  $registry
     * @param StockItemRepository       $stockItemRepository
     * @param StoreManagerInterface     $storeManager
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param MediaConfig               $mediaConfig
     * @param StockItemInterfaceFactory $stockItemInterfaceFactory
     * @param AbstractResource|null     $resource
     * @param AbstractDb|null           $resourceCollection
     * @param array                     $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Registry $registry,
        StockItemRepository $stockItemRepository,
        StoreManagerInterface $storeManager,
        CategoryCollectionFactory $categoryCollectionFactory,
        MediaConfig $mediaConfig,
        StockItemInterfaceFactory $stockItemInterfaceFactory,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->stockItemRepository = $stockItemRepository;
        $this->storeManager        = $storeManager;
        $this->categoryCollection  = $categoryCollectionFactory;
        $this->mediaConfig         = $mediaConfig;
        $this->stockItemFactory    = $stockItemInterfaceFactory;
        $this->rootCategoryId      = $this->getRootCategoryId();

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Generating data
     *
     * @param array $dataArray
     *
     * @return array
     */
    public function generate(array $dataArray): array
    {
        $this->prepareCategoryArray();

        foreach ($dataArray as $product) {
            if ($product instanceof ProductInterface) {
                $productArray = [];
                $stock        = $this->getProductStockItem((int) $product->getId());

                $specialPrice = $product->getPriceInfo()->getPrice('special_price')->getAmount()->getValue();
                $price = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();

                if (false !== $specialPrice) {
                    $price = $specialPrice;
                }

                $regularPrice = $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue();

                $productArray[self::TITLE_XML_NODE_NAME]          = $product->getName();
                $productArray[self::URL_XML_NODE_NAME]            = $product->getProductUrl();
                $productArray[self::PRODUCT_NUMBER_XML_NODE_NAME] = $product->getSku();
                $productArray[self::PRICE_XML_NODE_NAME]          = $price;
                $productArray[self::PREVIOUS_PRICE_XML_NODE_NAME] = $regularPrice;
                $productArray[self::IN_STOCK_XML_NODE_NAME]       = $stock->getIsInStock();
                $productArray[self::QTY_XML_NODE_NAME]            = $stock->getQty();
                $productArray[self::DESCRIPTION_XML_NODE_NAME]    = $product->getDescription();
                $productArray[self::IMG_URL_XML_NODE_NAME]        = $this->getProductImageUrl($product);
                $productArray[self::KEYWORDS_XML_NODE_NAME]       = $product->getMetaKeyword();

                if ($product->getCustomAttribute(self::BRAND_ATTRIBUTE_NAME)) {
                    $brand                                   =
                        $product->getCustomAttribute(self::BRAND_ATTRIBUTE_NAME)->getValue();
                    $productArray[self::BRAND_XML_NODE_NAME] = $brand;
                }

                if ($product->getCustomAttribute(self::EAN_ATTRIBUTE_NAME)) {
                    $ean                                   =
                        $product->getCustomAttribute(self::EAN_ATTRIBUTE_NAME)->getValue();
                    $productArray[self::EAN_XML_NODE_NAME] = $ean;
                }

                $productArray[self::HIERARCHIES_XML_NODE_NAME] = $this->getProductCategoryHierarchy($product);

                $this->productArray[] = $productArray;
            }
        }

        return $this->productArray;
    }

    /**
     * Gets all categories by root category ID
     *
     * @return \Magento\Framework\DataObject[]
     */
    protected function getAllCategoryByRootId(): array
    {
        try {
            $categoryCollection = $this->categoryCollection->create()
                ->addAttributeToSelect(
                    [
                        self::ENTITY_ID_ATTRIBUTE_NAME,
                        self::PATH_ATTRIBUTE_NAME,
                        self::NAME_ATTRIBUTE_NAME
                    ]
                )
                ->addAttributeToFilter(self::PATH_ATTRIBUTE_NAME, ['like' => "1/{$this->rootCategoryId}/%"]);
        } catch (LocalizedException $e) {
            return [];
        }

        return $categoryCollection->getItems();
    }

    /**
     * Preparing category array
     *
     * @return ProductProvider
     */
    protected function prepareCategoryArray(): ProductProvider
    {
        $categoryList = $this->getAllCategoryByRootId();

        foreach ($categoryList as $item) {
            $this->categoryArray[$item->getId()] = $item->getName();
        }

        return $this;
    }

    /**
     * Gets product category tree
     *
     * @param string $path
     *
     * @return array
     */
    protected function getProductCategoryTree(string $path): array
    {
        $dataArray   = [];
        $categoryIds = explode('/', $path);

        foreach ($categoryIds as $item) {
            if ($item > $this->rootCategoryId) {
                $dataArray[] = $this->categoryArray[$item];
            }
        }

        return $dataArray;
    }

    /**
     * Gets product category hierarchy
     *
     * @param ProductInterface $product
     *
     * @return array
     */
    protected function getProductCategoryHierarchy(ProductInterface $product): array
    {
        $hierarchies   = [];
        $categoryItems = null;

        if ($product instanceof Product) {
            $categoryItems = $product->getCategoryCollection()->getItems();
        }

        if (is_array($categoryItems)) {
            foreach ($categoryItems as $item) {
                $hierarchies[] = $this->getProductCategoryTree($item->getPath());
            }
        }

        return $hierarchies;
    }

    /**
     * Gets product stock item
     *
     * @param int $productId
     *
     * @return StockItemInterface
     */
    protected function getProductStockItem(int $productId): StockItemInterface
    {
        try {
            return $this->stockItemRepository->get($productId);
        } catch (NoSuchEntityException $e) {
            return $this->stockItemFactory->create();
        }
    }

    /**
     * Gets product image url
     *
     * @param ProductInterface $product
     *
     * @return string
     */
    protected function getProductImageUrl(ProductInterface $product): string
    {
        $mediaImagePath = $this->mediaConfig->getBaseMediaUrl();

        if ($product instanceof Product) {
            return $mediaImagePath . $product->getImage();
        }

        return '';
    }

    /**
     * Gets root category id
     *
     * @return int
     */
    protected function getRootCategoryId(): int
    {
        $storeId = $this->storeManager->getStore()->getId();
        $store   = $this->storeManager->getStore($storeId);

        if ($store instanceof Store) {
            return (int) $store->getRootCategoryId();
        }

        return 0;
    }
}
