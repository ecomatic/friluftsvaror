<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Model\Data;

use Magento\Framework\Model\AbstractModel;
use Magento\Sales\Api\Data\OrderInterface;
use Addwish\Awext\Api\Data\OrderProviderInterface;

/**
 * Class OrderProvider
 */
class OrderProvider extends AbstractModel implements OrderProviderInterface
{
    /**
     * @var array
     */
    protected $orderFeedArray = [];

    /**
     * Generating data
     *
     * @param array $dataArray
     *
     * @return array
     */
    public function generate(array $dataArray): array
    {
        foreach ($dataArray as $order) {
            if ($order instanceof OrderInterface) {
                $orderItems = $order->getItems();

                $this->orderFeedArray[] = [
                    self::ORDER_NUMBER_NODE_NAME  => $order->getIncrementId(),
                    self::TOTAL_NODE_NAME          => $order->getBaseGrandTotal(),
                    self::DATE_NODE_NAME           => $order->getCreatedAt(),
                    self::CUSTOMER_EMAIL_NODE_NAME => $order->getCustomerEmail(),
                    self::PRODUCTS_NODE_NAME       => $this->getProductList($orderItems)
                ];
            }
        }

        return $this->orderFeedArray;
    }

    /**
     * Gets products list
     *
     * @param array $order
     *
     * @return array
     */
    protected function getProductList(array $order): array
    {
        $productList = [];

        foreach ($order as $item) {
            $productList[][self::PRODUCT_NUMBER_XML_NODE_NAME] = $item->getSku();
        }

        return $productList;
    }
}
