<?php
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE_ADDWISH.txt for license details.
 */

namespace Addwish\Awext\Model\Provider;

use Addwish\Awext\Api\Provider\ProductFeedProviderInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Registry;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\Product\Media\Config as MediaConfig;
use Addwish\Awext\Api\Data\ProductConfigInterface;

/**
 * Class ProductFeedProvider
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductFeedProvider extends AbstractModel implements ProductConfigInterface, ProductFeedProviderInterface
{
    /**
     * @var int
     */
    protected $rootCategoryId;

    /**
     * @var array
     */
    protected $productArray = [];

    /**
     * @var array
     */
    protected $categoryArray = [];

    /**
     * @var StockItemRepository
     */
    protected $stockItemRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var MediaConfig
     */
    protected $mediaConfig;

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollection;

    /**
     * ProductFeedProvider constructor.
     *
     * @param Context                   $context
     * @param Registry                  $registry
     * @param StockItemRepository       $stockItemRepository
     * @param StoreManagerInterface     $storeManager
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param MediaConfig               $mediaConfig
     * @param AbstractResource|null     $resource
     * @param AbstractDb|null           $resourceCollection
     * @param array                     $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        StockItemRepository $stockItemRepository,
        StoreManagerInterface $storeManager,
        CategoryCollectionFactory $categoryCollectionFactory,
        MediaConfig $mediaConfig,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->stockItemRepository = $stockItemRepository;
        $this->storeManager        = $storeManager;
        $this->categoryCollection  = $categoryCollectionFactory;
        $this->mediaConfig         = $mediaConfig;

        $storeId              = $this->storeManager->getStore()->getId();
        $this->rootCategoryId = $this->storeManager->getStore($storeId)->getRootCategoryId();

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @param array $dataArray
     * @return array
     * @throws LocalizedException
     */
    public function generateData(array $dataArray): array
    {
        $this->prepareCategoryArray();

        foreach ($dataArray as $product) {
            if ($product instanceof ProductInterface) {
                $productArray = [];
                $stock        = $this->getProductStockItem($product->getId());

                $productArray[self::TITLE_XML_NODE_NAME]          = $product->getName();
                $productArray[self::URL_XML_NODE_NAME]            = $product->getProductUrl();
                $productArray[self::PRODUCT_NUMBER_XML_NODE_NAME] = $product->getSku();
                $productArray[self::PRICE_XML_NODE_NAME]          = $product->getFinalPrice();
                $productArray[self::PREVIOUS_PRICE_XML_NODE_NAME] = $product->getPrice();
                $productArray[self::IN_STOCK_XML_NODE_NAME]       = $stock->getIsInStock();
                $productArray[self::QTY_XML_NODE_NAME]            = $stock->getQty();
                $productArray[self::DESCRIPTION_XML_NODE_NAME]    = $product->getDescription();
                $productArray[self::IMG_URL_XML_NODE_NAME]        = $this->getProductImageUrl($product);
                $productArray[self::KEYWORDS_XML_NODE_NAME]       = $product->getMetaKeyword();

                if ($product->getCustomAttribute(self::BRAND_ATTRIBUTE_NAME)) {
                    $brand                                   =
                        $product->getCustomAttribute(self::BRAND_ATTRIBUTE_NAME)->getValue();
                    $productArray[self::BRAND_XML_NODE_NAME] = $brand;
                }

                if ($product->getCustomAttribute(self::EAN_ATTRIBUTE_NAME)) {
                    $ean                                   =
                        $product->getCustomAttribute(self::EAN_ATTRIBUTE_NAME)->getValue();
                    $productArray[self::EAN_XML_NODE_NAME] = $ean;
                }

                $productArray[self::HIERARCHIES_XML_NODE_NAME] = $this->getProductCategoryHierarchy($product);

                $this->productArray[] = $productArray;
            }
        }

        return $this->productArray;
    }

    /**
     * Gets all categories by root category ID
     *
     * @return \Magento\Framework\DataObject[]
     */
    protected function getAllCategoryByRootId(): array
    {
        try {
            $categoryCollection = $this->categoryCollection->create()
                ->addAttributeToSelect(
                    [
                        self::ENTITY_ID_ATTRIBUTE_NAME,
                        self::PATH_ATTRIBUTE_NAME,
                        self::NAME_ATTRIBUTE_NAME
                    ]
                )
                ->addAttributeToFilter(self::PATH_ATTRIBUTE_NAME, ['like' => "1/{$this->rootCategoryId}/%"]);
        } catch (LocalizedException $e) {
            return [];
        }

        return $categoryCollection->getItems();
    }

    /**
     * Preparing category array
     *
     * @return ProductFeedProvider
     * @throws LocalizedException
     */
    protected function prepareCategoryArray(): ProductFeedProvider
    {
        $categoryList = $this->getAllCategoryByRootId();

        foreach ($categoryList as $item) {
            $this->categoryArray[$item->getId()] = $item->getName();
        }

        return $this;
    }

    /**
     * Gets product category tree
     *
     * @param string $path
     *
     * @return array
     */
    protected function getProductCategoryTree($path): array
    {
        $dataArray   = [];
        $categoryIds = explode('/', $path);

        foreach ($categoryIds as $item) {
            if ($item > $this->rootCategoryId) {
                $dataArray[] = $this->categoryArray[$item];
            }
        }

        return $dataArray;
    }

    /**
     * Gets product category hierarchy
     *
     * @param Product $product
     *
     * @return array
     * @throws LocalizedException
     */
    protected function getProductCategoryHierarchy($product): array
    {
        $hierarchies   = [];
        $categoryItems = $product->getCategoryCollection()->getItems();

        if (is_array($categoryItems)) {
            foreach ($categoryItems as $item) {
                $hierarchies[] = $this->getProductCategoryTree($item->getPath());
            }
        }

        return $hierarchies;
    }

    /**
     * Gets product stock item
     *
     * @param int $productId
     *
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface|null
     */
    protected function getProductStockItem($productId)
    {
        try {
            return $this->stockItemRepository->get($productId);
        } catch (NoSuchEntityException $e) {
            return null; //todo
        }
    }

    /**
     * Gets product image url
     *
     * @param Product $product
     *
     * @return string
     */
    protected function getProductImageUrl($product): string
    {
        $mediaImagePath = $this->mediaConfig->getBaseMediaUrl();
        $imagePath      = $mediaImagePath . $product->getImage();

        return $imagePath;
    }
}
