<?php
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE_ADDWISH.txt for license details.
 */

namespace Addwish\Awext\Model\Provider;

use Addwish\Awext\Api\Provider\OrderFeedProviderInterface;
use Addwish\Awext\Api\Data\OrderConfigInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class OrderFeedProvider
 */
class OrderFeedProvider extends AbstractModel implements OrderConfigInterface, OrderFeedProviderInterface
{
    /**
     * @var array
     */
    protected $orderFeedArray = [];

    /**
     * {@inheritdoc}
     */
    public function generateData(array $dataArray): array
    {
        foreach ($dataArray as $order) {
            if ($order instanceof OrderInterface) {
                $orderArray = [];

                $orderArray[self::ORDER_NUMBER_NODE_NAME]   = $order->getIncrementId();
                $orderArray[self::TOTAL_NODE_NAME]          = $order->getBaseGrandTotal();
                $orderArray[self::DATE_NODE_NAME]           = $order->getCreatedAt();
                $orderArray[self::CUSTOMER_EMAIL_NODE_NAME] = $order->getCustomerEmail();
                $orderArray[self::PRODUCTS_NODE_NAME]       = $this->getProductList($order->getItems());
            }

            $this->orderFeedArray[] = $orderArray;
        }

        return $this->orderFeedArray;
    }

    /**
     * Gets order product list
     *
     * @param $order
     *
     * @return array
     */
    protected function getProductList($order)
    {
        $productList = [];

        foreach ($order as $item) {
            $productList[self::PRODUCT_NUMBER_XML_NODE_NAME] = $item->getSku();
        }

        return [$productList];
    }
}
