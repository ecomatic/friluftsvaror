<?php
/**
 * Created by PhpStorm.
 * User: waldek
 * Date: 13.04.18
 * Time: 10:23
 */

namespace Addwish\Awext\Model;

/**
 * Class AddwishCart
 * @package Addwish\Awext\Model
 */
class AddwishCart
{
    /**
     * @var float | null
     */
    private $total;

    /**
     * @var string | null
     */
    private $email;

    /**
     * @var array
     */
    private $productNumbers = [];

    /**
     * @return float | null
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     * @return AddwishCart
     */
    public function setTotal(float $total): AddwishCart
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return AddwishCart
     */
    public function setEmail(string $email = null): AddwishCart
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return array
     */
    public function getProductNumbers(): array
    {
        return $this->productNumbers;
    }

    /**
     * @param string $productNumber
     * @return AddwishCart
     */
    public function addProductNumber(string $productNumber): AddwishCart
    {
        if (!in_array($productNumber, $this->productNumbers)) {
            $this->productNumbers[] = $productNumber;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        $data['total'] = number_format($this->total, 2);
        if (null !== $this->email) {
            $data['email'] = $this->email;
        }
        $data['productNumbers'] = $this->productNumbers;

        return json_encode($data);
    }
}
