<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Registry;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Addwish\Awext\Model\Search\OrderCriteria;
use Addwish\Awext\Model\Data\OrderProvider;
use Addwish\Awext\Helper\Config as ConfigHelper;
use Addwish\Awext\Api\OrderFeedInterface;

/**
 * Class OrderFeed
 */
class OrderFeed extends AbstractFeed implements OrderFeedInterface
{
    /**
     * Default export from date value;
     */
    const DEFAULT_EXPORT_FROM_DATE = '-2 day';

    /**
     * @var string
     */
    protected $dateFrom;

    /**
     * @var string
     */
    protected $dateTo;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var OrderCriteria
     */
    protected $orderCriteria;

    /**
     * @var OrderProvider
     */
    protected $orderProvider;

    /**
     * OrderFeed constructor.
     *
     * @param Context                  $context
     * @param ConfigHelper             $configHelper
     * @param Registry                 $registry
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderProvider            $orderProvider
     * @param OrderCriteria            $orderCriteria
     * @param AbstractResource|null    $resource
     * @param AbstractDb|null          $resourceCollection
     * @param array                    $data
     */
    public function __construct(
        Context $context,
        ConfigHelper $configHelper,
        Registry $registry,
        OrderRepositoryInterface $orderRepository,
        OrderProvider $orderProvider,
        OrderCriteria $orderCriteria,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->orderRepository   = $orderRepository;
        $this->orderProvider = $orderProvider;
        $this->orderCriteria     = $orderCriteria;

        parent::__construct(
            $context,
            $registry,
            $configHelper,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Execute
     *
     * @param string $dateFrom
     * @param string $dateTo
     *
     * @return OrderFeedInterface
     */
    public function execute(string $dateFrom, string $dateTo): OrderFeedInterface
    {
        $this->init($dateFrom, $dateTo);

        $orderList   = $this->getOrderList();
        $productFeed = $this->orderProvider->generate($orderList);

        $this->populateFeedData($productFeed);

        return $this;
    }

    /**
     * Initialize data
     *
     * @param string $dateFrom
     * @param string $dateTo
     *
     * @return OrderFeedInterface
     */
    protected function init(string $dateFrom, string $dateTo): OrderFeedInterface
    {
        if ($dateFrom) {
            $dateFrom = $this->configHelper->formatDate($dateFrom, 'Y-m-d 00:00:00');
        }

        if ($dateTo) {
            $dateTo = $this->configHelper->formatDate($dateTo, 'Y-m-d 23:59:59');
        }

        $this->dateTo   = $dateTo;
        $this->dateFrom = $dateFrom;
        $currentDate    = $this->configHelper->getCurrentDate();

        if (!$this->dateTo) {
            $this->dateTo = $currentDate->format('Y-m-d 23:59:59');
        }

        if (!$this->dateFrom) {
            $currentDate->modify(self::DEFAULT_EXPORT_FROM_DATE);
            $this->dateFrom = $currentDate->format('Y-m-d 00:00:00');
        }

        return $this;
    }

    /**
     * Gets order list
     *
     * @return OrderInterface[]
     */
    protected function getOrderList(): array
    {
        $searchCriteria = $this->orderCriteria->getSearchCriteria($this->dateFrom, $this->dateTo);
        $searchResults  = $this->orderRepository->getList($searchCriteria);

        $this->setLastPageNumber($searchResults);

        return $searchResults->getItems();
    }
}
