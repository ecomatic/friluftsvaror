<?php declare(strict_types=1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Data\Collection;
use Addwish\Awext\Helper\Config as ConfigHelper;

/**
 * Class AbstractFeed
 */
abstract class AbstractFeed extends AbstractModel
{
    /**
     * Default index names
     */
    const ROOT_NODE_NAME_INDEX         = 'root_node_name';
    const LAST_PAGE_NUMBER_NODE_INDEX  = 'last_page_number';
    const EXTENSION_VERSION_NODE_INDEX = 'extension-version';
    const DEFAULT_ENTITY_ITEM_NAME     = 'item';

    /**
     * @var array
     */
    protected $feedArray = [];

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * @var
     */
    protected $lastPageNumber = 0;

    /**
     * AbstractFeed constructor.
     *
     * @param Context               $context
     * @param Registry              $registry
     * @param ConfigHelper          $configHelper
     * @param AbstractResource|null $resource
     * @param AbstractDb|null       $resourceCollection
     * @param array                 $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ConfigHelper $configHelper,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->configHelper = $configHelper;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Gets feed response
     *
     * @return array
     */
    public function getFeed(): array
    {
        $this->setFeedNode(self::LAST_PAGE_NUMBER_NODE_INDEX, (string) $this->getLastPageNumber());
        $this->setFeedNode(self::EXTENSION_VERSION_NODE_INDEX, $this->getModuleVersion());

        return $this->feedArray;
    }

    /**
     * Gets feed data
     *
     * @return array
     */
    public function getFeedData(): array
    {
        return $this->feedArray;
    }

    /**
     * Sets feed node to feedArray
     *
     * @param string $key
     * @param string $node
     *
     * @return $this
     */
    public function setFeedNode(string $key, string $node): self
    {
        if ($key) {
            $this->feedArray[0][$key] = $node;
        }

        return $this;
    }

    /**
     * Sets feed node to feedArray
     *
     * @param string $key
     * @param array  $node
     *
     * @return $this
     */
    public function setFeedNodeArray(string $key, array $node): self
    {
        if ($key) {
            $this->feedArray[0][$key] = $node;
        }

        return $this;
    }

    /**
     * Gets feed node by key
     *
     * @param string $key
     *
     * @return string
     */
    public function getFeedNode(string $key): string
    {
        if (array_key_exists($key, $this->feedArray[0][$key])) {
            return $this->feedArray[0][$key];
        }

        return '';
    }

    /**
     * Sets feed data to root node
     *
     * @param array $feedData
     *
     * @return $this
     */
    public function populateFeedData(array $feedData): self
    {
        $this->setFeedNodeArray($this->getRootNodeName(), $feedData);

        return $this;
    }

    /**
     * Sets Last Page Number
     *
     * @param SearchResultsInterface $searchResult
     *
     * @return $this
     */
    public function setLastPageNumber(SearchResultsInterface $searchResult): self
    {
        if ($searchResult instanceof Collection) {
            $this->lastPageNumber = $searchResult->getLastPageNumber();
        } else {
            $pageSize   = $searchResult->getSearchCriteria()->getPageSize();
            $totalCount = $searchResult->getTotalCount();

            if ($pageSize) {
                $this->lastPageNumber = ceil(($totalCount / $pageSize));
            }
        }

        return $this;
    }

    /**
     * Gets calculated last page number
     *
     * @return int
     */
    public function getLastPageNumber(): int
    {
        return (int) $this->lastPageNumber;
    }

    /**
     * Gets module version
     *
     * @return string
     */
    public function getModuleVersion(): string
    {
        return $this->configHelper->getModuleVersion();
    }

    /**
     * Gets root node name
     *
     * @return string
     */
    protected function getRootNodeName(): string
    {
        if ($this->isRootNodeName()) {
            return (string) $this->_data[self::ROOT_NODE_NAME_INDEX];
        }

        return self::DEFAULT_ENTITY_ITEM_NAME;
    }

    /**
     * Checking if root node name exists
     *
     * @return bool
     */
    protected function isRootNodeName(): bool
    {
        if (isset($this->_data[self::ROOT_NODE_NAME_INDEX]) && $this->_data[self::ROOT_NODE_NAME_INDEX]) {
            return true;
        }

        return false;
    }
}
