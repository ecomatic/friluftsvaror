<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Model\Search;

use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\Api\Search\SearchCriteria;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\Data\OrderInterface;
use Addwish\Awext\Helper\Config as ConfigHelper;

/**
 * Class OrderCriteria
 */
class OrderCriteria
{
    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    protected $sortOrderBuilder;

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * OrderCriteria constructor.
     *
     * @param FilterBuilder         $filterBuilder
     * @param FilterGroupBuilder    $filterGroupBuilder
     * @param SortOrderBuilder      $sortOrderBuilder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ConfigHelper          $configHelper
     */
    public function __construct(
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        SortOrderBuilder $sortOrderBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ConfigHelper $configHelper
    ) {
        $this->filterBuilder         = $filterBuilder;
        $this->filterGroupBuilder    = $filterGroupBuilder;
        $this->sortOrderBuilder      = $sortOrderBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->configHelper          = $configHelper;
    }

    /**
     * Gets search criteria
     *
     * @param string $dateFrom
     * @param string $dateTo
     *
     * @return SearchCriteria
     */
    public function getSearchCriteria(string $dateFrom, string $dateTo): SearchCriteria
    {
        $statusFilterGroup = $this->filterGroupBuilder
            ->setFilters(
                [
                    $this->filterBuilder
                        ->setField(OrderInterface::STATUS)
                        ->setConditionType('eq')
                        ->setValue(Order::STATE_COMPLETE)
                        ->create()
                ]
            )
            ->create();

        $storeFilterGroup = $this->filterGroupBuilder
            ->setFilters(
                [
                    $this->filterBuilder
                        ->setField(OrderInterface::STORE_ID)
                        ->setValue($this->configHelper->getStoreId())
                        ->setConditionType('eq')
                        ->create()
                ]
            )
            ->create();

        $dateFromFilterGroup = $this->filterGroupBuilder
            ->setFilters(
                [
                    $this->filterBuilder
                        ->setField(OrderInterface::CREATED_AT)
                        ->setConditionType('from')
                        ->setValue($dateFrom)
                        ->create()
                ]
            )
            ->create();

        $dateToFilterGroup = $this->filterGroupBuilder
            ->setFilters(
                [
                    $this->filterBuilder
                        ->setField(OrderInterface::CREATED_AT)
                        ->setConditionType('to')
                        ->setValue($dateTo)
                        ->create()
                ]
            )
            ->create();

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $searchCriteria->setSortOrders(
            [
                $this->sortOrderBuilder
                    ->setField(OrderInterface::CREATED_AT)
                    ->setAscendingDirection()
                    ->create()
            ]
        );

        $searchCriteria->setFilterGroups(
            [
                $statusFilterGroup,
                $storeFilterGroup,
                $dateFromFilterGroup,
                $dateToFilterGroup
            ]
        );

        return $searchCriteria;
    }
}
