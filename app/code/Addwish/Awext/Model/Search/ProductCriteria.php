<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Model\Search;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\Api\Search\SearchCriteria;
use Magento\Catalog\Model\Product\VisibilityFactory;
use Magento\Catalog\Model\Product\Attribute\Source\StatusFactory;
use Magento\Store\Model\Store;
use Addwish\Awext\Helper\Config as ConfigHelper;

/**
 * Class ProductCriteria
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductCriteria
{
    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    protected $sortOrderBuilder;

    /**
     * @var VisibilityFactory
     */
    protected $visibilityFactory;

    /**
     * @var StatusFactory
     */
    protected $statusFactory;

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * ProductCriteria constructor.
     *
     * @param FilterBuilder         $filterBuilder
     * @param FilterGroupBuilder    $filterGroupBuilder
     * @param SortOrderBuilder      $sortOrderBuilder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param VisibilityFactory     $visibilityFactory
     * @param StatusFactory         $statusFactory
     * @param ConfigHelper          $configHelper
     */
    public function __construct(
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        SortOrderBuilder $sortOrderBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        VisibilityFactory $visibilityFactory,
        StatusFactory $statusFactory,
        ConfigHelper $configHelper
    ) {
        $this->filterBuilder         = $filterBuilder;
        $this->filterGroupBuilder    = $filterGroupBuilder;
        $this->sortOrderBuilder      = $sortOrderBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->visibilityFactory     = $visibilityFactory;
        $this->statusFactory         = $statusFactory;
        $this->configHelper          = $configHelper;
    }

    /**
     * Gets search criteria
     *
     * @param int|null $currentPage
     * @param int|null $pageSize
     *
     * @return SearchCriteria
     */
    public function getSearchCriteria(int $currentPage = null, int $pageSize = null): SearchCriteria
    {
        $statusModel     = $this->statusFactory->create();
        $visibilityModel = $this->visibilityFactory->create();

        $statusFilterGroup = $this->filterGroupBuilder
            ->setFilters(
                [
                    $this->filterBuilder
                        ->setField(ProductInterface::STATUS)
                        ->setConditionType('in')
                        ->setValue($statusModel->getVisibleStatusIds())
                        ->create()
                ]
            )->create();

        $visibilityFilterGroup = $this->filterGroupBuilder
            ->setFilters(
                [
                    $this->filterBuilder
                        ->setField(ProductInterface::VISIBILITY)
                        ->setConditionType('in')
                        ->setValue($visibilityModel->getVisibleInSiteIds())
                        ->create()
                ]
            )->create();

        $storeFilterGroup = $this->filterGroupBuilder
            ->setFilters(
                [
                    $this->filterBuilder
                        ->setField(Store::STORE_ID)
                        ->setValue($this->configHelper->getStoreId())
                        ->setConditionType('eq')
                        ->create()
                ]
            )->create();

        $searchCriteria = $this->searchCriteriaBuilder->create();

        if ($currentPage) {
            $searchCriteria->setCurrentPage($currentPage);
        }

        if ($pageSize) {
            $searchCriteria->setPageSize($pageSize);
        }

        $searchCriteria->setFilterGroups(
            [
                $statusFilterGroup,
                $visibilityFilterGroup,
                $storeFilterGroup
            ]
        );

        return $searchCriteria;
    }
}
