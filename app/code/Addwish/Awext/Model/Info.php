<?php declare(strict_types=1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Model;

use Addwish\Awext\Helper\Config as ConfigHelper;
use Addwish\Awext\Api\InfoInterface;

/**
 * Class Info
 */
class Info implements InfoInterface
{
    /**
     * Node names
     */
    const INFO_NODE    = 'info';
    const VERSION_NODE = 'version';

    /**
     * @var array
     */
    protected $nodeArray = [];

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * Info constructor.
     *
     * @param ConfigHelper $configHelper
     */
    public function __construct(ConfigHelper $configHelper)
    {
        $this->configHelper = $configHelper;
    }

    /**
     * Gets module info
     *
     * @return array
     */
    public function getModuleInfo(): array
    {
        $version = $this->configHelper->getModuleVersion();

        $this->setNode(self::VERSION_NODE, $version);

        return [self::INFO_NODE => $this->nodeArray];
    }

    /**
     * Sets node to nodeArray
     *
     * @param string $key
     * @param string $value
     *
     * @return InfoInterface
     */
    public function setNode(string $key, string $value): InfoInterface
    {
        $this->nodeArray[$key] = $value;

        return $this;
    }

    /**
     * Sets node array  to nodeArray
     *
     * @param string $key
     * @param array  $value
     *
     * @return InfoInterface
     */
    public function setNodeArray(string $key, array $value): InfoInterface
    {
        $this->nodeArray[$key] = $value;

        return $this;
    }

    /**
     * Gets node by key
     *
     * @param string $key
     *
     * @return array
     */
    public function getNode(string $key): array
    {
        if (array_key_exists($key, $this->nodeArray[$key])) {
            return $this->nodeArray[$key];
        }

        return [];
    }
}
