<?php declare(strict_types = 1);
/**
 * @author         Wojtek Brożyna
 * @package        Addwish\Awext 
 * @copyright      Copyright(C) 2018 Addwish
 * @license        See LICENSE.txt for license details.
 */

namespace Addwish\Awext\Plugin\Product;

use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Store\Model\Store;
use Closure;

/**
 * Class Collection
 *
 * This plugin fixes issue with filtration product collection by store_id
 * in \Magento\Catalog\Model\ProductRepository::getList()
 *
 * Please removed it  when the problem will be solved by the magento team.
 */
class Collection
{
    /**
     * Adding filtration by store_id. Supported 'eq' condition.
     *
     * @param ProductCollection $subject
     * @param Closure           $proceed
     * @param array             $fields
     * @param string|null       $condition
     *
     * @return ProductCollection
     */
    public function aroundAddFieldToFilter(ProductCollection $subject, Closure $proceed, $fields, $condition = null)
    {
        if (is_array($fields) && count($fields)) {
            foreach ($fields as $key => $filter) {
                if ($filter['attribute'] == Store::STORE_ID && isset($filter['eq'])) {
                    $subject->addStoreFilter($filter['eq']);
                    unset($fields[$key]);
                }
            }
        }

        if (count($fields)) {
            return $proceed($fields, $condition);
        }

        return $subject;
    }
}
