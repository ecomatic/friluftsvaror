require(
    ['jquery', 'domReady'],
    function($, domReady) {
        domReady(function () {
            $.get(
                '/rest/V1/awext/cart',  function (data) {
                    if (data.total != 0 || data.total != null) {
                        data = JSON.parse(data);
                        ADDWISH_PARTNER_NS.api.cart.setCart(data);
                    }
                }
            );
            console.log('Called');
            return {};
        });
    }
);