<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
 */


namespace NWT\KCO\Model;

/**
 * Klarna Push (history) model
 */

class Push extends  \Magento\Framework\Model\AbstractModel
{

    const ERR_KLARNA_FETCH              = 100; //cannot fetch klarna order

    /**
     * Define resource model
     */
     
    public function _construct() {
        
        $this->_init('NWT\KCO\Model\Resource\Push');
    }


    public function loadByKid($kID,$test,$store) {
        $pKey = $kID.'|'.($test>0?1:0).'|'.$store;
        return $this->load($pKey,'kid');
    }

    static public function getRequest($kID,$test,$store,$prefix) {

        $pKey = $kID.'|'.($test>0?1:0).'|'.$store;
        $OM = \Magento\Framework\App\ObjectManager::getInstance();
        $push = $OM->create('NWT\KCO\Model\Push')
		->load($pKey,'kid')
		->setIsAlreadyStarted(true)
		->setIsAlreadyFetched(true);


        if(!$push->getId()) {
            try {
                $currentTime =  (new \DateTime())->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT);
                $push->setKid($pKey)->setOrigin($prefix)->setCreatedAt($currentTime)->save();
                $push->setIsAlreadyStarted(false);
            } catch(\Exception $e) {
                //duplicate key? try to reload
                $push->load($pKey,'kid');
                if(!$push->getId()) {
                    //nope, no duplicate key, cannot do nothing
                    throw $e;
                }
            }
        } else {
            $push->setIsAlreadyPlaced(true);
        }

        $pushID = $push->getId();

        if(!$push->getMarshal()) { //we didn't fetched Klarna Order
            try {
                $klarnaOrder = $OM->create('NWT\KCO\Model\Klarna\Order')->setTestMode($test>0)->fetchForUpdate($kID,null);
            } catch(\Exception $e) {
		        //print_r($e->getPayload());
                throw new \Exception("Cannot fetch Klarna order [".get_class($e)."], {$e->getMessage()}",self::ERR_KLARNA_FETCH);
            }
            $klarnaData = $klarnaOrder->marshal();
            //reload push request; maybe was updated from another request
            $push->load($pushID);
            if(!$push->getMarshal()) {
                $push->setAlreadyFetched(false);
                $push->setMarshal(serialize($klarnaData))->save();
            }
        }
        return $push;
    }

    public function getAge() {
        $now  = time();
        $rup  = strtotime($this->getCreatedAt());
        $age  = round( ($now-$rup)/60, 2); //minutes
        return $age;
    }


}
