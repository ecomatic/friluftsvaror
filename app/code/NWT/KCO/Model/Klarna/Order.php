<?php
/**
 * Klarna Checkout extension
 *
 * PHP version ~5.5.0|~5.6.0|~7.0.0
 *
 * @category  NWT
 * @package   NWT_KCO
 * @author    Nordic Web Team <support@nodicwebteam.se>
 * @copyright 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license   NWT Commercial License (NWTCL 1.0)
 * @link      http://extensions.nordicwebteam.se/klarna-checkout-2.html
 */

namespace NWT\KCO\Model\Klarna;

use Magento\Framework\Exception\LocalizedException;

//KlarnaNS is Klarna Checkout API, rewriten by us to use namespaces & PhpXmlRpc 4.0, @see Lib/{KlarnaNs|PhpXmlRpc}
use NWT\KCO\Lib\KlarnaNS\Checkout\Order as KlarnaOrder;
use NWT\KCO\Lib\KlarnaNS\Checkout\Connector as KlarnaConnector;



/**
 * Klarna (Checkout) Order Model
 */

class Order extends KlarnaOrder
{

    const CUSTOMER_TYPE_COMPANY = 'organization';
    const CUSTOMER_TYPE_PERSON = 'person';



    protected $_addrFieldMap = array(    //map between Magento and Klarna address fields
        'firstname'=>'given_name',
        'lastname'=>'family_name',
        'street'=>'street_address',
        'company'=>'organization_name', 
        'city'=>'city',
        'country_id'=>'country',
        'postcode'=>'postal_code',
        'telephone'=>'phone',
        'email'=>'email',
        'prefix'=>'title',
        'care_of'=>'care_of'
    );

    protected $_addrFieldMapShort = array(    //map between Magento and Klarna address fields, for country with address stored in Klarna (not Germany)
        'postcode'=>'postal_code',
        'email'=>'email',
    );



    protected $_testMode    = true;
    protected $_quote       = null;

    /**
     * @var \NWT\KCO\Helper\Data
     */
    protected $_helper;

    /**
     * @var \NWT\KCO\Model\Klarna\Locale
     */
    protected $_klarnaLocale;


    /**
     * @var \NWT\KCO\Model\Klarna\Factory
     */
    protected $_klarnaFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;




    /**
     * Catalog product configuration
     *
     * @var \Magento\Catalog\Helper\Product\Configuration|null
     */
    protected $_productConfig = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;



    public function __construct(
        \NWT\KCO\Helper\Data $helper,
        \NWT\KCO\Model\Klarna\Factory $klarnaFactory,
        \Magento\Catalog\Helper\Product\Configuration $productConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Psr\Log\LoggerInterface $logger
    ) {

        $this->_helper          = $helper;
        $this->_klarnaFactory   = $klarnaFactory;
        $this->_klarnaLocale    = $klarnaFactory->getLocale();
        $this->_productConfig   = $productConfig;
        $this->_storeManager    = $storeManager;
        $this->_countryFactory  = $countryFactory;
        $this->_logger          = $logger;

        $this->_testMode = $this->_helper->isTestMode();


        if ($this->_testMode) {
            $connector = KlarnaConnector::create($helper->getSharedSecret(), KlarnaConnector::BASE_TEST_URL);
            // $connector->getTransport()->setOption(CURLOPT_SSL_VERIFYPEER, false); //we have a problem with test server
        } else {
            $connector = KlarnaConnector::create($helper->getSharedSecret(), KlarnaConnector::BASE_URL);
        }

        return parent::__construct($connector);
    }

    public function setTestMode($testMode) 
    {
        $this->_testMode = $testMode;
        return $this;
    }
    
    public function getLocale() 
    {
        return $this->_klarnaLocale;
    }


    public function allowCountries($store = null) 
    {

        $countries = [];

        //magento allow countries
        $mageAllowCountries = explode(',', (string)$this->_helper->getStoreConfig('general/country/allow', $store));
        
        //klarna allow countries
        $klrnAllowCountries = $this->_klarnaLocale->getCountries();
        

        $allowCountries = array_intersect($mageAllowCountries, $klrnAllowCountries);
    
    
        //get default country, from config and add as first
        $defaultCountry = $this->_helper->getCountry($store);

        if ($defaultCountry && in_array($defaultCountry, $allowCountries)) {
            $countries[] = $defaultCountry;
        }

        //add rest
        foreach($allowCountries as $country) {
            if ($country && $country != $defaultCountry) {
                $countries[] = $country;
            }
        }

        return $countries;
    }




    //change to work with location only as ID (not absolute url), used only in tests
    public function setLocation($location)   
    {
        if ($location && strpos($location, '//') === false) { //if location and does not contains // (not absolute)
            if ($this->_testMode) {
                $location = KlarnaConnector::BASE_TEST_URL."{$this->relativePath}/{$location}";
            } else {
                $location = KlarnaConnector::BASE_URL."{$this->relativePath}/{$location}";
            }
        }
        parent::setLocation($location);
        return $this;
    }

    //just add return $this
    public function create(array $data) 
    {
        parent::create($data); return $this; 
    }
    public function update(array $data) 
    {
        parent::update($data); return $this; 
    }
    public function parse(array $data)  
    {
        parent::parse($data); return $this; 
    }
    public function fetch()             
    {
        parent::fetch(); return $this; 
    }
    public function reset()             
    {
        $this->setLocation(null);$this->parse(array()); return $this; 
    }

    //getters
    public function isTestMode()    
    {
        return $this->_testMode; 
    }
    public function getQuote()      
    {
        return $this->_quote; 
    }


    public function mageAddress($klrnAddr,$withEmpty = true) 
    {


        $return = array();

        if (empty($klrnAddr['street_address']) && !empty($klrnAddr['street_name'])) {
            $klrnAddr['street_address'] =  trim($klrnAddr['street_name'].' '.(!empty($klrnAddr['street_number'])?$klrnAddr['street_number']:''));
        }
        if (!empty($klrnAddr['country'])) {
            $klrnAddr['country'] = trim(strtoupper($klrnAddr['country']));
            //From Mage2, loadByCode will automatically use iso2 or iso3_code, based on code length
            $countryId = $this->_countryFactory->create()->loadByCode($klrnAddr['country'])->getId(); 
            if (!$countryId) {
                throw new LocalizedException(__('Invalid country code [%1]', $klrnAddr['country']));
            }
            $klrnAddr['country'] = $countryId;
        }

        foreach($this->_addrFieldMap as $mageField => $klrnField) {
            if (isset($klrnAddr[$klrnField])) {
                $return[$mageField] = $klrnAddr[$klrnField];
            }  elseif ($withEmpty) {
                $return[$mageField] = '';
            } //else ... do nothing
        }
        return $return;
    }


    public function klarnaAddress($mageAddr,$forceFull = false, $country3 = false) 
    {

        $return = array();

        $country = $mageAddr->getCountryId();
        $locale  = $this->_klarnaLocale->getCountry($country);

        if (!$locale) {
            return $return;
        }

        $map = $this->_addrFieldMap;
        //not test env accept any address
//         if (!$forceFull && empty($locale['full-address'])) {
//             $map = $this->_addrFieldMapShort;
//         } else {
//             $map = $this->_addrFieldMap;
//         }

        $map = $this->_addrFieldMap;
        
        $street = explode(' ', trim($mageAddr->getStreetLine(1)));
        
        $splitStreet = (count($street)>1);

        foreach($map as $mageField => $klrnField) {
        
            $val = $mageAddr->getData($mageField);

            if (!$val) continue;

            if ($mageField == 'street' && !empty($locale['split-street'])) {
                if ($splitStreet) {
                    $no = array_pop($street);
                    $return['street_name']   = implode(' ', $street);
                    $return['street_number'] = $no;
                } else {
                    $return['street_name'] = implode(' ', $street);
                    $return['street_number'] = '';
                }
            } elseif($mageField == 'postcode') {
                $return[$klrnField] = str_replace(' ','',$val);
            } else {
                $return[$klrnField] = $val;
            }
        }

        //if care_of is not set, but we have company, set it in the care_of field
        if (isset($map['care_of']) && empty($return['care_of']) && $mageAddr->getCompany()) {
            $return['care_of'] = $mageAddr->getCompany();
        }
        
        if ($country3 && !empty($country)) {
            //this (country3) is required into JS
            $return['country'] = strtolower($this->_countryFactory->create()->load($country)->getData('iso3_code'));
        }
        


        return $return;
    }



    public function assignQuote($quote,$validate = true) 
    {

        if ($validate) {
            if (!$quote->hasItems()) {
                throw new LocalizedException(__('Empty Cart'));
            }
            if ($quote->getHasError()) {
                throw new LocalizedException(__('Cart has errors, cannot checkout'));
            }

            $billingAddress = $quote->getBillingAddress();
            $currency       = $quote->getQuoteCurrencyCode();
            $country        = $billingAddress->getCountryId();
            $locale         = $this->_klarnaLocale->getCountry($billingAddress->getCountryId());

            if (!$locale) {
                throw new LocalizedException(__("Invalid country [%1], expecting [%2]", $country, implode(', ', $this->_klarnaLocale->getCountries())));
            }


            if ($currency != $locale['currency']) {
                throw new LocalizedException(__("Invalid currency (%1, expecting %2)", $currency, $locale['currency']));
            }
        }
        $this->_quote = $quote;
        return $this;
    }



    //fetch order for update (cart items)
    public function fetchForUpdate($location,$expectedStatus = array('checkout_incomplete')) 
    {

        $expectedStatus = (array)$expectedStatus;

        $this->setLocation($location);
        $this->fetch();

        if ($expectedStatus) {
            $data = $this->marshal();
            $status = isset($data['status'])?$data['status']:'missing';
            if (!in_array($status, $expectedStatus)) {
                throw new LocalizedException(__("Status %1, expected %2", $status, implode(",", $expectedStatus)));
            }
        }
        return $this;
    }
    
    
    

    //convert quote items to Klarna cart items

    public function cartItems($quote) 
    {
        $items = array_values($this->_klarnaFactory->createItems()->fromQuote($quote)); //klarna API don't like keys
        //$this->_logger->info(print_r($items,true));
        return $items;
    }


    public function merchantInfo() 
    {

        $h = $this->_helper;
        $storeID = $this->_storeManager->getStore()->getStoreId();
        $merchInfo = array(
            'id'               => $h->getEid(),
            'terms_uri'        => $h->getBuyTermsUri(),
            'checkout_uri'     => $h->getCheckoutUrl(),
            'confirmation_uri' => $h->getCheckoutUrl('confirmation', array('kid'=>'{checkout.order.id}','test'=>$this->_testMode?1:0,'store'=>$storeID)),
            'push_uri'         => $h->getCheckoutUrl('push', array('kid'=>'{checkout.order.id}','test'=>$this->_testMode?1:0,'store'=>$storeID)),
        );

        $minAge = $h->getMinimumAge();
        if ($minAge>0) {
            $merchInfo['validation_uri'] = str_ireplace('http://', 'https://', $h->getCheckoutUrl('validation'));
        }

        return $merchInfo;
    }


    public function customerInfo($quote) 
    {


        $billing    = $quote->getBillingAddress();
        
        $country    = $billing->getCountryId();
        $locale     = $this->_klarnaLocale->getCountry($country);

        if (!$locale) {
            return array();
        }



        

        if ($billing->getPostcode()) {
            //we already have billing address set, keep current address
            $return = $this->klarnaAddress($billing);
        } elseif ($this->isTestMode() && !empty($locale['test'])) {
            $return = $locale['test'];
        } else {
            $return = array();
        } 

        if(empty($return['email'])) {
            $customer = $quote->getCustomer();
            if($customer &&  ($email = $customer->getEmail())) {
                $return['email'] = $email;
            }
        }
        
        return $return;
    }


    /**
     * Update Klarna Reservation based on customer's Quote
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return $this
     * @throws LocalizedException|\Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function updateFromQuote($quote) 
    {

        $data  = $this->marshal();
        $qSign = $this->_helper->getQuoteSignature($quote);
        


        if ($this->getLocation()) {
	    
            $kSign = !empty($data['merchant_reference']['signature'])?$data['merchant_reference']['signature']:'-1';
            
            if ($qSign == $kSign) {
                //do nothing
                return $this;
            }

        }


        $billingAddress = $quote->getBillingAddress();
        if ($quote->isVirtual()) {
            $shippingAddress = $billingAddress;
        }

        $country    = $billingAddress->getCountryId();
        $locale     = $this->_klarnaLocale->getCountry($country);
        if (!$locale) {
            throw new LocalizedException(__("Invalid country (%1, expecting %2)", $country, implode(', ', array_keys($this->_klarnaLocale->getLocales()))));
        }


        $klarnaCountry = strtoupper(empty($data['purchase_country'])?'KK':$data['purchase_country']);

        //if order was created but we have another country (user change store/country), reset the Klarna order (we will generate another one)
        if ($this->getLocation() && $klarnaCountry != $locale['country']) {
            $this->reset();
        }

        $orderData  = array();
        $cart       = array('items'=>$this->cartItems($quote));

        // fix gui
        $gui = array(
            //NOT NEED ANYMORE, KCO iframe is responsive
            //'layout'=> Mage::helper('nwtkco')->isMobile()? 'mobile':'desktop' //accept desktop or mobile
        );

        // add options for auto disable focus 
        if ($this->_helper->getDisableAutofocus()) {
            $gui['options'] = array('disable_autofocus');
        }

        //full order data
        $orderData = [
            'purchase_country'  => $locale['country'],
            'purchase_currency' => $locale['currency'],
            'locale'            => $locale['locale'],
            'merchant'          => $minfo=$this->merchantInfo($quote),
            'merchant_reference'=> $mref = array('orderid1'=>'q'.$quote->getId(),'signature'=>$qSign), //set quote id on orderid1, will be replace by order increment id
            'cart'              => $cart,
            'customer'		=> ['type' => $this->_getDefaultCustomerType()]
        ];
        
        
        
        if ($gui) {
            $orderData['gui'] = $gui;
        }

        $default_options = $this->_getDefaultOptions();
        if (!empty($default_options)) {
            $orderData['options'] = $default_options;
        }

        if (($customerInfo = $this->customerInfo($quote)) ) {
            $orderData['shipping_address'] = $customerInfo; //billing address is read only (at least in test mode)
        }


        $customer = $quote->getCustomer();
        if ($customer && $customer->getId() && $customer->getDob()) {
            $dt = substr($customer->getDob(), 0, 10); //1973-04-19 00:00:00
            if ($dt{0} != 0) {
                $orderData['customer']['date_of_birth'] = $dt;
            }
        }
        
        if ($this->getLocation()) { //already created, just update cart items
            //this will use for update an existent Klarna Order
            try {

                //$this->update(array('cart' => $cart,'gui'=>$gui)); //Klarna do not allow to update the GUI ?!?
                $this->update(array('cart' => $cart,'merchant_reference'=>$mref,'options'=>$orderData['options']));
            } catch(\Exception $e) {
                $this->_logger->critical("[".__METHOD__."] Cannot update existent order {$this->getLocation()}; {$e->getMessage()}");
                $this->_logger->critical($e);
                $this->reset(); //we will try to create another one
            }
        }


        if (!$this->getLocation()) { //new order (or update fail)
            try {
                $this->create($orderData);
            } catch(\Exception $e) {

                $this->_logger->error("[".__METHOD__."] Cannot create order: {$e->getMessage()}");
                $this->_logger->critical($e);

                if (isset($orderData['shipping_address']) || isset($orderData['customer'])) {
                    //try to create another one, without address
                    unset($orderData['shipping_address']);
                    unset($orderData['customer']);
                    $this->create($orderData);
                } else {
                    throw $e; //do nothing
                }
            }
        }

        $this->fetch();
        return $this;
    }


    public function registerOrder($orderID) 
    {

        $this->update(
            array(
                'status' => 'created',
                'merchant_reference' => array('orderid1'=>$orderID)
            )
        );
        return $this;

    }

    public function cancelReservation() 
    {

        if (!$this->getLocation()) {
            return $this;
        }

        $data = $this->marshal();
        if (empty($data['status']) || $data['status'] != 'checkout_complete' || empty($data['reservation'])) {
            return $this;
        }

        $klarnaAPI =  $this->_klarnaFactory->createApi()->initFromCountry($data['purchase_country'], $this->_testMode);
        $result = $klarnaAPI->cancelReservation($data['reservation']);
        $this->reset();
        if (!$result) {
            throw new LocalizedException(__('Failed to cancel reservation, no reason'));
        }
    }





    protected function _getDefaultOptions()
    {
        $helper = $this->_helper;
        $opt = array();

        // could make a loop, but nevermind!
        if ($helper->isNationalIdentificationNumberMandatory() || (int)$helper->getMinimumAge()>0) { //getMinimumAge test also if minimu age is required, else return 0, @see NWT_KCO_Helper_Data
            $opt['national_identification_number_mandatory'] = true;
        }

        if ($helper->getAllowSeparateShippingAddress()) {
            $opt['allow_separate_shipping_address'] = true;
        }

        if ($helper->getShippingDetailsSuccess()) {
            $opt['shipping_details'] = $helper->getShippingDetailsSuccess();
        }


        // Only hexadecimal values are allowed. The default color scheme will show if no values are set.

        if ($helper->_hexIsValid($helper->getKcoColorButton())) {
              $opt['color_button'] = $helper->getKcoColorButton();
        }

        if ($helper->_hexIsValid($helper->getKcoColorButtonText())) {
              $opt['color_button_text'] = $helper->getKcoColorButtonText();
        }      
 
        if ($helper->_hexIsValid($helper->getKcoColorCheckbox())) {
              $opt['color_checkbox'] = $helper->getKcoColorCheckbox();
        }

        if ($helper->_hexIsValid($helper->getKcoColorCheckboxCheckmark())) {
              $opt['color_checkbox_checkmark'] = $helper->getKcoColorCheckboxCheckmark();
        }

        if ($helper->_hexIsValid($helper->getKcoColorHeader())) {
              $opt['color_header'] = $helper->getKcoColorHeader();
        }  

        if ($helper->_hexIsValid($helper->getKcoColorLinks())) {
              $opt['color_link'] = $helper->getKcoColorLinks();
        }

        
        // we can enable both types (person, company) or only person, or only company. This is set here according to settings.
	$opt['allowed_customer_types']  = [];
	
        if($helper->getUseB2b()){
            $opt['allowed_customer_types'][] = self::CUSTOMER_TYPE_COMPANY;
            if($helper->getKeepB2c()){
                $opt['allowed_customer_types'][] = self::CUSTOMER_TYPE_PERSON;
            }
        } else {
            $opt['allowed_customer_types'][] = self::CUSTOMER_TYPE_PERSON;
        }         
        
        return $opt;
    }
    
    protected function _getDefaultCustomerType() {
    
        $helper = $this->_helper;

        // if b2b isn't active return person type
        if(!$helper->isB2bAvailable()){
            return self::CUSTOMER_TYPE_PERSON;
        }

        // if b2b is active and b2c isn't then return company type
        if(!$helper->getKeepB2c()){
            return self::CUSTOMER_TYPE_COMPANY;
        }

        
        // if b2b and b2c is active and we have no company info, then return the default company type (check settings)
        return $helper->getDefaultCustomerType() == self::CUSTOMER_TYPE_COMPANY ? self::CUSTOMER_TYPE_COMPANY : self::CUSTOMER_TYPE_PERSON;
    }    

}
