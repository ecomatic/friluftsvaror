<?php

/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 * 
 *
 */

namespace NWT\KCO\Model\Klarna;

/**
 * Klarna (Checkout) Order Model
 */

class Items 
{


   /**
     * @var \NWT\KCO\Helper\Data
     */
    protected $_helper;


    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;



    /**
     * Catalog product configuration
     *
     * @var \Magento\Catalog\Helper\Product\Configuration|null
     */
    protected $_productConfig = null;
    
    

    public function __construct(
        \NWT\KCO\Helper\Data $helper,
        \Magento\Catalog\Helper\Product\Configuration $productConfig,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_helper = $helper;
        $this->_productConfig = $productConfig;
        $this->_logger = $logger;
    }
    
    
    protected $_cart     = array();
    protected $_discounts = array();
    protected $_maxvat = 0;
    protected $_inclTAX = false;
    protected $_toInvoice = false;
    
    protected $_store = null;


    public function cart()             { return $this->_cart;     }
    public function discounts()        { return $this->_discounts; }
    public function maxvat()           { return $this->_maxvat;    }
    
    
    

    public function init($store = null) {

        
        $this->_store = $store;
        $this->_cart = array();
        $this->_discounts = array();
        $this->_maxvat = 0;
        $this->_inclTAX = false;
        $this->_toInvoice = false;
        
        return $this;
    }

    public function addItems($items) {

        $sendParent = $this->_helper->sendParent($this->_store);
        $isQuote = null;

        foreach($items as $item) {

            if(is_null($isQuote)) {
                $isQuote =  ($item instanceof \Magento\Quote\Model\Quote\Item );
            }

            $oid = $item->getData('order_item_id');
            if($oid) { //invoice or creditmemo item
                $mainItem = $item->getOrderItem();
            } else { //quote or order item
                $mainItem = $item;
            }
            
            if($mainItem->getParentItemId() || $mainItem->isDeleted()) continue; 
            
            if($item instanceof \Magento\Sales\Model\Order\Item ) {
                $qty = $item->getQtyOrdered();
                if($this->_toInvoice) {
                    $qty -= $item->getQtyInvoiced();
                }
                $item->setQty($qty);
            }

            

  
            $allItems             = array();
            $bundle               = false;
            $isChildrenCalculated = false;
            $parentQty            = 1;

            //for bundle product, want to add also the bundle, with price 0 if is children calculated
            if($mainItem->getProductType() == 'bundle'  || ($mainItem->getHasChildren() && $mainItem->isChildrenCalculated())) {

                $bundle               = true;

                $isChildrenCalculated = $mainItem->isChildrenCalculated();                

                if($isChildrenCalculated) {
                    if(!$sendParent || ($this->_toInvoice && $item->getQty()<=0)) {
                        $parentComment = null;
                    } else {
                        $allItems[] = $item; //list also parent (bundle product);
                        $parentComment         = "Bundle Product,  ".round($item->getQty(),0)." x ".round($item->getPriceInclTax(),2);
                    }
                    if($isQuote) {
                        $parentQty = $item->getQty();
                        //this is required only in quote (children qty is not parent * children)
                        //in order is already multiplied
                    }
                } else {
                    $allItems[] = $item; //add bundle product
                    $parentComment         = "Bundle Product";
                }

                $children = $this->getChildrenItems($item); 
                if($children) {
                    foreach($children as $child) {
                        if($child->isDeleted()) continue;
                        $allItems[] = $child;
                    }
                }
            } else {
                //simple product
                $allItems[] = $item;
            }


            foreach($allItems as $item) {
            
                $oid = $item->getData('order_item_id');
                if($oid) { //invoice or creditmemo item
                    $mainItem = $item->getOrderItem();
                } else { //quote or order item
                    $mainItem = $item;
                } 
                if($item instanceof \Magento\Sales\Model\Order\Item ) {
                    $qty = $item->getQtyOrdered();
                    if($this->_toInvoice) {
                        $qty -= $item->getQtyInvoiced();
                    }
                    if($qty == 0) continue;
                    $item->setQty($qty);
                }
                
                $addPrices = true;


                if($bundle) {
                    if(!$mainItem->getParentItemId()) { //main product, add prices if not children calculated
                        $comment = $parentComment;
                        $addPrices = !$isChildrenCalculated;
                    } else { //children, add price only if children calculated
                        $comment = '';
                        $addPrices = $isChildrenCalculated;
                    }
                } else {

                    $comment = array();
                    //add configurable/children information, as comment
                    if($isQuote) {
                        $options = $this->_productConfig->getOptions($item);
                    } else {
                        $options = null;
                    } 
                   
                    if ( $options ) {
                        foreach($options as $option) {
                            if(isset($option['label']) && isset($option['value'])) {
                                $comment[] = $option['label'] . ' : ' . $option['value'];
                            }
                        }
                    }

                    $comment = implode('; ',$comment);
                }

                $vat = $mainItem->getTaxPercent();


                if($addPrices && ($item->getTaxAmount() != 0) && ($vat == 0)) {
                    //calculate vat if not set
                    $tax = $item->getPriceInclTax() - $item->getPrice();
                    if($item->getPrice() != 0 && $tax != 0) {
                        $vat = $tax / $item->getPrice() * 100;
                    }
                }
                $vat = round($vat,0);
                if($vat>$this->_maxvat) $maxvat = $vat;




                //$items with parent id are children of a bundle product;
                //if !$withPrice, add just bundle product (!$getParentId) with price,
                //the child will be without price (price = 0)
                
                $qty = $item->getQty();
                
                if($isQuote && $item->getParentItemId()) {
                    $qty = $qty*$parentQty; //parentQty will be != 1 only for quote, when item qty need to be multiplied with parent qty (for bundle)
                }
                
                $sku    = $item->getSku();
                
                //make sku unique (sku could not be unique when we have product with options)
                if(isset($this->_cart[$sku])) {
                    $sku = $sku.'-'.$item->getId();
                }

                $this->_cart[$sku] = array(
                    //need also the ID in reference because we could have same products (SKU) multiple times (as configurable child) ... or NOT ?!?
                    //remove id from reference because specter compatibility
                    'reference'     =>$sku,
                    'name'          =>$item->getName()." ".($comment?"({$comment})":""),
                    'quantity'      =>round($qty,0), //klarna doesn't accept decimal qty?!?
                    'unit_price'    =>$addPrices?round($item->getPriceInclTax() * 100,0):0, //Klarna need prices in cents
                    'discount_rate' =>0, //discount will be set on distinct row
                    'tax_rate'      => intval($vat * 100) //need to be integer, then * 100
                );



                if($addPrices) {

                    //keep discounts grouped by VAT
                    //if catalog prices include tax, then discount INCLUDE TAX (tax coresponding to that discount is set onto discount_tax_compensation_amount)
                    //if catalog prices exclude tax, alto the discount excl. tax
                    

                    $discountAmount = $item->getDiscountAmount();
                    if($this->_toInvoice) {
                        $discountAmount -= $item->getDiscountInvoiced(); //remaining discount
                    }

                    if($discountAmount != 0) {
                    
                        //check if Taxes are applied BEFORE or AFTER the discount
                        //if taxes are applied BEFORE the discount we have row_total_incl_tax = row_total+tax_amount
                        
                        if($vat != 0 && abs($item->getRowTotalInclTax() - ($item->getRowTotal()+$item->getTaxAmount())) < .001) {  
                           //add discount without VAT (is not OK for EU, but, it is customer setting/choice
                            $vat =0;
                        }

                        if(!isset($this->_discounts[$vat])) {
                                $this->_discounts[$vat] = 0;
                        }
                        
                        
                        if($vat != 0 && $item->getDiscountTaxCompensationAmount() == 0) { //discount without taxes, we want discount INCL taxes
                            $discountAmount += $discountAmount*$vat/100;
                        }
                        
                        $this->_discounts[$vat] +=  $discountAmount; //keep products discount, per tax percent
                    }
                }
            }
        }
        return $this;
    }
    
    
    
    public function addShipping($address) {
        
        if($this->_toInvoice && $address->getBaseShippingAmount() <= $address->getBaseShippingInvoiced()) {
            return $this;
        }

        
        $exclTax    = $address->getShippingAmount();
        $inclTax    = $address->getShippingInclTax();

        $tax        = $inclTax-$exclTax;

        if($exclTax != 0 && $tax > 0) {
            $vat = $tax /  $exclTax  * 100;
        } else {
            $vat = 0;
        }
            
        $vat = round($vat,0);
        if($vat>$this->_maxvat) $this->_maxvat = $vat;
        

        $shippingSKU = trim($this->_helper->getShippingSku($this->_store));
        if(!$shippingSKU) {
            $shippingSKU = 'shipping_fee';
        }

        $this->_cart[$shippingSKU] = array(
            'type'=>'shipping_fee',
            'reference'=>$shippingSKU,
            'name'=>(string)__('Shipping Fee (%1)',$address->getShippingDescription()),
            'quantity'=>1,
            'unit_price'=>round($inclTax * 100,0),
            'discount_rate'=>0,
            'tax_rate'=> intval($vat*100),
        );

        //keep discounts grouped by VAT
    
        //if catalog prices include tax, then discount INCLUDE TAX (tax coresponding to that discount is set onto shipping_discount_tax_compensation_amount)
        //if catalog prices exclude tax, alto the discount excl. tax
        
        $discountAmount = $address->getShippingDiscountAmount();

        if($discountAmount != 0) {

            //check if Taxes are applied BEFORE or AFTER the discount
            //if taxes are applied BEFORE the discount we have shipping_incl_tax = shipping_amount + shipping_tax_amount
            if($vat != 0 && abs($address->getShippingInclTax() - ($address->getShippingAmount()+$address->getShippingTaxAmount())) < .001) {
                //the taxes are applied BEFORE discount; add discount without VAT (is not OK for EU, but, is customer settings
                $vat =0;
            }
                
        
            if(!isset($this->_discounts[$vat])) {
                $this->_discounts[$vat] = 0;
            }
            if($vat != 0 && $address->getShippingDiscountTaxCompensationAmount() == 0) {   //prices (and discount) EXCL taxes, 
                $discountAmount += $discountAmount*$vat/100;
            } 
            $this->_discounts[$vat] += $discountAmount;
        }
        return $this;
    }
    
    
    public function addDiscounts($couponCode) {
    
        
       
       
       foreach($this->_discounts as $vat=>$amount) {

            if($amount==0) continue;
            
            $reference  = 'discount'.(int)$vat;
            if($this->_toInvoice) {
                $reference = 'discount-toinvoice';
            }
            
            $this->_cart[$reference] = array(
                'type'=>'discount',
                'reference'=>$reference,
                'name'=>$couponCode?(string)__('Discount (%1)',$couponCode):(string)__('Discount'),
                'quantity'=>1,
                'unit_price'=>round($amount * 100,0)*-1,
                'discount_rate'=>0,
                'tax_rate'=> $vat*100,
            );
        }
        
        return $this;
        
    }
   
   
    public function addTotals($grandTotal, $taxAmount) {
    
    
    
       //calculate Klarna total
       //WARNING:   The tax must to be applied AFTER discount and to the custom price (not original)
       //           else... the Klarana tax total will differ

       $calculatedTotal = 0;
       $calculatedTax   = 0;
       foreach($this->_cart as $item) {
           //the algorithm used by Klarna seems to be (need to confirm with Klarna)
           //total_price_including_tax = unit_price*quantity; //no round because klarna doesn't have decimals; all numbers are * 100
           //total_price_excluding_tax = total_price_including_tax / (1+taxrate/100000) //is 10000 because taxrate is already multiplied by 100
           //total_tax_amount = total_price_including_tax - total_price_excluding_tax
            $total_price_including_tax = $item['unit_price'] * $item['quantity'];
            if($item['tax_rate'] != 0) {
                $total_price_excluding_tax = round($total_price_including_tax / (1+$item['tax_rate']/10000),0); //   total/1.25
            } else {
                $total_price_excluding_tax = $total_price_including_tax;
            }
            $total_tax_amount = round($total_price_including_tax - $total_price_excluding_tax,0); //round is not required, alreay int
            $calculatedTax   += $total_tax_amount;
            $calculatedTotal += $total_price_including_tax;
       }


        //quote/order/invoice/creditmemo total taxes
        
        

       $taxAmount  = round($taxAmount*100,0);
       $grandTotal = round($grandTotal*100,0);
       
       //echo "Required Total: {$grandTotal}, Tax: {$taxAmount}\n";
       //echo "Calculated Total: {$calculatedTotal}, Tax: {$calculatedTax}\n";
       

       
       
       $diffTaxAmount = $taxAmount-$calculatedTax;
       $difference    = $grandTotal-$calculatedTotal;

       
        
       if($difference == 0) {
            return $this; //no correction require
       }
       
       
       $totalsDescription = '';

       $maxVat = $this->_maxvat;
            

        //try to find some custom totals
        if(abs($difference) > 100) { //difference seems to be too high, maybe we have some custom totals? Add to description (100 mean 1kr, under this we suppose that is rounding)
            $description = (string)__('Other(s)');
        } else {
            $description = (string)__('Regulation');
        }
        
        $reference = 'others';
        if($this->_toInvoice) {
            $reference .= '-toinvoice';
        }
        


       if(abs($diffTaxAmount) > 1 && $maxVat != 0) { //do not compare with 0; 1 mean 0.01 (all prices are multiplied by 100)
            //if we have difference on taxes, add a correction line with max vat percent
            //we don't know any method to find wich custom totals contains tax and what vat rate
            //use maximum percent
            $diffExclTax = $diffTaxAmount/$maxVat;
            $diffInclTax = $diffExclTax+$diffExclTax*$maxVat/100;
            $unitPrice   = round($diffInclTax*100,0); //need price * 100;
            //check if we have tax, after adding this
            $unitPriceExcludingTax = round($unitPrice / (1+$maxVat/100),0); //   total/1.25
            $tax = $unitPrice-$unitPriceExcludingTax;
            if(abs($tax) > 1) { //do not compare with 0; 1 mean 0.01kr; if vat is 0, don't need to add, we don't got tax regulation
                $this->_cart[$reference] = array(
                    'type'=>'discount',
                    'reference'=>$reference,
                    'name'=>$description,
                    'quantity'=>1,
                    'unit_price'=>$unitPrice,
                    'discount_rate'=>0,
                    'tax_rate'=> intval($maxVat*100)
                );
                $calculatedTotal += $unitPrice;
            }
       }

       //if tax wasn't fixed above (maxVat = 0), we will still have difference on tax, but... don't know what to do, I don't have a vat percent
       $difference =  $grandTotal-$calculatedTotal;
       
       //if still have difference, probably we have custom totals without taxes
       if(abs($difference) > 0) { //fix totals to match magento total
            $reference = 'others0';
            if($this->_toInvoice) {
                $reference .= '-toinvoice';
            }            
            //add a line to have same total in Klarna
             $this->_cart[$reference] = array(
                'type'=>'discount',
                'reference'=>$reference,
                'name'=>$description,
                'quantity'=>1,
                'unit_price'=>$difference,
                'discount_rate'=>0,
                'tax_rate'=> 0
            );
       }
       return $this;
    }

    
   

    
    
    protected $_itemsArray = array();
    
    
    /**
     * Getting all available children for Invoice, Shipment or CreditMemo item
     *
     * @param \Magento\Framework\DataObject $item
     * @return array
     */
    public function getChildrenItems($item)
    {
        $itemsArray = [];

        $items = null;
        if ($item instanceof \Magento\Sales\Model\Order\Invoice\Item) {
            $parentId = 'INV'.$item->getInvoice()->getId();
            if(!isset($this->_itemsArray[$parentId])) {
                $this->_itemsArray[$parentId] = array();
                $items = $item->getInvoice()->getAllItems();
            } 
        } elseif ($item instanceof \Magento\Sales\Model\Order\Shipment\Item) {
            $parentId = 'SHIP'.$item->getShipment()->getId();
            if(!isset($this->_itemsArray[$parentId])) {
                $this->_itemsArray[$parentId] = array();
                $items = $item->getShipment()->getAllItems();
            }
        } elseif ($item instanceof \Magento\Sales\Model\Order\Creditmemo\Item) {
            $parentId = 'CRDM'.$item->getCreditmemo()->getId();
            if(!isset($this->_itemsArray[$parentId])) {
                $this->_itemsArray[$parentId] = array();
                $items = $item->getCreditmemo()->getAllItems();
            }
        } elseif ($item instanceof \Magento\Sales\Model\Order\Item)  {
            return $item->getChildrenItems();
        } else { //quote
            return  $item->getChildren(); 
        }
        
        

        if ($items) {
            foreach ($items as $value) {
                $parentItem = $value->getOrderItem()->getParentItem();
                if ($parentItem) {
                    $this->_itemsArray[$parentId][$parentItem->getId()][$value->getOrderItemId()] = $value;
                } /*else {
                    //we want only children (parent is already added), this is why this is commented
                    $this->_itemsArray[$parentId][$value->getOrderItem()->getId()][$value->getOrderItemId()] = $value;
                }*/
            } 
        }

        if (isset($this->_itemsArray[$parentId][$item->getOrderItem()->getId()])) {
            return $this->_itemsArray[$parentId][$item->getOrderItem()->getId()];
        } else {
            return array();
        }
    }



    //generate Klarna items from Magento Quote
    public function fromQuote($quote) {



        $this->init($quote->getStore());


        $billingAddress = $quote->getBillingAddress();
        if($quote->isVirtual()) {
            $shippingAddress = $billingAddress;
        } else {
            $shippingAddress = $quote->getShippingAddress();
        }

         /*Get all cart items*/
        $cartItems = $quote->getAllVisibleItems(); //getItemParentId is null and !isDeleted

        $this->addItems($cartItems);

        if(!$quote->isVirtual()) {
            $this->addShipping($shippingAddress);
        }

        $this->addDiscounts($quote->getCouponCode());
        $this->addTotals($quote->getGrandTotal(),$shippingAddress->getTaxAmount());

        //$this->_logger->info(print_r($this->_cart,true));
        return $this->_cart;
    }


    //generate Klarna items from Magento Order
    public function fromOrder($order) {



        $this->init($order->getStore());


        $this->addItems($order->getAllItems())
              ->addShipping($order)
              ->addDiscounts($order->getCouponCode())
              ->addTotals($order->getGrandTotal(),$order->getTaxAmount());

        return $this->_cart;
    }

 

    //generate Klarna items from Magento Invoice
    public function fromInvoice($invoice) {


        $order  = $invoice->getOrder();

        $this->init($order->getStore())
             ->addItems($invoice->getAllItems());
        

        if($invoice->getShippingAmount() != 0 && $order->getShippingDiscountAmount() !=0 && $invoice->getShippingDiscountAmount() == 0) {
            //copy discount shipping discount amount from order (because is not copied to the invoice)
            $oShippingDiscount = $order->getShippingDiscountAmount();
            $iShipping = $invoice->getShippingAmount();
            $oShipping = $order->getShippingAmount();
            
            if($iShipping != $oShipping && $oShipping>0) { //this cannot happens, but...  if yes, we will adjust shipping discount amoutn
                $oShippingDiscount = round($iShipping*$oShippingDiscount/$oShipping,4);
            }
            $invoice->setShippingDiscountAmount($oShippingDiscount);
        }
        if($invoice->getShippingAmount() != 0) {
            $this->addShipping($invoice);
        }
        
        $this->addDiscounts($order->getCouponCode()) //coupon code is not copied to invoice
             ->addTotals($invoice->getGrandTotal(),$invoice->getTaxAmount());

        return $this->_cart;
    }
    
    


}
