<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 * 
 *
 */

namespace NWT\KCO\Model\Klarna;

use Magento\Framework\Exception\LocalizedException;

//KlarnaNS is Klarna Order API, rewriten by us to use namespaces & PhpXmlRpc 4.0, @see Lib/{KlarnaNs|PhpXmlRpc}
use NWT\KCO\Lib\KlarnaNS\Api\Klarna;
use NWT\KCO\Lib\KlarnaNS\Api\KlarnaCountry;
use NWT\KCO\Lib\KlarnaNS\Api\KlarnaCurrency;
use NWT\KCO\Lib\KlarnaNS\Api\KlarnaLanguage;


/**
 * Klarna API Model 
 */
class Api extends Klarna
{


    protected $_helper;

    public function __construct(
        \NWT\KCO\Helper\Data $helper
    ) {
        $this->_helper = $helper;
        parent::__construct();
    }


    public function initFromCountry($countryCode,$test,$store = null) {

        $country     = KlarnaCountry::fromCode($countryCode);

        if(!$country) {
            throw new LocalizedException(__('Country [%1] not allowed!.',$countryCode));
        }
        $currency           = KlarnaCountry::getCurrency($country);
        $language           = KlarnaCountry::getLanguage($country);


        $this->config(
                $this->_helper->getEid($store),
                $this->_helper->getSharedSecret($store),
                $country,
                $language,
                $currency,
                $test?Klarna::BETA:Klarna::LIVE,
                'json',
                './pclasses.json'
        );
        if(!$this->getCountry()) {
            //country, language, currency will be set null (instead of throwException) if is wrong, in Klarna::init
            throw new LocalizedException(__("Country [%1] not allowed!",$countryCode));
        }
        return $this;
    }


    public function initFromOrder($order)
    {

       $payment = $order->getPayment();
       if(!$payment->getAdditionalInformation('reservation')) {
           throw new LocalizedException(__("Invalid payment, missing reservation number."));
       }


       $order   = $payment->getOrder();
       $test    = $payment->getAdditionalInformation('test');


        $countryCode = $order->getBillingAddress()->getCountryId();
        $country     = KlarnaCountry::fromCode($countryCode);

        if(!$country) {
            throw new LocalizedException(__('Country [%1] not allowed!',$countryCode));
        }

        $currency           = KlarnaCountry::getCurrency($country);
        $currencyCode       = KlarnaCurrency::getCode($currency);
        $orderCurrencyCode  = $order->getOrderCurrencyCode();


        if(trim(strtoupper($currencyCode)) != trim(strtoupper($orderCurrencyCode))) {
            throw new LocalizedException(__('Currency [%1] not allowed (expecting %2)',$orderCurrencyCode,$currencyCode));
        }


        $language = KlarnaCountry::getLanguage($country);
        $languageCode = KlarnaLanguage::fromCode($language);

        $store = $order->getStore();


        $this->config(
                $this->_helper->getEid($store),
                $this->_helper->getSharedSecret($store),
                $country,
                $language,
                $currency,
                $test?Klarna::BETA:Klarna::LIVE,
                'json',
                './pclasses.json'
        );
        if(!$this->getCountry()) {
            //country, language, currency will be set null (instead of throwException) if is wrong, in Klarna::init
            throw new LocalizedException(__("Country / Currency / Language not allowed %1/%2/%3",$countryCode,$orderCurrencyCode,$languageCode));
        }
        return $this;
    }
    
    //for debuging, expodse protedcted goodList member
    public function goodList() {
        return $this->goodsList;
    }


}
