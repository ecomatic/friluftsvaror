<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 *
 *
 */


namespace NWT\KCO\Model\Klarna;


/**
 * Factory class for \NWT\KCO\Model\Klarna\{Api|Order|Items}
 */
class Factory
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Factory constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param string $className
     * @param array $data
     * @return \NWT\KCO\Model\Klarna\Api
     */
    public function createApi(array $data = [])
    {
 
        return $this->_objectManager->create('\NWT\KCO\Model\Klarna\Api', $data);
    }
  
     /**
     * Create class instance with specified parameters
     *
     * @param string $className
     * @param array $data
     * @return \NWT\KCO\Model\Klarna\Order
     */
    public function createOrder(array $data = [])
    {
 
        return $this->_objectManager->create('\NWT\KCO\Model\Klarna\Order', $data);
    }
    
  
     /**
     * Create class instance with specified parameters
     *
     * @param string $className
     * @param array $data
     * @return \NWT\KCO\Model\Klarna\Items
     */
    public function createItems(array $data = [])
    {
 
        return $this->_objectManager->create('\NWT\KCO\Model\Klarna\Items', $data);
    }    
    
     /**
     * Get class instance with specified parameters
     *
     * @param string $className
     * @param array $data
     * @return \NWT\KCO\Model\Klarna\Locale
     */
    public function getLocale(array $data = [])
    {
 
        return $this->_objectManager->get('\NWT\KCO\Model\Klarna\Locale', $data);
    }
}


