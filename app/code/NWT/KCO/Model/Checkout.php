<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
 */


namespace NWT\KCO\Model;

use NWT\KCO\Exception as NWT_KCO_Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

use Magento\Framework\DataObject;
use Magento\Customer\Api\Data\GroupInterface;



/**
 * Klarna (Onepage) Checkout Model
 */
class Checkout extends \Magento\Checkout\Model\Type\Onepage
{

   protected $_paymentMethod = 'nwtkco';
   protected $_klarnaOrder    = null;
   protected $_allowCountries = null;
   protected $_nwtkcoHelper   = null;
   
   protected $_objectManager = null;
   

   protected $_doNotMarkCartDirty  = false;


   
   
   
    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession; //@see Onepage::__construct
    }
    
    public function getObjectManager() {
      if(is_null($this->_objectManager)) {
	$this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      }
      return $this->_objectManager;
    }

    public function getKlarnaOrder()         
    {
      
      if(is_null($this->_klarnaOrder)) {
	        $this->_klarnaOrder = $this->getObjectManager()->get('NWT\KCO\Model\Klarna\Order');
      }
      return $this->_klarnaOrder; 
    }
    
    public function getKlarnaLocale() 
    {
        return $this->getKlarnaOrder()->getLocale();
    }
    

    public function allowCountries() {
      if(is_null($this->_allowCountries)) {
        $this->_allowCountries = $this->helper()->getCountries();
      }
      return $this->_allowCountries; 
    }

    public function helper() 
    {

    
      if(is_null($this->_nwtkcoHelper)) {
	  $this->_nwtkcoHelper = $this->getObjectManager()->get('NWT\KCO\Helper\Data');
      }
      return $this->_nwtkcoHelper;
    }
    
    public function paymentMethod()
    {
      return $this->_paymentMethod;
    }
    
    
    
    public function isEnabled() 
    { 
      return $this->helper()->isEnabled(); 
    }


    protected function throwRedirectToCartException($message) { throw new NWT_KCO_Exception($message,'checkout/cart'); }
    protected function throwReloadException($message)         { throw new NWT_KCO_Exception($message,'*/*'); }


    public function setDoNotMarkCartDirty() {   $this->_doNotMarkCartDirty = true; }
    public function getDoNotMarkCartDirty() {   return $this->_doNotMarkCartDirty; }



   public function initCheckout($reloadIfCurrencyChanged = true) {

        $quote  = $this->getQuote();


        $this->checkCart();


        //init checkout
        $customer = $this->getCustomerSession();
       if($customer->getId()) {
            //$this->_logger->info(__("Set customer %1",$customer->getId()));
            $quote->assignCustomer($customer->getCustomerDataObject()); //this will set also primary billing/shipping address as billing address 
            //$quote->setCustomer($customer->getCustomerDataObject());
            //TODO: set customer address
        }

        $allowCountries = $this->allowCountries(); //this is not null (it is checked into $this->checkCart())

        $blankAddress = $this->getBlankAddress($allowCountries[0]);

        $billingAddress  = $quote->getBillingAddress();
        if($quote->isVirtual()) {
            $shippingAddress = $billingAddress;
        } else {
            $shippingAddress = $quote->getShippingAddress();
        }

        if (!$shippingAddress->getCountryId()) {
            $this->_logger->info(__("No country set, change to %1",$allowCountries[0]));
            $this->changeCountry($allowCountries[0],$save = false);
        } elseif(!in_array($shippingAddress->getCountryId(),$allowCountries)) {
            $this->_logger->info(__("Wrong ountry set %1, change to %2",$shippingAddress->getCountryId(),$allowCountries[0]));
            $this->messageManager->addNotice(__("Klarna checkout is not available for %1, country was changed to %2.",$shippingAddress->getCountryId(),$allowCountries[0]));
            $this->changeCountry($allowCountries[0],$save = false);
        }

        if(!$billingAddress->getCountryId() || $billingAddress->getCountryId() != $shippingAddress->getCountryId()) {
            //$this->_logger->info(__("Billing country [%1] != shipping [%2]",$billingAddress->getCountryId(),$shippingAddress->getCountryId()));
            $this->changeCountry($shippingAddress->getCountryId(),$save = false);
        }

        //set test postcode, some shipping methods need
        if(!$shippingAddress->getPostcode()) {
            $locale     = $this->getKlarnaLocale()->getCountry($shippingAddress->getCountryId());
            if(!empty($locale['test']['postal_code'])) {
                $shippingAddress->setPostcode($locale['test']['postal_code'])->setCollectShippingRates(true);
            }
        }


        $currencyChanged = $this->checkAndChangeCurrency();
        
     
        $payment = $quote->getPayment();



        //force payment method  to our payment method
        $paymentMethod     = $payment->getMethod();
        
        $shipPaymentMethod = $shippingAddress->getPaymentMethod();

        if(!$paymentMethod || !$shipPaymentMethod || $paymentMethod != $this->_paymentMethod || $shipPaymentMethod != $paymentMethod) {
            $payment->unsMethodInstance()->setMethod($this->_paymentMethod);
            $quote->setTotalsCollectedFlag(false);
            //if quote is virtual, shipping is set as billing (see above)
            //setCollectShippingRates because in onepagecheckout is affirmed that shipping rates could depends by payment method
            $shippingAddress->setPaymentMethod($payment->getMethod())->setCollectShippingRates(true);
        }


        //TODO: ADD MINIMUM AOUNT TEST here


       $method = $this->checkAndChangeShippingMethod();
       
       
        $quote->setTotalsCollectedFlag(false)->collectTotals()->save(); //REQUIRED (maybe shipping amount was changed)
/*

       $billingAddress->save();
       $shippingAddress->save();

       $this->totalsCollector->collectAddressTotals($quote, $shippingAddress);
       $this->totalsCollector->collectQuoteTotals($quote);

       //$quote->collectTotals();
       $this->quoteRepository->save($quote);*/

        if($currencyChanged && $reloadIfCurrencyChanged) {
            //not needed
            $this->throwReloadException(__('Checkout was reloaded.'));
        }

        if($method === false) {
            throw new LocalizedException(__('No shipping method'));
        }

        return $this;

   }

    public function checkCart() {

        $quote       = $this->getQuote();
        $redirectUrl = 'checkout/cart';

        //@see Mage_Checkout_Model_Type_Onepage::initCheckout
        if($quote->getIsMultiShipping()) {
	  $quote->setIsMultiShipping(false)->removeAllAddresses();
        }
        if(!$this->isEnabled()) {
            $this->throwRedirectToCartException(__('The Klarna Checkout is not enabled, please use an alternative checkout method.'));
        }
        if(!$this->allowCountries()) {
            $this->throwRedirectToCartException(__('The Klarna Checkout is NOT available (no allowed country), please use an alternative checkout method.'));
        }
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->throwRedirectToCartException(__('The cart has no items (or has errors).'));
        }

        if (!$quote->validateMinimumAmount()) {
            $error =$this->helper()->getStoreConfig('sales/minimum_order/error_message');
            if(!$error) $error = __('Subtotal must exceed minimum order amount.');
            $this->throwRedirectToCartException($error);
        }
        return true;
   }


   public function checkAndChangeCurrency() {

        $quote  = $this->getQuote();
        $store  = $this->getQuote()->getStore();

        $country    = $quote->getBillingAddress()->getCountryId();

        if(!$country) {
            throw new LocalizedException(__('Country is not set.')); //this cannot happens, check address is done into initCheckout, before this
        }

        $locale     = $this->getKlarnaLocale()->getCountry($country);

        if(empty($locale['currency'])) {
            throw new LocalizedException(__('Klarna Checkout is not available for country (%1), no currency available.',$country));
        }

        $requiredCurrency = $locale['currency'];
        $currentCurrency  = $quote->getQuoteCurrencyCode();
        


        if($requiredCurrency == $currentCurrency) {
            return false; //currency not changed
        }
        
        
        //try to change the currency (do not make any checks, we will check if currency was applied)

        $store->setCurrentCurrencyCode($requiredCurrency); //this will change currency only if currency is available, else, no change @see Magento\Store\Model\Store

        $currency = $store->getCurrentCurrency(); //reload current currency, check if was set (if no rate, the default will be used)

        if($currency->getCode() != $requiredCurrency) {
            $this->throwRedirectToCartException(__('Klarna Checkout requires the currency to be %1 but is not available, please use an alternative checkout.',$requiredCurrency));
        }
        $quote->setTotalsCollectedFlag(false);
        if(!$quote->isVirtual() && $quote->getShippingAddress()) {
            $quote->getShippingAddress()->setCollectShippingRates(true);
        }
        $this->messageManager->addNotice(__('Currency was changed to %1 (cannot use %2 for  %3).',$requiredCurrency,$currentCurrency,$locale['name']));

        return true; //currency was changed
   }


   public function changeCountry($country,$saveQuote = false) {

        $allowCountries = $this->allowCountries();
        if(!$country || !in_array($country,$allowCountries)) {
           throw new LocalizedException(__('Invalid country (%1)',$country));
        }

        $blankAddress = $this->getBlankAddress($country);
        $quote        = $this->getQuote();

        $quote->getBillingAddress()->addData($blankAddress);
        if(!$quote->isVirtual()) {
            $quote->getShippingAddress()->addData($blankAddress)->setCollectShippingRates(true);
        }
        if($saveQuote) {
            $this->checkAndChangeCurrency();
            $quote->collectTotals()->save();
        }
   }



   public function getBlankAddress($country) {

        $locale     = $this->getKlarnaLocale()->getCountry($country);

        $blankAddress = array(
            'customer_address_id'=>0,
            'save_in_address_book'=>0,
            'same_as_billing'=>0,
            'street'=>'',
            'city'=>'',
            'postcode'=>'',
            'region_id'=>'',
            'country_id'=>$country
        );
        return $blankAddress;
   }


   public function  checkAndChangeShippingMethod()
    {

        $quote        = $this->getQuote();
        if($quote->isVirtual()) {
            return true;
        }

        $quote->collectTotals(); //this is need by shipping method with minimum amount

        $shipping = $quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
        $allRates = $shipping->getAllShippingRates();

        if(!count($allRates)) {
            return false;
        }

        $rates    = array();
        foreach($allRates as $rate) {
            $rates[$rate->getCode()] = $rate->getCode();
        }


        //test current
        $method = $shipping->getShippingMethod();
        if($method && isset($rates[$method])) {
            return $method;
        }

        //test default
        $method = $this->helper()->getShippingMethod();
        if($method && isset($rates[$method])) {
            $shipping->setShippingMethod($method);//->setCollectShippingRates(true);
            return $method;
        }

        $method = $allRates[0]->getCode();
        $shipping->setShippingMethod($method);//->setCollectShippingRates(true);
        return $method;

    }




   //get (or create in not exists) KlarnaOrder and update from quote

   public function updateKlarnaOrder()
   {

        $quote       = $this->getQuote();

        $klarnaOrder = $this->getKlarnaOrder()->assignQuote($quote);

        //if we already have an order
        $checkoutURI = $this->getCheckoutSession()->getKlarnaOrderUri(); //check session for Klarna Order Uri

        if($checkoutURI) {
            try {
                $klarnaOrder->fetchForUpdate($checkoutURI);
            } catch(\Exception $e) {
                $klarnaOrder->reset(); //create new one
                $this->getCheckoutSession()->unsKlarnaOrderUri(); //remove URI from session
                $this->helper()->logError("Cannot fetch for update Klarna Order {$checkoutURI}, {$e->getMessage()} (see exception.log)");
                $this->helper()->logError($e);
            }
        }

        $klarnaOrder->updateFromQuote($quote);

        //save klarna uri in checkout/session
        $this->getCheckoutSession()->setKlarnaOrderUri($klarnaOrder->getLocation());

        return $this;

    }



    protected function _validateKlarnaOrder($klarnaOrder,$quote) {

        //check required fields
        $klarnaData = $klarnaOrder->marshal();

        if(empty($klarnaData['reservation'])) {
            throw new LocalizedException(__('Checkout incomplete, missing reservation.'));
        }
        if(empty($klarnaData['cart']['items'])) {
            throw new LocalizedException(__('Checkout incomplete, no items.'));
        }
        if(empty($klarnaData['shipping_address'])) {
            throw new LocalizedException(__('Checkout incomplete, missing shipping_address.'));
        }
        if(empty($klarnaData['billing_address'])) {
            throw new LocalizedException(__('Checkout incomplete, missing billing_address.'));
        }
        if(empty($klarnaData['purchase_currency'])) {
            throw new LocalizedException(__('Checkout incomplete, missing purchase_currency.'));
        }
        if(empty($klarnaData['merchant_reference']['signature'])) {
            throw new LocalizedException(__('Checkout incomplete, missing quote signature.'));
        }


        $qCurrency = strtolower(trim($quote->getQuoteCurrencyCode()));
        $kCurrency = strtolower(trim($klarnaData['purchase_currency']));

        if($qCurrency != $kCurrency) {
            throw new LocalizedException(__('Quote was changed, Current currency %1, klarna currency %2',$qCurrency,$kCurrency));
        }

        return true;
    }


    public function _getQuoteFromKlarnaOrder($klarnaOrder) {

        $klarnaData = $klarnaOrder->marshal();
        if(empty($klarnaData['merchant_reference']['orderid1'])) {
            throw new LocalizedException(__('Checkout incomplete, missing quote id.'));
        }

        $qqID = $klarnaData['merchant_reference']['orderid1'];  //ordeid is q12345..
        if($qqID{0} != 'q') {
            throw new LocalizedException(__('Invalid request, wrong order id %1 ',$qqID));
        }
        $qID = (int)substr($qqID,1); //orderid is q12345..
        if($qID<=0) {
            throw new LocalizedException(__('Invalid request, wrong order id %1',$qqID));
        }
        $quote = $this->quoteRepository->get($qID);
        
        if(!$quote->getId()) {
            throw new LocalizedException(__('Invalid request, quote %1 doesn\'t exists',$qID));
        }
        $klarnaOrder->assignQuote($quote);
        $this->setQuote($quote);

        return $quote;

    }



    //saveOrder; session independent

    public function placeOrder($klarnaOrder,$kID,$test) {

        $this->setDoNotMarkCartDirty(true); //prevent observer to mark quote dirty, we will check here if quote was changed and, if yes, will redirect to checkout

        $quote = $klarnaOrder->getQuote();

        if(!$quote) {
            $quote = $this->_getQuoteFromKlarnaOrder($klarnaOrder);
        }

        $this->_validateKlarnaOrder($klarnaOrder,$quote);

	 
        //this will be saved in order (and is uniq); is ID of push request
        //required to avoid duplicate orders; when push/confirmation are executed concurent
        $quote->setNwtKid($kID);
        
        $klarnaData = $klarnaOrder->marshal();

        $reservation = $klarnaData['reservation'];

        $cart       = $klarnaData['cart']['items'];
        $shipping   = $klarnaData['shipping_address'];
        $billing    = $klarnaData['billing_address'];

        $diff       = array_diff_assoc($billing,$shipping);
        

        $billingAddress = $quote->getBillingAddress()
            ->addData($klarnaOrder->mageAddress($billing))
            ->setCustomerAddressId(0)
            ->setSaveInAddressBook(0)
            ->setShouldIgnoreValidation(true);

        $shippingAddress = $quote->getShippingAddress()
            ->addData($klarnaOrder->mageAddress($shipping))
            ->setSameAsBilling(1)
            ->setCustomerAddressId(0)
            ->setSaveInAddressBook(0)
            ->setShouldIgnoreValidation(true);

        $quote->setCustomerEmail($billingAddress->getEmail());

        if(empty($diff)) { //?!?
            $shippingAddress->setSameAsBilling(0);
        }

        $customer      = $quote->getCustomer(); //this (customer_id) is set into self::init
        $isNewCustomer = false;
                

        if ($customer && $customer->getId()) {
            $quote->setCheckoutMethod(self::METHOD_CUSTOMER)
                ->setCustomerId($customer->getId())
                ->setCustomerEmail($customer->getEmail())
                ->setCustomerFirstname($customer->getFirstname())
                ->setCustomerLastname($customer->getLastname())
                ->setCustomerIsGuest(false);
        } else {
             //checkout method
            $quote->setCheckoutMethod(self::METHOD_GUEST)
                ->setCustomerId(null)
                ->setCustomerEmail($billingAddress->getEmail())
                ->setCustomerFirstname($billingAddress->getFirstname())
                ->setCustomerLastname($billingAddress->getLastname())
                ->setCustomerIsGuest(true)
                ->setCustomerGroupId(GroupInterface::NOT_LOGGED_IN_ID);

            //register customer, if required
            //the customer is registered after order is placed (no METHOD_REGISTER), see bellow
            if(
                $billingAddress->getEmail() &&
                        $this->helper()->registerCustomer() &&
                        !$this->_customerEmailExists($billingAddress->getEmail(),$quote->getStore()->getWebsiteId())
            ) {
                        $isNewCustomer = true;
            }
        }


        //set payment
        $payment = $quote->getPayment();

        //force payment method
        if(!$payment->getMethod() || $payment->getMethod() != $this->_paymentMethod) {
            $payment->unsMethodInstance()->setMethod($this->_paymentMethod);
        }
        $paymentData = (new DataObject($klarnaData))->setIsTestMode($test)->setPushId($kID);

        
        $quote->getPayment()->getMethodInstance()->assignData($paymentData);

        $quote->setNwtReservation($reservation); //this is used by pushAction

        //- do not recollect totals
        $quote->setTotalsCollectedFlag(true);
        // - or... ????
        //$quote->setTotalsCollectedFlag(true)->collectTotals();

        $qSign = $this->getQuoteSignature($quote);
        $kSign = $klarnaData['merchant_reference']['signature'];

        if($qSign != $kSign) {
           throw new LocalizedException(__("Quote was updated, please review it (check shipping amount, products)"));
        }
        
        
        $order = $this->quoteManagement->submit($quote);


	
        $this->_eventManager->dispatch(
            'checkout_type_onepage_save_order_after',
            ['order' => $order, 'quote' => $this->getQuote()]
        );

        if ($order->getCanSendNewEmailFlag()) {
            try {
                $this->orderSender->send($order);
            } catch (\Exception $e) {
                $this->_logger->critical($e);
            }
        }
        

        

        // add order information to the session
        $this->_checkoutSession
            ->setLastOrderId($order->getId())
            ->setLastRealOrderId($order->getIncrementId())
            ->setLastOrderStatus($order->getStatus());
            


        $this->_eventManager->dispatch(
            'checkout_submit_all_after',
            [
                'order' => $order,
                'quote' => $this->getQuote()
            ]
        );

        if ($isNewCustomer) {
            //@see Magento\Checkout\Controller\Account\Create
            $orderCustomerService = $this->getObjectManager()->get('\Magento\Sales\Api\OrderCustomerManagementInterface');

            try {
                $orderCustomerService->create($order->getId());
            } catch (\Exception $e) {
                $this->_logger->error(__("Order %1, cannot create customer [%2]: %3",$order->getIncrementId(),$order->getCustomerEmail(),$e->getMessage()));
                $this->_logger->critical($e);
            }
        }
        
        
        if($order->getCustomerEmail() && $this->helper()->subscribeNewsletter($this->getQuote())) {
            try {
                //subscribe to newsletter
                $this->_subscribeNews($order);
            } catch(\Exception $e) {
               $this->_logger->error("Cannot subscribe customer ({$order->getCustomerEmail()}) to the Newsletter: ".$e->getMessage());
            }
        }
        
        return $order;
    }


    protected function _subscribeNews($order)
    {

        $email = $order->getCustomerEmail();

        if(!$email) {
            return false;
        }

        $subscriber = $this->getObjectManager()->create('Magento\Newsletter\Model\Subscriber');
        $subscriber->loadByEmail($email);

        if($subscriber->getId()) {
            return false;
        }

        return $subscriber->subscribe($email);
    }




    public function getQuoteSignature($quote = null) {

        if(!$quote) {
            $quote = $this->getQuote();
        }

        $billingAddress = $quote->getBillingAddress();
        if(!$quote->isVirtual()) {
            $shippingAddress = $quote->getShippingAddress();
        }

        $info = array(
            //'store'   =>$quote->getStore()->getId(), -- with a wrong cookie (store), magento will reset store when we choose default store
            'currency'=>$quote->getQuoteCurrencyCode(),
            //'customer'=>$quote->getCustomer()->getId(), -- this is updated when quote is placed, could be changed
            'shipping_method'=>$quote->isVirtual()?null:$shippingAddress->getShippingMethod(),
            'shipping_country' =>$quote->isVirtual()?null:$shippingAddress->getCountryId(),
            'billing_country' =>$billingAddress->getCountryId(),
            'payment' =>$quote->getPayment()->getMethod(),
            'subtotal'=>sprintf("%.2f",round($quote->getBaseSubtotal(),2)), //store base (currency will be set in checkout)
            'total'=>sprintf("%.2f",round($quote->getBaseGrandTotal(),2)),  //base grand total
            'items'=>array()
        );

        foreach($quote->getAllVisibleItems() as $item) {
            $info['items'][$item->getId()] = sprintf("%.2f",round($item->getQty()*$item->getBasePriceInclTax(),2));
        }
        ksort($info['items']);
        return md5(serialize($info));

    }

    public function cancelKlarnaReservation() {
        $this->getCheckoutSession()->unsKlarnaOrderUri(); //remove klarna URI from session
        $this->getKlarnaOrder()->cancelReservation();
    }

    
    
    //Checkout ajax updates
    
    
    /**
     * Set shipping method to quote, if needed
     *
     * @param string $methodCode
     * @return void
     */
    public function updateShippingMethod($methodCode)
    {
	$quote = $this->getQuote();
	if($quote->isVirtual()) {
            return $this;
        }
        $shippingAddress = $quote->getShippingAddress();
        if ($methodCode != $shippingAddress->getShippingMethod()) {
            $this->ignoreAddressValidation();
            $shippingAddress->setShippingMethod($methodCode)->setCollectShippingRates(true);
            $quote->setTotalsCollectedFlag(false)->collectTotals()->save();
        }
    }
    
     
    /**
     * Update shipping address
     *
     * @param string $methodCode
     * @return void
     */
    public function updateShippingAddress($data)
    {
        $quote = $this->getQuote();
        if($quote->isVirtual()) {
            return $this;
        }
        $addr = $this->getKlarnaOrder()->mageAddress($data,$withEmpty = false);
        if(!$addr) return $this;
        
        $shippingAddress = $quote->getShippingAddress();

        
        $cnt = 0;
        foreach($addr as $field=>$value) {
            $kValue = trim(strtolower($value));
            $mValue = trim(strtolower((string)$shippingAddress->getData($field)));
            if($kValue != $mValue) {
                $shippingAddress->setData($field,$value);
                $cnt++;
            }
        }
        if($cnt) {
           $shippingAddress->setShouldIgnoreValidation(true)->setCollectShippingRates(true);
           $quote->setTotalsCollectedFlag(false)->collectTotals()->save();
        }
 
    }
    
    
        /**
     * Make sure addresses will be saved without validation errors
     *
     * @return void
     */
    private function ignoreAddressValidation()
    {
	$quote = $this->getQuote();
        $quote->getBillingAddress()->setShouldIgnoreValidation(true);
        if (!$quote->getIsVirtual()) {
           $quote->getShippingAddress()->setShouldIgnoreValidation(true);
        }
    }


}

