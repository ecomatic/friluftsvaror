<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 * 
 *
 */

namespace NWT\KCO\Model\Payment\Method;

use Magento\Framework\DataObject;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Framework\Exception\LocalizedException;
use NWT\KCO\Lib\KlarnaNS\Api\KlarnaFlags;

/**
 * Klarna Checkout Payment method 
 */
class Checkout extends AbstractMethod
{

    protected $_code  = 'nwtkco';

   /**
    * @var string
    */
    protected $_formBlockType = 'NWT\KCO\Block\Payment\Checkout\Form';

   /**
    * @var string
    */
   protected $_infoBlockType = 'NWT\KCO\Block\Payment\Checkout\Info';



    protected $_isGateway = false;
    protected $_isOffline = false;
    protected $_canOrder = false;
    protected $_canAuthorize = true; //authorize is it called by initialize
    protected $_canCapture = true;   //capture payment when invoice is placed
    protected $_canCapturePartial = true; 
    protected $_canCaptureOnce = false; // capture can be performed once and no further capture possible (didn't see when/how it's used)
    protected $_canRefund = true; //refund on credit memo
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid = true;   //the payment will be canceled when order is canceled
    protected $_canUseInternal = false; //cannot be used internal (backend)
    protected $_canUseCheckout = true; //used in checkout (will redirect the user to the /klarna/checkout)
    protected $_isInitializeNeeded = true;  //will use initialize to authorize and set state/status to new/pending (with authorize the state is set to processing)
    protected $_canFetchTransactionInfo = false;
    protected $_canReviewPayment = false;
    protected $_canCancelInvoice = false; //is not yet implemented?!? (Magento 2.0.4)









    //"Keep" quote, we will need into canUseCurrency

    protected $_quote;

    /**
     * Check whether payment method can be used
     * @param \Magento\Quote\Api\Data\CartInterface|Quote|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        $this->_quote = $quote;
        return $this->_helper->isEnabled() && parent::isAvailable($quote);
    }

    
    
    /**
     * Assign data to info model instance
     *
     * @param array|\Magento\Framework\DataObject $data
     * @return \Magento\Payment\Model\Info
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {


        $expires_dt = null;

        if($data->getExpiresAt()) {
            $expires = @strtotime($data->getExpiresAt());
            if($expires) {
                $expires_dt = date("Y-m-d H:i:s",$expires);
            }
        }
        if(!$data->hasIsTestMode()) {
	  $testMode = $this->_helper->isTestMode();
        } else {
	  $testMode = $data->getIsTestMode();
        }

        $this->getInfoInstance()
            ->setAdditionalInformation('reservation',$data->getReservation())
            ->setAdditionalInformation('kid',$data->getId())
            ->setAdditionalInformation('test',$testMode?1:0)
            ->setAdditionalInformation('expires_at',$expires_dt)
            ->setAdditionalInformation('push_id',$data->getPushId());

        return $this;
    }

    /**
     * Check capture availability
     *
     * @return bool
     * @api
     */
    public function canCapture()
    {
        if(!$this->_canCapture) {
            return false;
        }
        $payment = $this->getInfoInstance();
        $order   = $payment->getOrder();
        return $this->_helper->canCapture($order?$order->getStore():null);
        return false; 
    }

    /**
     * Check partial capture availability
     *
     * @return bool
     * @api
     */
    public function canCapturePartial()
    {
        if(!$this->_canCapturePartial) {
            return false;
        }
        $payment = $this->getInfoInstance();
        $order   = $payment->getOrder();
        return $this->_helper->_canCapturePartial($order?$order->getStore():null);

    }

    /**
     * To check billing country is allowed for the payment method
     *
     * @param string $country
     * @return bool
     */
    public function canUseForCountry($country)
    {
        $country = trim(strtoupper($country));
        $result =  $country && in_array($country,$this->_helper->getCountries()) && parent::canUseForCountry($country);
        return $result;
    }


    /**
     * Get currency model instance. 
     *
     * @return Magento\Directory\Model\Currency
     */
    public function getCurrency($currency)
    {
        if ($this->_currency === null) {
            $this->_currency = $this->_currencyFactory->create();
        }
        return $this->_currency;
    }




   /**
     * Check method for processing with base currency
     *
     * @param string $currencyCode
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function canUseForCurrency($currencyCode)    
    {
    
        if(!$this->_quote) return true; //this->_quote is set into isAvailable

        $country         = $this->_quote->getBillingAddress()->getCountryId();
        $currentCurrency = $this->_quote->getQuoteCurrencyCode();
        $store           = $this->_quote->getStore();

        if(!$country) {
            return false;
        }

        $locale = $this->_klarnaLocale->getCountry($country);
        if(empty($locale['currency'])) {
            return false;
        }

        $available = $store->getAvailableCurrencyCodes();

        if($locale['currency'] == $currencyCode  || $locale['currency'] == $currentCurrency) {
            //base or current quote currency, no convert need
            return true;
        }


        if(!in_array($locale['currency'],$available)) {
            return false;
        }

        //check if required currency can be used (and have rate for it)
        //@see Mage_Core_Model_Store::getCurrentCurrencyCode), if a rate doesn't exists, the currency will be changed to the base
        $currency = $this->_currencyFactory->create()->load($locale['currency']);
        if(!$currency || !$currency->getId()) {
            return false;
        }

        //KlarnaCheckout  will try to set required currency, if we don't have a rate, this will not be possible, then currenct currency cannot be used
        //so, check if we have a rate for currencty
        $rate = $store->getBaseCurrency()->getRate($currency);
        return !empty($rate);
    }





    /**
     * Checkout redirect URL getter for onepage checkout (hardcode)
     *
     * @see \Magento\Checkout\Controller\Onepage::savePaymentAction()
     * @see Quote\Payment::getCheckoutRedirectUrl()
     * @return string
     */
    public function getCheckoutRedirectUrl()
    {
        return $this->_helper->getCheckoutUrl();
    }


    /**
     * Get config payment action url
     * Used to universalize payment actions when processing payment place
     *
     * @return string
     * @api
     */
    public function getConfigPaymentAction()
    {
        return self::ACTION_AUTHORIZE;
    }

    /**
     * Method that will be executed instead of authorize or capture
     * if flag isInitializeNeeded set to true
     *
     * @param string $paymentAction
     * @param object $stateObject
     *
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @api
     */
    public function initialize($paymentAction, $stateObject)
    {
        //$paymentAction not used, we will "authorize" by default

        $payment = $this->getInfoInstance();

        $order   = $payment->getOrder();

        //import quote data

        $order
            ->setNwtReservation($payment->getAdditionalInformation('reservation'))
            ->setNwtKid($payment->getAdditionalInformation('push_id'))
        ;


        $orderState = \Magento\Sales\Model\Order::STATE_NEW;

        $stateObject->setState($orderState);

        $orderStatus = $this->getConfigData('order_status');

        if (!$orderStatus) {
            $orderStatus = $order->getConfig()->getStateDefaultStatus($orderState);
        } else {

            //check which state we have (NEW or PROCESSING)

            $statuses = $order->getConfig()->getStateStatuses($orderState);
            if(!isset($statuses[$orderState])) {
                //check if we have  "processing" status
                $orderState = \Magento\Sales\Model\Order::STATE_PROCESSING;
                $statuses = $order->getConfig()->getStateStatuses($orderState);
           //     if(isset($statuses[$orderStatus])) {
                    //set state = processing
                    $stateObject->setState($orderState);
          //      }
            }
        }

        $stateObject->setStatus($orderStatus);
        $stateObject->setIsNotified(false);
        
        //We need to keep this, to restore after magento "destroy" it
        //@see Observer\FixOrderStatus
        $payment
            ->setNwtkcoState($stateObject->getState())
            ->setNwtkcoStatus($stateObject->getStatus());
            

        //$payment->authorize(true,$order->getBaseTotalDue());  //due to some bugs, we don't want to use magento authorize, we will "replicate" almost all operations
        
        $this->authorize($payment,$order->getBaseTotalDue());
        
        $payment->setAmountAuthorized($order->getTotalDue());
        

        return $this;

    }



    /**
     * Authorize payment method
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
     
    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {

        //NOTE: amount is "baseAmount"

        if (!$this->canAuthorize()) {
            throw new LocalizedException(__('Authorize action is not available.'));
        }
        
	$order = $payment->getOrder();
	$this->setStore($order->getStoreId());
        
	$payment->setShouldCloseParentTransaction(false);
        // update totals
        $amount = $payment->formatAmount($amount, true);
        $payment->setBaseAmountAuthorized($amount);

        $formattedAmount = $order->getBaseCurrency()->formatTxt($amount);
        
        
        $info = $this->getInfoInstance();
        $payment->setTransactionId($info->getAdditionalInformation('reservation'));
    //    $payment->setIsFraudDetected(false); //bug into magento <=2.1.4 (don't know when/if was fixed) which mark all orders as FraudDetected on multicurrency stores
        
        //restore OUR state/status (set into initialize), not state set by authorize 
        //@see Magento\Sales\Model\Order\Payment\Operations\AuthorizeOperation
        //@see Magento\Sales\Model\Order\Payment\State\OrderCommand
        
        $order
	    ->setState($payment->getNwtkcoState())
            ->setStatus($payment->getNwtkcoStatus())
	;        
        
        
        
        $canCapture = $this->canCapture();
        if($canCapture) {
            $payment->setIsTransactionClosed(0); //let transaction OPEN (need to cancel/void this reservation)
            $message = __('Authorized amount of %1.',$formattedAmount);
            
        } else {
            $message = __('Authorized amount of %1. Klarna Reservation was CREATED and transaction CLOSED. All further Klarna operations  (activate, refund) will be done, by hand, into Klarna control panel.', $formattedAmount);
        }
        
        // update transactions, order state and add comments
        $transaction = $payment->addTransaction(Transaction::TYPE_AUTH);
        $message = $payment->prependMessage($message);
        $payment->addTransactionCommentsToOrder($transaction, $message);
        
        return $this;
    }


    /**
     * Capture payment method
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */

    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount) {

        //NOTE: amount is "baseAmount"

        $authTransaction = $payment->getAuthorizationTransaction();
        if($authTransaction) {
            $rno = $authTransaction->getTxnId();
            $payment->setParentTransactionId($rno);
        } else {
            $rno  = $this->getInfoInstance()->getAdditionalInformation('reservation');
        }

        if(!$rno) {
            throw new LocalizedException(__('Cannot capture online, there is no Klarna Reservation number set'));
        }

        $order = $payment->getOrder();

        //this is set via sales_order_payment_capture event, @see \NWT\KCO\Observer\PaymentCapture
        $invoice = $payment->getCapturedInvoice();
        if(!$invoice) {
            throw new LocalizedException(__('Cannot capture online, no invoice set'));
        }

        //Klarna API allow to activate partial invoice by adding only SKU & qty
        //but, we have some "skus" (like discount or other totals) which is shared/split between invoices
        //(and, because qty == 1, we cannot capture partial that article)

        //so, we will:
        //  1. Update reservation with invoice items + one more item (TOTAL DUE)
        //  2. Activate Klarna invoice with magento invoice items (TOTAL DUE will remain unactivated)

        $items = $this->_klarnaFactory->createItems()->fromInvoice($invoice);
        $result = false;

        try {
            $klarnaAPI = $this->_klarnaFactory->createApi()->initFromOrder($payment->getOrder());
            
            //create list of articles which need to be activated
            foreach($items as $item) {
                $flags = KlarnaFlags::INC_VAT;
                if(!empty($item['type']) && $item['type'] == 'shipping_fee') {
                    $flags = $flags | KlarnaFlags::IS_SHIPMENT;
                }

                $klarnaAPI->addArticle(
                    $item['quantity'],              // Quantity
                    $item['reference'],     // Article number
                    $item['name'], // Article name/title
                    round($item['unit_price']/100,2),     // Price (items contains price in cents, this is why /100
                    round($item['tax_rate']/100,2),       // 25% VAT
                    round($item['discount_rate']/100,2),  // Discount
                    $flags          // Flags
                );
            }

            //add total due (from order)
            $orderDue = $order->getTotalDue();

            if(!$invoice->getId()) {
                //for current invoice (not saved), totals were not saved (calculated on order) yet (@bug?)
                $orderDue -= $invoice->getGrandTotal();
            }

            if($orderDue != 0) {
                $flags = KlarnaFlags::INC_VAT;
                $klarnaAPI->addArticle(
                    1,              // Quantity
                    'totaldue',     // Article number
                    'Order Total Due', // Article name/title
                    round($orderDue,2),     // Price (items contains price in cents, this is why /100
                    0,       // no VAT
                    0,  // Discount
                    $flags          // Flags
                );
            }
            $result = $klarnaAPI->update($rno);             //update reservation
        } catch(\Exception $e) {
           $this->_logger->critical($e);
            //need to throw MageException, to be shown as error
           throw new LocalizedException(__('Cannot update klarna reservation: %1.',$e->getMessage()?$e->getMessage():get_class($e)));
        }
        
        if(!$result) {
            $this->_logger->critical(__("Cannot update klarna reservation (%1), with total due %2 and items:\n %3",$rno,$totalDue,print_r($items,true)));
            throw new LocalizedException(__('Cannot update klarna reservation (%1), with invoice items.',$rno));
        }


        //activate partial invoice (don't know if it's required to reinit, but, to be sure...)
        $klarnaAPI = $this->_klarnaFactory->createApi()->initFromOrder($payment->getOrder());
        foreach($items as $item) {
            $klarnaAPI->addArtNo($item['quantity'],$item['reference']);
        }

        try {
            $result = $klarnaAPI->activate($rno);
        } catch(\Exception $e) {
           $this->_logger->critical($e);
            //need to throw MageException, to be shown as error
            throw new LocalizedException(__('The Klarna order cannot be activated: %1.',$e->getMessage()?$e->getMessage():get_class($e)));
        }

        $payment->setTransactionId($result[1]);
        $payment->setIsTransactionClosed(1);
        $payment->setMessage((string)__('Klarna Reservation %1 was ACTIVATED; Risk: %1',$rno,$result[0]));
        return $this;

    }



    /**
     * Refund specified amount for payment
     *
     * @param Varien_Object $payment
     * @param float $amount
     *
     * @return Mage_Payment_Model_Abstract
     */
    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        
        if (!$this->canRefund()) {
             throw new LocalizedException(__('Refund action is not available.'));
        }
        $ino = $payment->getRefundTransactionId();
        if(!$ino) {
            throw new LocalizedException(__('Unknown transaction.'));
        }

        $creditmemo = $payment->getCreditmemo();
        $invoice = $creditmemo->getInvoice();

        $totalRefunded = $invoice->getBaseTotalRefunded();

        //$fullRefund = ($totalRefunded == $invoice->getBaseGrandTotal() && $amount == $totalRefunded);
        $fullRefund = ($totalRefunded == $invoice->getBaseGrandTotal());
        //^^^we will make a full refund even we have another partial refund before
        //klarna will set all items to 0, inlcuding previous refunds
        //^this is what klarna understand by refund, they don't create another document but
        // - update qty to 0 (for 'by article refund')
        // - add another line into invoice with -$amount
        //don't know if it's a good ideea , we will see


        $message = __('Klarna Invoice %1 was refunded (total: %2).',$ino,$totalRefunded);

        try {

            $klarnaAPI = $this->_klarnaFactory->createApi()->initFromOrder($payment->getOrder());
            if($fullRefund) {
                $result = $klarnaAPI->creditInvoice($ino); //Klarna will responde with credited invoice number (not with the new invoice number), this sux
            } else {

                $amount = $creditmemo->getGrandTotal(); //$amount received represents base grand total, we need grand
                $message = __("%1 was refunded from Klarna Invoice %2",$amount,$ino);
                $result = $klarnaAPI->returnAmount(
                    $ino,             // Invoice number
                    $amount,        // Amount given as a discount. $amount is base grand total
                    0,         // 25% VAT
                    KlarnaFlags::INC_VAT,   // Amount including VAT.
                    "Discount #".$creditmemo->getIncrementId() // Description
                );
                //credit Amount
            }
        } catch(\Exception $e) {
            $this->_logger->critical($e);
            //need to throw MageException, to be shown as error
            throw new LocalizedException(__('The Klarna order cannot be refunded: %1.',$e->getMessage()?$e->getMessage():get_class($e)));
        }

        //we dont' have klarna refund ID (because Klarna change invoice refunded by adding -$amount or set qty = 1)
        // we will generate on (result is invoice number)
        $tno = $result.'-'.$creditmemo->getIncrementId();
        $payment->setTransactionId($tno)
                ->setIsTransactionClosed(1)
                ->setMessage((string)$message);
        return $this;
    }


   /**
     * Cancel payment 
     *
     * @param Varien_Object $payment
     *
     * @return $this
     */
    public function cancel(\Magento\Payment\Model\InfoInterface $payment)
    {
        return $this->void($payment);
    }


    /**
     * Void payment 
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @return $this
     */
    public function void(\Magento\Payment\Model\InfoInterface $payment)
    {
        if (!$this->canVoid()) {
            throw new LocalizedException(__('Void action is not available.'));
        }

        $authTransaction = $payment->getAuthorizationTransaction();
        if($authTransaction) {
            $rno = $authTransaction->getTxnId();
            $payment->setParentTransactionId($rno);
        } else {
            $rno  = $this->getInfoInstance()->getAdditionalInformation('reservation');
        }
        if(!$rno) {
            //do nothing
            return $this;
        }

        $result = null;
        try {
            $klarnaAPI = $this->_klarnaFactory->createApi()->initFromOrder($payment->getOrder());
            $result = $klarnaAPI->cancelReservation($rno);
        } catch(\Exception $e) {
            $this->_helper->logCritical($e);
            //need to throw MageException, to be shown as error
            throw new LocalizedException(__('The Klarna order cannot be canceled, %1.',$e->getMessage()?$e->getMessage():get_class($e)));
        }

        if(!$result) {
            throw new LocalizedException(__("Cannot cancel Klarna Reservation %1.",$rno));
        }

        $payment->setTransactionId($rno.'-cancel');
        $payment->setMessage((string)__("Klarna reservation %1 was CANCELED.",$rno));

        return $this;

    }

    
    /**
     * Detach payment - this will just "detach" the payment from order, reservation will not be canceled
     * Not USED yet (need to add action in order controller)
     *
     * @param \Magento\Payment\Model\InfoInterface  $payment
     *
     * @return $this
     */
    public function detach(\Magento\Payment\Model\InfoInterface $payment)
    {
        $authTransaction = $payment->getAuthorizationTransaction();
        if(!($authTransaction && ($rno = $authTransaction->getTxnId()))) {
            //not transaction to detach
            return $this;
        }

        //check if we have invoice in pending states
        $order = $payment->getOrder();
        $invoices = $order->getInvoiceCollection();
        $openInvoices = 0;
        foreach($invoices as $invoice) {
            if($invoice->canCancel()) { //$invoice->getState() == self::STATE_OPEN;
                $openInvoices++;
            }
        }
        if($openInvoices>0) {
            throw new LocalizedException(__("Klarna reservation %1 cannot be detached. You have %2 pending invoice(s).",$rno,$openInvoices));
        }

        //detach the authorization (close the transaction)
        $payment
            ->setParentTransactionId($rno)
            ->setTransactionId($rno.'-void')
            ->setMessage((string)__("Klarna reservation %1 was detached from the order.<br>All (Klarna) payment operations from now on (activating the order, invoicing etc.) have to be performed offline (on the Klarna Site).<br>",$rno));

        return $this;

    }

}
