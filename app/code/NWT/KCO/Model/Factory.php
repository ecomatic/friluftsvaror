<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 *
 *
 */


namespace NWT\KCO\Model;


/**
 * Factory class for \NWT\KCO\ModelPush|Checkout}
 */
class Factory
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Factory constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param string $className
     * @param array $data
     * @return \NWT\KCO\Model\Push
     */
    public function createPush(array $data = [])
    {
 
        return $this->_objectManager->create('\NWT\KCO\Model\Push', $data);
    }
  

     /**
     * Get class instance with specified parameters
     *
     * @param string $className
     * @param array $data
     * @return \NWT\KCO\Model\Checkout
     */
    public function getCheckout(array $data = [])
    {
 
        return $this->_objectManager->get('\NWT\KCO\Model\Checkout', $data);
    }
}


