<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 *
 *
 */


namespace NWT\KCO\Model\System\Config\Source\Shipping;

class Allowedmethods extends \Magento\Shipping\Model\Config\Source\Allmethods implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Return array of active carriers.
     *
     * @param bool $isMultiselect
     * @return array
     */

    public function toOptionArray($isMultiselect=false)
    {

        
        $options = parent::toOptionArray(true);
        if($isMultiselect) {
            //remove first option (empty one)
            array_pop($options);
        }
        return $options;
    }
}
