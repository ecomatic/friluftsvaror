<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 * 
 *
 */


namespace NWT\KCO\Model\System\Config\Source;
/**
 * Model Source for all allowed Klarna Countries
 */
class Country implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * Klarna Locale
     *
     * @var \NWT\KCO\Model\Klarna\Locale
     */
    protected $_locale;

    /**
     * @param \Magento\Directory\Model\ResourceModel\Country\Collection $countryCollection
     */
    public function __construct(\NWT\KCO\Model\Klarna\Locale $locale)
    {
        $this->_locale = $locale;
    }


    public function toOptionArray($isMultiselect=false)
    {

        $locales = $this->_locale->getLocales();
        
        $return = array();

        if(!$isMultiselect) {
            $return[] = array('value'=>'', 'label'=> '');
        }

        foreach($locales as $key=>$locale) {
            $return[] = array(
                'value'=>$key,
                'label'=>$locale['name']
            );
        }

        return $return;
    }
}
