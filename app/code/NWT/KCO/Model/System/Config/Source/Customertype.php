<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 * 
 *
 */


namespace NWT\KCO\Model\System\Config\Source;
/**
 * Model Source for all allowed Klarna Countries
 */
class Customertype implements \Magento\Framework\Option\ArrayInterface
{


    public function toOptionArray($isMultiselect=false)
    {
    
        $options = array();

        if(!$isMultiselect) {
            $options[] = array('value'=>'', 'label'=> '');
        }

        $options[] = array(
               'value' => 'person',
               'label' => __('B2C')
            );
        $options[] = array(
               'value' => 'organization',
               'label' => __('B2B')
        );

        return $options;
    }
}
