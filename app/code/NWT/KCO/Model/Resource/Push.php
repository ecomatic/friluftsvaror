<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
 */


namespace NWT\KCO\Model\Resource;


/**
 * Klarna Push (history) resource model
 */
class Push extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
     
    public function _construct() {
        $this->_init('nwtkco_push', 'entity_id');
    }

}
