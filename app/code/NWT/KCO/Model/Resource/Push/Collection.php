<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
 */


namespace NWT\KCO\Model\Resource\Push;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

  /**
   *  Define model & resource model
   */

    public function _construct()
    {
         $this->_init(
	    'NWT\KCO\Model\Push', //model
	    'NWT\KCO\Model\Resource\Push' //resource
	 );
    }
}
