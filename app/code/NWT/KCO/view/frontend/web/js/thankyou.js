/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    "jquery",
    "Magento_Customer/js/section-config",
    "Magento_Customer/js/customer-data"
], function($,config,storage) {
    'use strict';
    try {
        var sections = config.getAffectedSections('checkout/onepage/saveOrder');
        if(!sections) {
            sections = ["cart", "checkout-data", "last-ordered-items", "messages"];
        }
        storage.reload(sections);
    } catch (Err) {
        //fuck off
        console.log(Err);
        try {
            localStorage.removeItem('mage-cache-storage');
        } catch(Err) {
            console.log(Err);
        }
    }
});
