/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    "jquery",
    'Magento_Ui/js/modal/alert',
    "jquery/ui",
    "mage/translate",
    "mage/mage",
    "mage/validation",
    "tinyCheck"
], function($, alert){
    "use strict";
    $.widget('mage.nwtKlarnaCheckout', {
	options: {
            countryFormSelector: '#country-form',
            shippingMethodFormSelector: '#shipping-method-form',
            newsletterFormSelector : '#nwtkco-newsletter-form',
            commentFormSelector:  '#klarnakassan-comment',
            couponFormSelector: '#discount-coupon-form',
            waitLoadingContainer: '#review-please-wait',
            ctrlkey: null,
            ctrlcookie: 'NwtKCOCartCtrlKey',
            klarnaShippingAddressChange: false,
	},
	
      _create: function () {
            this._bindEvents();
            this.klarnaApiChanges();
            this.uiManipulate();
        },

        _bindEvents: function(block) {
            //$blocks = ['shipping_method','cart','coupon','messages', 'klarna','newsletter','comment','country'];

            block = block?block:null;
            if(!block || block == 'shipping_method') {
                $(this.options.shippingMethodFormSelector).find('input[type=radio]').on('change', $.proxy(this._changeShippingMethod, this));
            }
            if(!block || block == 'newsletter') {
                $(this.options.newsletterFormSelector).find('input[type=checkbox]').on('change', $.proxy(this._changeSubscriptionStatus, this));
            }
            if(!block || block == 'comment') {
                $(this.options.commentFormSelector).on('submit', $.proxy(this._saveComment,this));
                this.checkValueOfInputs($(this.options.commentFormSelector));
            }
            if(!block || block == 'coupon') {
                $(this.options.couponFormSelector).on('submit', $.proxy(this._applyCoupon,this));
                this.checkValueOfInputs($(this.options.couponFormSelector));
            }
            if(!block || block == 'country') {
                $(this.options.countryFormSelector).find('select').on('change', $.proxy(this._changeCountry, this));
            }
            this.makeTinycheck();
            
        },

        checkValueOfInputs : function(form){
            var checkValue = function(elem){
                if($(elem).val()){
                    form.find('.primary').show();
                } else {
                    form.find('.primary').hide();
                }
            }
            var field = $(form).find('.nwtkco-show-on-focus').get(0);
            $(field).on("keyup", function(){
                checkValue(this)
            });
            $(field).on("change", function(){
                checkValue(this)
            });
        },

	
        /**
         * show ajax loader
         */
        _ajaxBeforeSend: function () {
            if(window._klarnaCheckout)  try { window._klarnaCheckout(function (api) {api.suspend();}); } catch(err) {}
            $(this.options.waitLoadingContainer).show();
        },

        /**
         * hide ajax loader
         */
        _ajaxComplete: function () {
            if(window._klarnaCheckout)  try { window._klarnaCheckout(function (api) {api.resume();}); } catch(err) {}
            $(this.options.waitLoadingContainer).hide();
        },

        _changeShippingMethod:function() { this._ajaxFormSubmit($(this.options.shippingMethodFormSelector)); },
        _changeCountry:function() { 
            $(this.options.waitLoadingContainer).html('Saving country, wait...').show();
            $(this.options.countryFormSelector).submit(); },
        _changeSubscriptionStatus:function() { this._ajaxFormSubmit($(this.options.newsletterFormSelector)); },
        _saveComment:function() { this._ajaxFormSubmit($(this.options.commentFormSelector)); return false;},
        _applyCoupon:function() { this._ajaxFormSubmit($(this.options.couponFormSelector)); return false;},

      
        _ajaxFormSubmit: function (form) {
            return this._ajaxSubmit(form.prop('action'),form.serialize());
        },
        /**
         * Attempt to ajax submit order
         */
        _ajaxSubmit: function (url,data,method) {
            
            if(!method) method = 'post';
            
            if ($(this.options.waitLoadingContainer).is(":visible")) {
                return false;
            }
            
            $.ajax({
                url: url,
                type: method,
                context: this,
                data: data,
                dataType: 'json',
                beforeSend: this._ajaxBeforeSend,
                complete: this._ajaxComplete,
                success: function (response) {
                    if ($.type(response) === 'object' && !$.isEmptyObject(response)) {
                        
                        if(response.reload || response.redirect) {
                            this.loadWaiting = false; //prevent that resetLoadWaiting hiding loader
                            if(response.messages) {
                                //alert({content: response.messages});
                                $(this.options.waitLoadingContainer).html('<span class="error">'+response.messages+' Reloading...</span>');
                            } else {
                                $(this.options.waitLoadingContainer).html('<span class="error">Reloading...</span>');
                            } 
                            
                            if (response.redirect) {
                                window.location.href = response.redirect;
                            } else {
                                window.location.reload();
                            }
                            return true;
                        } //end redirect   
                        
                        //ctlKeyy Cookie
                        if(response.ctrlkey) {
                            this.options.ctrlkey = response.ctrlkey;
                            $.mage.cookies.set(this.options.ctrlcookie, response.ctrlkey);
                        }
                        
                        if(response.updates) {

                            var blocks = response.updates;
                            var div = null;

                            for (var block in blocks) {
                                if(blocks.hasOwnProperty(block)){
                                    div = $('#nwtkco_'+block);
                                    if(div.size()>0) {
                                        div.replaceWith(blocks[block]);
                                        this._bindEvents(block);
                                    }
                                }
                            }

                        }
                        if(response.messages) {
                            alert({
                                content: response.messages
                            });
                        }

                    } else {
                        alert({
                            content: $.mage.__('Sorry, something went wrong. Please try again (reload this page)')
                        });
                       // window.location.reload();
                    }
                },
                error: function () {
                    alert({
                        content: $.mage.__('Sorry, something went wrong. Please try again later.')
                    });
                    //window.location.reload();
//                     this._ajaxComplete();
                }
            });
        },
        
        klarnaApiChanges: function() {
            
            if(!window._klarnaCheckout) return false;
            var self = this;
            if(this.options.klarnaShippingAddressChange) {
                window._klarnaCheckout(function(api) {api.on({'shipping_address_change': function(data) {
                    console.log('shipping_address_change',self.options.klarnaShippingAddressChange,data);
                    if(data) {
                        console.log('Updating...',$.param(data));
                        self._ajaxSubmit(self.options.klarnaShippingAddressChange,$.param(data));
                    }
                }});});
            }
        },
        
 
        /**
         * UI Stuff
         */
        getViewport : function(){
            var e = window, a = 'inner';
            if (!('innerWidth' in window )) {
                a = 'client';
                e = document.documentElement || document.body;
            }
            return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
        },
        makeTinycheck : function(){
            $('input[type="radio"], input[type="checkbox"]').tinyCheck();
        },
        sidebarFiddled : false,
        fiddleSidebar : function(){
            var t = this;
            if( (this.getViewport().width <= 960) && !this.sidebarFiddled ){
                $('.mobile-collapse').each(function(){
                    $(this).collapsible('deactivate');
                    t.sidebarFiddled = true;
                });
            }
        },
        uiManipulate : function(){
            var t = this;
            $(window).resize(function(){
                t.fiddleSidebar();
            });
            $(document).ready(function(){
                t.fiddleSidebar();
            });
        },

	
 });
    
    return $.mage.nwtKlarnaCheckout;
});
