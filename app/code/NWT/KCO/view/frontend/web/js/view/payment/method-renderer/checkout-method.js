/**
 *
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
 */

/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'mage/url'
    ],
    function ($,Component,url) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'NWT_KCO/payment/checkout'
            },
            continueToNwtkco: function () {
                $.mage.redirect(url.build('nwtkco'));
                return false;
            }
        });
    }
);
