<?php
/**
 * Klarna Checkout extension
 *
 * PHP version ~5.5.0|~5.6.0|~7.0.0
 *
 * @category  NWT
 * @package   NWT_KCO
 * @author    Nordic Web Team <support@nodicwebteam.se>
 * @copyright 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license   NWT Commercial License (NWTCL 1.0)
 * @link      http://extensions.nordicwebteam.se/klarna-checkout-2.html
 */


namespace NWT\KCO\Logger\Handler;
use NWT\KCO\Logger;

class Push extends \Magento\Framework\Logger\Handler\System
{

    /**
     * @var string
     */
    protected $fileName = '/var/log/nwtkco-push.log';

    /**
     * @var int
     */
    protected $loggerType = Logger::KCOPUSH;

}
