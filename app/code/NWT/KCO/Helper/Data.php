<?php
/**
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
 */

namespace NWT\KCO\Helper;

/**
 * NWT module base helper
 *
 */
class Data extends \NWT\Base\Helper\Data
{
    const XML_NWT_CONFIG_PATH = 'nwtkco/settings/'; //used by Base/Helper/Data for magic get(StoreConfig)

    const COOKIE_PRODUCTS_TOOGLED     = 'NwtKCOProductsToogled';
    const COOKIE_CART_CTRL_KEY        = 'NwtKCOCartCtrlKey';

    /**
     * Klarna Locale
     *
     * @var \NWT\KCO\Model\Klarna\Locale
     */
    protected $_klarnaLocale;


    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \NWT\KCO\Model\Klarna\Locale $klarnaLocale
    )
    {
        $this->_klarnaLocale = $klarnaLocale;
        parent::__construct($context);
    }

    public function getCheckoutPath($path = null) {
        if(empty($path)) return 'nwtkco';
        
        return 'nwtkco/order/'.trim(ltrim($path,'/'));
    }

    public function getCheckoutUrl($path = null, $params = array())   {
        if(empty($path)) {
          return $this->_getUrl('nwtkco',$params);
        }
        return $this->_getUrl($this->getCheckoutPath($path),$params);
    }


    public function getTermsUrl($store = null) {

        //if there are multiple pages with same url key; magento will generate options with key|id
        //@see Mage_Cms_Model_Resource_Page_Collection::toOptionArray

        $url = explode('|',(string)$this->getStoreConfig(self::XML_NWT_CONFIG_PATH.'terms_url',$store));
        return $url[0];

    }


    public function getBuyTermsUri($store = null) {
        return  $this->_getUrl('',array('_direct'=>(string)$this->getTermsUrl(),'_store'=>$store));
    }


    public function getMinimumAge($store = null) {
        if($this->isMinimumAgeRequired($store)) {
            return (int)$this->_getMinimumAge();
        }
        return 0;
    }


    public function getKlarnaLocale() {
        return $this->_klarnaLocale;
    }

    protected $_countries = null;
    protected $_countryOptions = null;

    public function getCountries($store = null) {

        if(is_null($this->_countries)) {

            $mageAllowCountries = preg_split("#\s*[ ,;]\s*#",strtoupper((string)$this->getStoreConfig('general/country/allow',$store)),null, PREG_SPLIT_NO_EMPTY);
            $klrnAllowCountries = $this->_klarnaLocale->getCountries();
            $this->_countries = array_intersect($mageAllowCountries,$klrnAllowCountries);
            //KCO specific country
            if((int)$this->getAllowspecific()> 0) {
                $specificCountries =  preg_split("#\s*[ ,;]\s*#",strtoupper((string)$this->getSpecificcountry()),null,PREG_SPLIT_NO_EMPTY);
                if($specificCountries) {
                    $this->_countries = array_intersect($this->_countries,$specificCountries);
                }
            }
            //add default country first
            $defaultCountry = strtoupper(trim((string)$this->getCountry()));
            $key = array_search($defaultCountry, $this->_countries);
            if($key) { //intentionally not tested with  !== false, if is on first position do nothing
                unset($this->_countries[$key]);
                array_unshift($this->_countries,$defaultCountry); //add default on first position
            }
        }
        return $this->_countries;
    }

    public function getCountryOptions($store = null) {

        if(is_null($this->_countryOptions)) {

            $this->_countryOptions = array();
            $countries = $this->getCountries($store);
            $wNames = $this->_klarnaLocale->countryHash();
            foreach($countries as $country) {
                $this->_countryOptions[$country] = $wNames[$country];
            }
        }
        return $this->_countryOptions;
    }

    public function getDefaultLocale($store = null) {
        $countries = $this->getCountries($store);
        if(!$countries) {
            return false;
        }
        $locale = $this->_klarnaLocale->getCountry($countries[0]);
        if(empty($locale['locale'])) {
            return false;
        }
        return $locale['locale'];
    }



    public function getCheckoutLinks($store = null) {
        $links = trim($this->getStoreConfig(self::XML_NWT_CONFIG_PATH.'checkout_links',$store));
        return preg_split("#\s*[ ,;]\s*#", $links, null, PREG_SPLIT_NO_EMPTY);
    }

    public function getCartCtrlKeyCookieName()          { return self::COOKIE_CART_CTRL_KEY; }

    public function subscribeNewsletter($quote) {

        if($quote->getPayment()) {
            $status = (int)$quote->getPayment()->getAdditionalInformation("nwtkco_newsletter");
        } else {
            $status = null;
        }

        if($status) { //when is set (in quote) is -1 for NO, 1 for Yes
            return $status>0;
        } else { //get default value
            return $this->getStoreConfigFlag(self::XML_NWT_CONFIG_PATH.'newsletter_subscribe',$quote->getStore()->getId());
        }
    }
    
    public function isB2bAvailable($store=null){
        return $this->getUseB2b($store) && $this->b2bAllowedForLocale($store);
    }


    public function b2bAllowedForLocale($store=null) {
        
        $countries = $this->getCountries($store);
        if(!$countries) {
            return false;
        }
        $locale = $this->_klarnaLocale->getCountry($countries[0]);
        return !empty($locale['company_allowed']);
    }
    

    public function _hexIsValid($hex){
        $hex = str_replace('#','',$hex);
        return ctype_xdigit($hex);
    }


    public function replaceCheckout($store = null) {
        return $this->isEnabled($store) && $this->_replaceCheckout($store);
    }

    //this is not (anymore) required, KLARNA (finally made their iframe resposnsive)
    //we required here (to return false), for older phtml files, which still used it
    public function changeLayout($store = null) {
        return false;
    }








    public function getQuoteSignature($quote) {


        $billingAddress = $quote->getBillingAddress();
        if(!$quote->isVirtual()) {
            $shippingAddress = $quote->getShippingAddress();
        }

        $info = array(
            //'store'   =>$quote->getStore()->getId(), -- with a wrong cookie (store), magento will reset store when we choose default store
            'currency'=>$quote->getQuoteCurrencyCode(),
            //'customer'=>$quote->getCustomer()->getId(), -- this is updated when quote is placed, could be changed
            'shipping_method'=>$quote->isVirtual()?null:$shippingAddress->getShippingMethod(),
            'shipping_country' =>$quote->isVirtual()?null:$shippingAddress->getCountryId(),
            'billing_country' =>$billingAddress->getCountryId(),
            'payment' =>$quote->getPayment()->getMethod(),
            'subtotal'=>sprintf("%.2f",round($quote->getBaseSubtotal(),2)), //store base (currency will be set in checkout)
            'total'=>sprintf("%.2f",round($quote->getBaseGrandTotal(),2)),  //base grand total
            'items'=>array()
        );

        foreach($quote->getAllVisibleItems() as $item) {
            $info['items'][$item->getId()] = sprintf("%.2f",round($item->getQty()*$item->getBasePriceInclTax(),2));
        }
        ksort($info['items']);
        return md5(serialize($info));

    }


    //Magically (shorthand) getStoreConfig,
    // the helper need to define the "prefix" config path
    // ex: const XML_NWT_CONFIG_PATH = 'nwtkco/settings/';
    // usage
    // $this->getAlaBalaPorcala() === $this->scopeConfig->getValue ('nwtkco/settings/ala_bala_portocala',...)
    // $this->isEnabled() === $this->scoppeConfig->isSetFlag('nwtkco/settings/enabled'..)
    public function __call($method, $args)
    {

        //get config

        $store = isset($args[0]) ? $args[0] : null;

        if($method{0} == '_') {
            // _isEnable will be similar with isEnable, _getTitle with getTitle..., _removeCheckoutLinks with removeCheckoutLinks
            // usefull when want to override a method and use default getter
            $method = substr($method,1);
        }

        //getBuyTermsUri => return getStoreConfig('payment/nwtkco/buy_terms_uri')
        if(substr($method, 0, 3) == 'get') {
            $key = self::XML_NWT_CONFIG_PATH.$this->_underscore(substr($method,3));
            return $this->scopeConfig->getValue(
                $key,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $store
            );
        }

        if(substr($method, 0, 2) == 'is') {
            //isCheckoutActive => return getStoreConfigFlag('payment/nwtkco/checkout_active')
            $key = self::XML_NWT_CONFIG_PATH.$this->_underscore(substr($method,2));
        }  else {
            //removeButtons => return getStoreConfigFlag('payment/nwtkco/remove_buttons')
            $key = self::XML_NWT_CONFIG_PATH.$this->_underscore($method);
        }

        return $this->scopeConfig->isSetFlag(
            $key,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }


    protected function _underscore($name)
    {
        $result = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $name));
        return $result;
    }


}
