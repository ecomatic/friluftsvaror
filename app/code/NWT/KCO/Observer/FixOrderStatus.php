<?php
/**
 *
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
*/
namespace NWT\KCO\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

/**
 * KCO module observer
 *
 *          Fix Order Status, magento (wrong) set status to "Suspected Fraud" when currency, other than base, is used 

 *
 */
 
class FixOrderStatus implements ObserverInterface
{

 

    /**
     *  Fix Order Status, magento (wrong) set status to "Suspected Fraud" when currency, other than base, is used 
     *          https://github.com/magento/magento2/issues/4263
     *          https://github.com/magento/magento2/commit/e04fc7ff724de3b865a27311b78d88370a3a0cdb
     *          
     *          Magento\Sales\Model\Order\Payment::isCaptureFinal($amountToCapture)
     *           {
     *               $total = $this->getOrder()->getTotalDue();  <== NEED TO BE getBaseTotalDue
     *               return $this->formatAmount($total, true) == $this->formatAmount($amountToCapture, true);
     *           } 
     * @event sales_order_payment_place_end
     * @see  Magento\Sales\Model\Order\Payment::place
     * @param EventObserver $observer
     * @return void
     */
     
    public function execute(EventObserver $observer)
    {

        $payment = $observer->getEvent()->getPayment();
        //throw new \Exception("X".$payment->getMethod()."Y");
        

        if($payment->getMethod() == 'nwtkco' && $payment->getNwtkcoState()) {
            $payment->getOrder()
                ->setState($payment->getNwtkcoState())
                ->setStatus($payment->getNwtkcoStatus());
               $payment->setIsFraudDetected(false);
        }
        
        return $this;
        
        
    }
    
 }

