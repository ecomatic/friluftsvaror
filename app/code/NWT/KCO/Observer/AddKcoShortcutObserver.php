<?php
/**
 *
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
*/
namespace NWT\KCO\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

/**
 * PayPal module observer
 */
class AddKcoShortcutObserver implements ObserverInterface
{

    /**
     * @var \NWT\KCO\Helper\Data
     */
    protected $helper;

    public function __construct(\NWT\KCO\Helper\Data $helper)
    {
        $this->helper = $helper;
    }


    /**
     * Add PayPal shortcut buttons
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        if(!$this->helper->isEnabled()) return $this;
        
        if($this->helper->replaceCheckout()) return $this; //do nothing, we will replace checkout url with nwtkco, do not add extra button

        if($observer->getEvent()->getIsCatalogProduct()) return $this; //do nothing
         
         
        /** @var \Magento\Catalog\Block\ShortcutButtons $shortcutButtons */
        $shortcutButtons = $observer->getEvent()->getContainer();
   


        $params = [];
        $params['checkoutSession'] = $observer->getEvent()->getCheckoutSession();
        

        // we believe it's \Magento\Framework\View\Element\Template
        $shortcut = $shortcutButtons->getLayout()->createBlock(
            'NWT\KCO\Block\Checkout\Shortcut',
            '',
            $params
        );
        $shortcut->setIsInCatalogProduct(
            $observer->getEvent()->getIsCatalogProduct()
        )->setShowOrPosition(
            $observer->getEvent()->getOrPosition()
        );
        $shortcutButtons->addShortcut($shortcut);
        
    }
    
 }

