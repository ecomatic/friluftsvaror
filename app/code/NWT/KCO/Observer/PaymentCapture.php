<?php
/**
 *
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
*/
namespace NWT\KCO\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

/**
 * KCO module observer
 */
class PaymentCapture implements ObserverInterface
{

 

    /**
     * Set invoice on payment (we need it into capture method)
     *
     * @event sales_order_payment_capture
     * @see  Magento\Sales\Model\Order\Payment\Operation\CaptureOperation::capture
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {

        $payment = $observer->getEvent()->getPayment();
        $invoice = $observer->getEvent()->getInvoice();
        
        $payment->setCapturedInvoice($invoice);
        return $this;
        
        
    }
    
 }

