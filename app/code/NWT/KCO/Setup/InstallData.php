<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
 */

namespace NWT\KCO\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * Init
     *
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(CustomerSetupFactory $customerSetupFactory)
    {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();
        
	$customerSetup->addAttribute(
	    'customer_address',
	    'care_of',
	    [
		    'type' => 'static',
		    'label' => 'Care of',
		    'input' => 'text',
		    'required' => false,
		    'system' => false,
		    'sort_order' => 60,
		    'validate_rules' => 'a:2:{s:15:"max_text_length";i:255;s:15:"min_text_length";i:1;}',
		    'position' => 60, //after company, @see \Magento\Customer\Setup\CustomerSetup                
	    ]
	);
	$customerSetup->getEavConfig()->getAttribute('customer_address', 'care_of')
	    ->setData('used_in_forms', [
		'adminhtml_customer_address',
		'customer_address_edit',
		'customer_register_address'
	      ])
	      ->save();

        $setup->endSetup();
    }
}
