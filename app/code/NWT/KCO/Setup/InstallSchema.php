<?php
/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     NWT Commercial License (NWTCL 1.0)
 *
 */

namespace NWT\KCO\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{


    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        /**
         * Prepare database for install
         */
        $installer->startSetup();
        $connection = $installer->getConnection();

        $pushTable = $installer->getTable('nwtkco_push');
        $installer->run("
CREATE TABLE IF NOT EXISTS `{$pushTable}` (
  `entity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kid` varchar(255) NOT NULL,
  `marshal` LONGBLOB COMMENT 'Klarna order data, serialized',
  `origin` varchar(255) NOT NULL DEFAULT '' COMMENT 'Who create the request, confirmation or push',
  `error`  tinyint(3) NOT NULL,
  `error_msg` TEXT,
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_ORDER_ID` (`kid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


	  //add this fiels in <fieldsets><sales_convert_quote> in config.xml (to copy from quote to order, when save)

	  //Need by push action
	  $quoteData   = array(
	      'nwt_reservation'=>'VARCHAR(255) NULL',
	  );
	  $orderData   = array(
	      'nwt_reservation'=>'VARCHAR(255) NULL',
	      'nwt_kid'=>"int(11) unsigned DEFAULT NULL  COMMENT 'Klarna Push Request ID' AFTER `nwt_reservation`"
	  );


	  $addressData = array(
	      'care_of' => 'VARCHAR(255) NULL AFTER `company`'
	  );

	  $alterTables = array(
	      'quote'=>$quoteData,
	      'quote_address'=>$addressData,
	      'sales_order'=>$orderData,
	      'sales_order_address'=>$addressData
	  );
	  

	  foreach($alterTables as $_table=>$columns) {

	      $table = $installer->getTable($_table);
	      $tableInfo = $connection->describeTable($table);
	      foreach($columns as $column=>$definition) {
		  if (isset($tableInfo[$column])) continue;
		  
		  $connection->addColumn($table,$column,$definition); ////@see lib/Varien/Db/Adapter/Pdo/Mysql.php
		  //Actually, we need to use Magento\Sales\Setup\SalesSetup::addAttribute to add this atttributes, but...
		  //all this tables are converted (long time ago, from M1) to the flat one, so, will works (trust me, I'm engineer, "merge si asa")
	      }
	      

	  }
	  

      $_table = 'quote';
      $table = $installer->getTable($_table);
      $idxName  = $installer->getIdxName($_table,['nwt_reservation']);
      $connection->addIndex($table, $idxName, ['nwt_reservation']);

        $_table = 'sales_order';
        $table = $installer->getTable($_table);
        $idxName  = $installer->getIdxName($_table,['nwt_reservation']);
        $connection->addIndex($table, $idxName, ['nwt_reservation']);

		$idxName  = $installer->getIdxName($_table,['nwt_kid'],\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE);
		$connection->addIndex($table, $idxName, ['nwt_kid'],\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE);





        /**
         * Prepare database after install
         */
        $installer->endSetup();

    }
   
    
}
