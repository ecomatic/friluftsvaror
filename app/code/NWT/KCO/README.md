## Synopsis

NWT Klarna Checkout module

#### 1.0.9 -> Sidebar now uses the config colors so it's in tone with the iframe


## Contributors

https://www.nordicwebteam.se

## Installation

### Manual
 *download and copy all files  into app/code/NWT/KCO/* directory

### Git (Submodule)

  * if you use git into your magento folder, use submodule (or modman)

 `git submodule add git@bitbucket.org:nordicwebteam/nwt-kco-m2.git app/code/NWT/KCO`
 
  * or, if u don't use git into magento folder, use 

   `git clone git@bitbucket.org:nordicwebteam/nwt-kco-m2.git app/code/NWT/KCO`

### Modman
  `modman clone git@bitbucket.org:nordicwebteam/nwt-kco-m2.git` (the module will be deployed into **app/code/NWT/KCO**)

### Composer

* the module will be deployed into `vendor/nwt/kco`

   Add 

     {
        "type": "vcs",
        "url": "git@bitbucket.org:nordicwebteam/nwt-base-m2.git"
     },
     {
        "type": "vcs",
        "url": "git@bitbucket.org:nordicwebteam/nwt-kco-m2.git"
     }

   into **composer.json, repositories section** (if you already have nwt-base-m2 added, skip it)
    
   Run 

     composer require --prefer-source  "nwt/kco:*"

   After that, 

     bin/magento module:enable --clear-static-content NWT_Base NWT_KCO
     bin/magento setup:upgrade
