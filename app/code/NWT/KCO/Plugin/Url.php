<?php
namespace NWT\KCO\Plugin;

class Url {

    /**
     * @var \NWT\KCO\Helper\Data
     */
    protected $helper;

    public function __construct(\NWT\KCO\Helper\Data $helper)
    {
        $this->helper = $helper;
    }

    public function afterGetCheckoutUrl($subject,$result)
    {

        if ($this->helper->replaceCheckout()) {
            return $this->helper->getCheckoutUrl();
        }
        return $result;

    }
}