<?php

namespace NWT\KCO\Controller\Order;

class SaveComment extends \NWT\KCO\Controller\Order\Update
{

    /**
     * Order success (thankyou) action
     */

    public function execute()
    {
        if ($this->_expireAjax()) {
            return;
        }
	    $checkout =  $this->getKlarnaCheckout();

        try {
            //init/check checkout
            $comment = $this->getRequest()->getPost('kco_customer_comment', '');
            $quote = $this->getKlarnaCheckout()->getQuote();
            $quote->setCustomerNote($comment)->setCustomerNoteNotify(false);
            $quote->save();
 
        }  catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                $e->getMessage()
            );
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                __('We can\'t update your comment.')
            );
        }
        $this->_sendResponse('comment',$updateCheckout = false);
    }

}

