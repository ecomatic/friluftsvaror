<?php
namespace NWT\KCO\Controller\Order;

class Confirmation extends \NWT\KCO\Controller\Checkout
{


   /**
     * Place the order (if not) and redirect to thankyou page
     *
     */
    public function execute()
    {


        $kID   = $this->getRequest()->getParam('kid');
        $test  = (int)$this->getRequest()->getParam('test',-1);
        $store = (int)$this->getRequest()->getParam('store',-1);
  
        $prefix = "CONF ($kID,$test,$store)";
        $this->_logPush("START",$prefix);

        if(!$kID || $test<0 || $store<0) {
            $this->_logPush("INVALID (redirect user to cart)",$prefix);
            $this->messageManager->addError(__('Invalid request'));
            $this->_redirect('checkout/cart');
            return;
        }


        try {

            $order = $this->_placeOrder($kID,$test,$store,$prefix);

            if($order->getIsAlreadyPlaced()) {
                $this->_logPush("Found already placed order {$order->getIncrementId()}",$prefix);
            } else {
                $this->_logPush("Order {$order->getIncrementId()} was created",$prefix);
            }
        } catch(\Exception $e) {

            $this->getCheckoutSession()->unsKlarnaOrderUri(); //remove Klarna Order

            $code = $e->getCode();
            $this->_logPush("FAIL, [{$code}] {$e->getMessage()}",$prefix);
            switch($code) {
                case self::ERR_KLARNA_FETCH: //cannot fetch Klarna order
                    echo __('<h1 style="color:red">Cannot fetch the Klarna confirmation</h1><p>Please <a href="%1">refresh this page</a> or wait for confirmation (the order will be placed when we get the Klarna confirmation). Please contact us if you don\'t receive the order confirmation (by email).</p><p><br><em>If you</em> <a href="%2">go back</a><em>, a new order will be created (and the previous one will be canceled)</em></p>',$this->helper()->getCheckoutUrl('confirmation'),$this->helper()->getCheckoutUrl());
                    exit("<p><br><br>{$e->getMessage()}</p>");
                case self::ERR_KLARNA_INVALID: //invalid response, missing status or reservation
                    $this->messageManager->addError(__("The order was NOT placed, %1",$e->getMessage()));
                    $this->messageManager->addNotice('<strong>'.__("Please review the order and retry to place it.").'</strong>');
                    break;
                case self::ERR_KLARNA_STATUS: //not expected status (complete/created)
                    $this->messageManager->addError(__('Please complete the checkout, the Klarna order is not complete.'));
                    break;
                case self::ERR_KLARNA_ORDER_CANCELED: //cannot place order, reservation was canceled
                    $this->messageManager->addError(__('Cannot place the order, %1',$e->getMessage()));
                    $this->messageManager->addNotice('<strong>'.__("Please review the order and retry to place it, the previous order was CANCELED.").'</strong>');
                    break;
                default:
                    //order cannot be placed (and Klarna reservation, if any, was NOT cacneled)
                    $this->messageManager->addError(__('Cannot place the order, %1',$e->getMessage()));
                    $this->messageManager->addNotice('<strong>'.__("Please review the order and retry to place it, the previous order will be canceled.").'</strong>');
                    break;
            }
            $this->_redirect('*');
            return;
        }

        $klarnaOrder = $order->getKlarnaOrder();



        //clear LastOrderId, LastRecurringProfileIds, RedirectUrl, LastBillingAgreementId etc.

        $session = $this->getCheckoutSession();

        $session->clearHelperData();
        $session->clearQuote()->clearStorage();


        $session
            ->setLastQuoteId($order->getQuoteId())
            ->setLastSuccessQuoteId($order->getQuoteId())
            ->setLastOrderId($order->getId())
            ->setLastRealOrderId($order->getIncrementId())
            ->setLastOrderStatus($order->getStatus())
        ;


        //set this in session, to avoid refetch klarna order (from klarna server), in thank you page
        $this->getCheckoutSession()->setKlarnaOrder($klarnaOrder->marshal());

        $this->_redirect('*/*/thankyou');
        $this->_logPush("END",$prefix);
    }

    
}