<?php
namespace NWT\KCO\Controller\Order;

class Thankyou extends \NWT\KCO\Controller\Checkout
{

    /**
     * Order success (thankyou) action
     */

    public function execute()
    {




        //Just LAYOUT test
        if($this->getRequest()->getParam('nwtkco')=='test') {
            return $this->_test();
        }

        
        $session = $this->getCheckoutSession();
        if (!$this->_objectManager->get('Magento\Checkout\Model\Session\SuccessValidator')->isValid()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        $this->coreRegistry->register('KlarnaOrder', $session->getKlarnaOrder());
        $lastOrderId = $session->getLastOrderId();
        
        $session->clearQuote(); //destroy quote, unset QuoteId && LastSuccessQuoteId
        $session->unsKlarnaOrderUri(); //unset klarna location
        $session->unsKlarnaOrder(); //unset klarna order

        $title = $this->helper()->getThankyouTitle();
        if(!$title) $title = __("Klarna Checkout");

        $resultPage = $this->resultPageFactory->create();  //need to be BEFORE event dispach (GA need to have layout loaded, to set orderIds on block)
        $resultPage->getConfig()->getTitle()->set($title);

        $this->_eventManager->dispatch(
            'checkout_onepage_controller_success_action',
            ['order_ids' => [$lastOrderId]]
        );

        return $resultPage;
    }

    protected function _test() {

        $mid = $this->getRequest()->getParam('mid');
        $kid = $this->getRequest()->getParam('kid');
        if(!(
            $mid &&
            $kid &&
            ($order =  $this->orderFactory->create()->load($mid)) &&
            $order->getId() &&
            $order->getNwtReservation() == $kid
        )) {
            exit('Marsh...');
        }


        $pushOrder = $this->_objectManager->create('NWT\KCO\Model\Push')->load($order->getNwtKid());
        if($pushOrder->getMarshal()) {
            $this->coreRegistry->register('KlarnaOrder',unserialize($pushOrder->getMarshal()));
        }

        $session = $this->getCheckoutSession();
        $session->setLastRealOrderId($order->getIncrementId());
        $resultPage = $this->resultPageFactory->create(); //need to be before dispatch, else GA event will don't have block
        $title = $this->helper()->getThankyouTitle();
        if(!$title) $title = __("Klarna Checkout");


        $session->unsKlarnaOrder();

        $resultPage->getConfig()->getTitle()->set($title." - TEST[{$mid},{$kid}]");


        $this->_eventManager->dispatch(
            'checkout_onepage_controller_success_action',
            ['order_ids' => [$order->getId()]]
        );
        //from don't know what reason this is lost
        $session->setLastRealOrderId($order->getIncrementId());


        return $resultPage;

    }
    
}