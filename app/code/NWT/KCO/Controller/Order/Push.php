<?php
namespace NWT\KCO\Controller\Order;

class Push extends \NWT\KCO\Controller\Checkout
{


   /**
     * Place the order (if not) and update klarna order
     *
     */
    public function execute()
    {
        $kID  = $this->getRequest()->getParam('kid');
        $test = (int)$this->getRequest()->getParam('test',-1);
        $store = (int)$this->getRequest()->getParam('store',-1);

        $prefix = "\tPUSH ($kID,$test,$store)";
        $this->_logPush("START",$prefix);

        if(!$kID || $test<0 || $store<0) {
            $this->_logPush("INVALID request",$prefix);
            return $this->_croak('Invalid request');
        }

        //check if we have already started process request (by confirmation)
        $pushRequest = $this->nwtkcoFactory->createPush()->loadByKid($kID,$test,$store);

        if(!$pushRequest->getId()) {
            $this->_logPush("No already started process found, wait (max) 10 seconds",$prefix);
            for($i=0;$i<5;$i++) {
                sleep(2); //wait for confirmation action
                //check again
                $pushRequest = $this->nwtkcoFactory->createPush()->loadByKid($kID,$test,$store);
                if($pushRequest->getId()) {
                    $this->_logPush("Found just already started process (confirmation) #{$pushRequest->getId()}, do nothing (we will wait for other process to finish)",$prefix);
                    exit("The request is processed via confirmation action, we will wait to finish");
                    break;
                }
            }
        } else {
            $age  = $pushRequest->getAge(); //minutes
            if($age < 5) { //5 minutes
                $this->_logPush("Found already started process (confirmation), {$age} minutes old, do nothing (we will wait for other process to finish)",$prefix);
                exit("The request is processed via confirmation action, we will wait to finish");
            } else {
                $this->_logPush("Found already started process (confirmation), {$age} minutes old, check if we have order placed or error",$prefix);
            }
        }

        //recheck Klarna order
        try {
            $klarnaOrder = $this->klarnaFactory->createOrder()->setTestMode($test>0)->fetchForUpdate($kID,null);
            $this->_logPush("Klarna order found (fetched)",$prefix);
        } catch(\Exception $e) {
            $this->_logPush("Cannot fetch Klarna order (was previous canceled?), {$e->getMessage()}",$prefix);
            return $this->_fail("Cannot fetch Klarna order {$kID}, {$e->getMessage()}");
        }

        $klarnaData = $klarnaOrder->marshal();

        //check klarna order status/reservation
        if(empty($klarnaData['status'])) {
            $this->_logPush($msg="Invalid Klarna response (missing status)",$prefix);
            return $this->_bad($msg);
        }
        if($klarnaData['status'] != 'checkout_complete') {
            if($klarnaData['status'] == 'created') {
                $this->_logPush($msg="Klarna order already 'created', do nothing!",$prefix);
            } else {
                $this->_logPush($msg="Invalid status {$klarnaData['status']}, expect checkout_complete",$prefix);
                return $this->_bad($msg);
            }
            exit($msg);
        }

        if(empty($klarnaData['reservation'])) {
            $this->_logPush($msg="Invalid Klarna response (missing reservation)",$prefix);
            return $this->_bad($msg);
        }


        //check if we have already placed order
        $order =  $this->orderFactory->create()->load($klarnaData['reservation'],'nwt_reservation');

        if($order->getId()) {
            try {
                $klarnaOrder->registerOrder($order->getIncrementId());
                $this->_logPush($msg = "Found an already placed order, {$order->getIncremendId()} for {$klarnaData['reservation']} reservation; Order was registered to Klarna",$prefix);
            } catch(\Exception $e) {
                $this->_logPush($msg = "Found an already placed order, {$order->getIncremendId()} for {$klarnaData['reservation']} reservation; FAIL to register order to Klarna, $e->getMessage()",$prefix);
                $this->nwtkcoHelper->logCritical($e);
                return $this->_fail($msg);
            }
            exit($msg);
        }

        //no order placed, check if confirmation request was finished with error
        if($pushRequest->getId() && $pushRequest->getError()) {
            try {
                $klarnaOrder->cancelReservation();
                $this->_logPush($msg="Confirmation process was finished with error [{$pushRequest->getError()}], {$pushRequest->getErrorMsg()}; Reservation was canceled",$prefix);
            } catch(\Exception $e) {
                $this->_logPush($msg="Confirmation process was finished with error [{$pushRequest->getError()}], {$pushRequest->getErrorMsg()}; Fail to cancel reservation, {$e->getMessage()}",$prefix);
                $this->nwtkcoHelper->logCritical($e);
                return $this->_fail($msg);
            }
            exit($msg);
        }


        //the process was started but we don't have an order, also no error
        if($pushRequest->getId()) {
            //TODO: cancel the registration? (the process was started but... we don't have order also no error; )
            $this->_logPush("Found already started process (confirmation), {$age} minutes old, we will do not retry to place order, we will let reservation active (let owner to decide)",$prefix);
            exit("We already trying to place order but... we will not try again, let store owner to decide");
        }


        //try to place order; we assume that the user has never returned to confirmation action
        try {
            $order = $this->_placeOrder($kID,$test,$store,$prefix);
            if($order->getIsAlreadyPlaced()) {
                $this->_logPush("Found already placed order {$order->getIncrementId()}; do nothing.",$prefix);
            } else {
                $this->_logPush("Order {$order->getIncrementId()} was created",$prefix);
            }
        } catch(\Exception $e) {
            $code = $e->getCode();
            $this->_logPush("Cannot register/place order for klarna order {$kID}, [$code] {$e->getMessage()}.",$prefix);
            $this->nwtkcoHelper->logError($msg = __('[%1] Cannot register/place order for klarna order %2. [%2] %4 (see exception.log)',__METHOD__,$kID, $code,$e->getMessage()));
            $this->nwtkcoHelper->logCritical($e);
            exit((string)$msg);
        }
        exit("Klarna Order {$kID}, Magento order {$order->getIncrementId()} was created. END");


    }

    
}