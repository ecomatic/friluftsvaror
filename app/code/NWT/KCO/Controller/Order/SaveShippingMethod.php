<?php

namespace NWT\KCO\Controller\Order;


class SaveShippingMethod extends \NWT\KCO\Controller\Order\Update
{

    /**
     * Save shipping method action
     */

    public function execute()
    {
        if ($this->_expireAjax()) {
            return;
        }

        $shippingMethod = $this->getRequest()->getPost('shipping_method', '');
        if(!$shippingMethod) {
            $this->getResponse()->setBody(json_encode(array('messages'=>'Esti bulangiu')));
            return;
        }


        if($shippingMethod) {
            try {
                $checkout = $this->getKlarnaCheckout();
                $checkout->updateShippingMethod($shippingMethod);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    $e->getMessage()
                );
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('We can\'t update shipping method.')
                );
            }
        }
        $this->_sendResponse();
    }

}

