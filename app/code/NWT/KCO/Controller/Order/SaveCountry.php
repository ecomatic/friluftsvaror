<?php

namespace NWT\KCO\Controller\Order;

use NWT\KCO\Exception as NWTKCOException;
use Magento\Framework\Exception\LocalizedException;

class SaveCountry extends \NWT\KCO\Controller\Checkout
{

    /**
     * Change Country Action
     * this is not AJAX
     */
    

    public function execute()
    {
        $checkout =  $this->getKlarnaCheckout();
        $country = $this->getRequest()->getPost('country', '');        
        $allowCountries = $checkout->allowCountries();
        
        if($country && in_array($country,$allowCountries)) {
            try {
                $checkout->changeCountry($country, $saveQuote = true);
                //$this->messageManager->addSuccess(__("Country was changed [%1]",$country));
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    $e->getMessage()
                );
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('We can\'t update country.')
                );
            }
        } else {
            $this->messageManager->addError(__('Invalid Country'));
        } 
        return $this->resultRedirectFactory->create()->setPath('*');
    }
        
}

