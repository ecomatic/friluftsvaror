<?php
namespace NWT\KCO\Controller\Order;

class ShippingAddressChange extends \NWT\KCO\Controller\Order\Update
{


   /**
     * Update shipping address on Klarna shipping_address_change trigger
     *
     */


    public function execute()
    {
        if ($this->_expireAjax()) {
            return;
        }
        
        $blocks         = [];
        $updateCheckout = false;
        
        $data = $this->getRequest()->getParams();
        
        if($data) {
        
            try {
                //init/check checkout
                $checkout    =  $this->getKlarnaCheckout();
                $qSignBefore = $checkout->getQuoteSignature();
                $checkout->updateShippingAddress($data);
                $qSignAfter  = $checkout->getQuoteSignature();
                $updateCheckout = ($qSignAfter != $qSignBefore);
                
                if($updateCheckout) {
                    $blocks = ['cart','shipping_method','klarna'];
                } else {
                    $blocks = ['shipping_address'];
                }
    
            }  catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    $e->getMessage()
                );
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('We can\'t update shipping address. [%1]',$e->getMessage())
                );
            }
        }
        $this->_sendResponse($blocks,$updateCheckout);
        
    }

    
}