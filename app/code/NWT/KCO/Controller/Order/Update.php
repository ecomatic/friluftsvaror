<?php

namespace NWT\KCO\Controller\Order;

use NWT\KCO\Exception as NWTKCOException;

abstract class Update extends \NWT\KCO\Controller\Checkout
{
    //ajax updates
    protected function _sendResponse($blocks = null,$updateCheckout = true) {


        $response = [];

        //reload the blocks even we have an error
        if(is_null($blocks)) {
            $blocks = ['shipping_method','cart','coupon','messages', 'klarna','newsletter','comment','country'];
        } elseif($blocks) {
            $blocks = (array)$blocks;
        } else {
            $blocks = [];
        }

        if(!in_array('messages',$blocks)) {
            $blocks[] = 'messages';
        }


        $updateKlarna = false;
        if($updateCheckout) {
            $key = array_search('klarna', $blocks);
            if($key !== false) {
                $updateKlarna = true;
                unset($blocks[$key]); //this will be set later
            }
        }

        $checkout = $this->getKlarnaCheckout();
        if($updateCheckout) {  //if blocks contains only "messages" do not update


            try {

                $checkout = $checkout->initCheckout();

                //set new quote signature
                $response['ctrlkey'] = $checkout->getQuoteSignature();

                if($updateKlarna) {
                    //update klarna iframe
                    $checkoutURI = $this->getCheckoutSession()->getKlarnaOrderUri();
                    $klarnaOrder = $checkout->updateKlarnaOrder()->getKlarnaOrder();
                    $this->coreRegistry->register('KlarnaOrder',$checkout->getKlarnaOrder());
                }

            } catch(NWTKCOException $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    $e->getMessage()
                );
                if($e->isReload()) {
                    $response['reload'] = 1;
                    $response['messages'] = $e->getMessage();
                    $this->getCheckoutSession()->addNotice($e->getMessage());
                } elseif($e->getRedirect()) {
                    $response['redirect'] = $e->getRedirect();
                    $response['messages'] = $e->getMessage();
                    $this->messageManager->addError($e->getMessage());
                } else {
                    $this->messageManager->addError($e->getMessage());
                }
            } catch(\Magento\Framework\Exception\LocalizedException $e) {
                //do nothing, we will just show the message
                $this->messageManager->addError($e->getMessage()?$e->getMessage():__('Cannot update checkout (%1)',get_class($e)));
            }  catch(\Exception $e) {
                $this->messageManager->addError($e->getMessage()?$e->getMessage():__('Cannot initialize Klarna Checkout (%1)',get_class($e)));
            }


            if(!empty($response['redirect'])) {
                if($this->getRequest()->isXmlHttpRequest()) {
                    $response['redirect'] =$this->getUrl($response['redirect']);
                    $this->getResponse()->setBody(Zend_Json::encode($response));
                } else {
                    $this->_redirect($response['redirect']);
                }
                return;
            }


            if($updateKlarna &&  (empty($klarnaOrder) || $klarnaOrder->getLocation() != $checkoutURI)) {
                //another klarna order was created, add klarna block (need to be reloaded)
                $blocks[] = 'klarna';
                //if klarna have same location, we will use klarna api resume
            }
        }
        
        $response['ok'] = true; //to avoid empty response
        
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('*');
            return;
        }
        
        $response['ok'] = true;
        if($blocks) {    
        
            $this->coreRegistry->register('quote',$checkout->getQuote());
            $this->_view->loadLayout('nwtkco_order_update');
            foreach($blocks as $id) {
                $name = "nwtkco.{$id}";
                $block = $this->_view->getLayout()->getBlock($name);
                if($block) {
                    $response['updates'][$id] = $block->toHtml();
                }
            }
        }
        $this->getResponse()->setBody(json_encode($response));
        
    }

}

