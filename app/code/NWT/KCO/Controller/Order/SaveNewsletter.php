<?php

namespace NWT\KCO\Controller\Order;

class SaveNewsletter extends \NWT\KCO\Controller\Order\Update
{

    /**
     * Save newsletter subscription action
     */

    public function execute()
    {
        if ($this->_expireAjax()) {
            return;
        }
        try {
            $checkout = $this->getKlarnaCheckout();
            $quote = $this->getKlarnaCheckout()->getQuote();

            $newsletter = (int)$this->getRequest()->getParam('newsletter', 0);
            if ($quote->getPayment()) {
                $quote->getPayment()->setAdditionalInformation('nwtkco_newsletter', $newsletter > 0 ? 1 : -1)->save();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    $e->getMessage()
                );
        } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('We can\'t update your subscription.')
                );
        }
        $this->_sendResponse('newsletter',$updateCheckout = false);
    }

}

