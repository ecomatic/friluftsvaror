<?php


namespace NWT\KCO\Controller;


use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;

abstract class Checkout extends \Magento\Checkout\Controller\Action {


    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;
    
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    
     /**
     * @var \NWT\KCO\Model\Checkout
     */
   protected $klarnaCheckout;
   
     /**
     * @var \NWT\KCO\Helper\Data
     */
   protected $nwtkcoHelper;

    /**
     * @var \NWT\KCO\Logger
     */
    protected $nwtkcoLogger;


    /**
     * @var \NWT\KCO\Model\Factory
     */
    protected $nwtkcoFactory;
    /**
     * @var \NWT\KCO\Model\Klarna\Factory
     */
    protected $klarnaFactory;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

   public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager, 
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderCustomerManagementInterface $orderCustomerService,
        \NWT\KCO\Model\Factory $nwtkcoFactory,
        \NWT\KCO\Model\Klarna\Factory $klarnaFactory,
        \NWT\KCO\Helper\Data $nwtkcoHelper,
        \NWT\KCO\Logger $nwtkcoLogger
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->scopeConfig = $scopeConfig;
        $this->layoutFactory = $layoutFactory;
        $this->quoteRepository = $quoteRepository;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->resultJsonFactory = $resultJsonFactory;
    	$this->storeManager= $storeManager;
    	$this->orderFactory = $orderFactory;
    	$this->quoteFactory = $quoteFactory;
        $this->checkoutSession = $checkoutSession;
        $this->orderCustomerService = $orderCustomerService;
        $this->nwtkcoFactory = $nwtkcoFactory;
        $this->klarnaFactory = $klarnaFactory;
        $this->klarnaCheckout = $nwtkcoFactory->getCheckout();
        $this->nwtkcoHelper = $nwtkcoHelper;
        $this->nwtkcoLogger = $nwtkcoLogger;

        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement
        );
    }


    /**
     * Dispatch request
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {

        $action = $this->getRequest()->getActionName();

        $this->_request = $request;	
        if(!in_array($action,['thankyou','confirmation','push'])) {
		
            $result = $this->_preDispatchValidateCustomer();
            if ($result instanceof \Magento\Framework\Controller\ResultInterface) {
                return $result;
            }


            /** @var \Magento\Quote\Model\Quote $quote */
            $quote = $this->_objectManager->get('Magento\Checkout\Model\Session')->getQuote();
            if ($quote->isMultipleShippingAddresses()) {
                $quote->removeAllAddresses();
            }
            if ($quote->getIsMultiShipping()) {
                $quote->setIsMultiShipping(false);
                $quote->removeAllAddresses();
            }

            if (!$this->_canShowForUnregisteredUsers()) {
                throw new NotFoundException(__('Page not found.'));
            }
        }
        return parent::dispatch($request);
    }




    /**
     * Validate ajax request and redirect on failure
     *
     * @return bool
     */
    protected function _expireAjax()
    {


        if(!$this->getRequest()->isXmlHttpRequest()) {
            return false;
        }
        //check if quote was changed
        $ctrlkey    = (string)$this->getRequest()->getParam('ctrlkey');
        if(!$ctrlkey) {
            return false; //do not check
        }

        //check if cart was updated
        $currkey    = $this->getKlarnaCheckout()->getQuoteSignature();
        if($currkey != $ctrlkey) {
            $response = array(
                'reload'   => 1,
                'messages' =>(string)__('The cart was updated (from another location), reloading the checkout, please wait...')
            );
            $this->getCheckoutSession()->addError($this->__('The requested changes were not applied. The cart was updated (from another location), please review the cart.'));
            $this->getResponse()->setBody(Zend_Json::encode($response));
            return true;
        }

        return false;
    }



    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    protected function _ajaxRedirectResponse()
    {
        $resultRaw = $this->resultRawFactory->create();
        $resultRaw->setStatusHeader(403, '1.1', 'Session Expired')
            ->setHeader('Login-Required', 'true');
        return $resultRaw;
    }




    protected function getKlarnaCheckout()      { return $this->klarnaCheckout; }
    protected function getCheckoutSession()     { return $this->checkoutSession; }
    protected function getSession()             { return $this->checkoutSession; }   
    
    protected $_cart = null;
    protected function getCart()
    {
	if(is_null($this->_cart)) {
	  $this->_cart = $this->_objectManager->get('Magento\Checkout\Model\Cart');
	}
	return $this->_cart;
    }
    
    
    protected function helper()
    {
      return $this->nwtkcoHelper;
    }

    
    
    /**
     * Check can page show for unregistered users
     *
     * @return boolean
     */
    protected function _canShowForUnregisteredUsers()
    {
        return $this->_objectManager->get(
            'Magento\Customer\Model\Session'
        )->isLoggedIn() || $this->getRequest()->getActionName() == 'index' || $this->_objectManager->get(
            'Magento\Checkout\Helper\Data'
        )->isAllowedGuestCheckout(
            $this->getCheckoutSession()->getQuote()
        ) || !$this->_objectManager->get(
            'Magento\Checkout\Helper\Data'
        )->isCustomerMustBeLogged();
    }



    /*   CONFIRMATION/PUSH (place order) PART */

    const ERR_KLARNA_FETCH              = 100; //cannot fetch klarna order
    const ERR_KLARNA_INVALID            = 110; //invalid klarna response (missing required field)
    const ERR_KLARNA_STATUS             = 120; //not expected status (checkout_complete/created)
    const ERR_KLARNA_ORDER_CANCELED     = 200; //cannot place order, reservation was canceled
    const ERR_KLARNA_ORDER_NOT_CANCELED = 210; //cannot place order, reservation was NOT canceled


    protected function _placeOrder($kID,$test,$store,$prefix="") {

        //Yea, i know that this is too complicated, but...  I didn't find a simpler solution (yet!)
        //we want to place order on  confirmation or push action, which came first, and use the result in the other one

        $this->storeManager->setCurrentStore($store);


        //get (or create if not exists) the request for $kID
        //the request will contains (fetch) also the klarna data (in marshal field)

        try {
            $pushRequest = \NWT\KCO\Model\Push::getRequest($kID,$test,$store,$prefix);
            if($pushRequest->getIsAlreadyFetched()) {
                $_fetched = ' (already fetched)';
            } else {
                $_fetched = ' ; Klarna data was fetched';
            }
            if($pushRequest->getIsAlreadyStarted()) {
                $this->_logPush("Found Push history ID {$pushRequest->getId()} for KID {$kID}{$_fetched}",$prefix);
            } else {
                $this->_logPush("Push history ID {$pushRequest->getId()} for KID {$kID} was just CREATED{$_fetched}",$prefix);
            }
        } catch(\Exception $e) {
            $this->_logPush("Failed to fetch request, {$e->getMessage()}",$prefix);
            throw new \Exception($e->getMessage(),self::ERR_KLARNA_FETCH);
        }

        $klarnaData = unserialize($pushRequest->getMarshal());
        

        //if we have a processed request, and the result was an error... try to cancel klarna reservation (if not) and stop here
        if($pushRequest->getError()) {
            $this->_logPush("Already processed request, we have error [{$pushRequest->getError()}], {$pushRequest->getErrorMsg()}",$prefix);
            //refetch klarna order; if still exists we will cancel
            try {
                $klarnaOrder = $this->klarnaFactory->createOrder()->setTestMode($test>0)->fetchForUpdate($kID,null);
                $this->_logPush("Klarna order found (refetched)",$prefix);
                $klarnaOrder->cancelReservation();
                $this->_logPush("Reservation was canceled",$prefix);
            } catch(\Exception $e) {
                $this->_logPush("Cannot refetch and cancel Klarna order (was previous canceled?), {$e->getMessage()}",$prefix);
            }
            throw new \Exception($pushRequest->getErrorMsg(),$pushRequest->getError());
        }

        $err        = null;
        $errCode    = 0;

        //check klarna order status/reservation
        if(empty($klarnaData['status'])) {
            $err     =__('Invalid Klarna response (missing status)');
            $errCode = self::ERR_KLARNA_INVALID;
        } elseif(!in_array($klarnaData['status'],array('checkout_complete','created'))) {
            $err     =__('Invalid status %1, expect checkout_complete or created',$klarnaData['status']);
            $errCode = self::ERR_KLARNA_STATUS;
        } elseif(empty($klarnaData['reservation'])) {
            $err     =__('Invalid Klarna response (missing reservation)');
            $errCode = self::ERR_KLARNA_INVALID;
        }

        if($errCode) {
            try {
                $pushRequest->setError($errCode)->setErrorMsg($err)->save();
            } catch(\Exception $e) {
                $this->_logPush("Error [{$errCode}], [$err]; cannot update push request",$prefix);
                $this->helper()->logError(__METHOD__."{$prefix} Cannot update push request {$pushRequestID}, {$e->getMessage()} (see exception.log)");
                $this->helper()->logError($e);
            }
            throw new \Exception($err,$errCode);
        }

        $prefix = "{$prefix}] [({$klarnaData['status']},{$klarnaData['reservation']})";
        

        //order was already placed ?
        $order = $this->orderFactory->create()->load($klarnaData['reservation'],'nwt_reservation');

        if(!$order->getId() && $pushRequest->getIsAlreadyStarted() && $pushRequest->getAge()<5) {
            //if NO order was placed, but the request alreay exists and is started soon, wait a moment for the other process
            $pushID = $pushRequest->getId();
            $this->_logPush("Push request is already started ({$pushRequest->getAge()} minutes old) but we still don\'t have an order, wait 5 seconds",$prefix);
            sleep(5); //wait other to finish
            $this->_logPush('continue',$prefix);

            //reload request; check we now have an error
            $pushRequest->load($pushID);
            if($pushRequest->getError()) {
                $this->_logPush("Other process was finished with error [{$pushRequest->getError()}], {$pushRequest->getErrorMsg()}",$prefix);
                throw new \Exception($pushRequest->getErrorMsg(),$pushRequest->getError());
            } else {
                $this->_logPush('Push request was reloaded, no error, recheck if now have an order',$prefix);
            }
            //recheck order, maybe now is finished
            $order = $this->orderFactory->create()->load($klarnaData['reservation'],'nwt_reservation');
        }

        //recreate klarnaOrder from request
        $klarnaOrder = $this->klarnaFactory->createOrder()
			    ->setTestMode($test>0)
			    ->setLocation($kID)
			    ->parse($klarnaData);


        if(!$order->getId()) {

            //no order place, no error, try to place order

            $checkout = $this->getKlarnaCheckout();
            $this->_logPush("Order not found, try to place/create",$prefix);
            //try to place order
            $order = null;
            $orderException = null;
            try {
                $order = $checkout->placeOrder($klarnaOrder,$pushRequest->getId(),$test);
                $this->_logPush("Order successfully created {$order->getIncrementId()}",$prefix);
                $order->setIsAlreadyPlaced(false);
            } catch(\Exception $orderException) {
                $this->helper()->logError("Cannot place order for Klarna order {$kID}; reservation {$klarnaData['reservation']}, {$orderException->getMessage()}");
                $this->helper()->logError($orderException);
                $this->_logPush("Order cannot be placed, {$orderException->getMessage()}",$prefix);

                //recheck is we still do not have already placed order
                //if order is already place; we will got duplicate error when trying to replace same quote (because of nwt_kid/increment_id unique fields)
                $order = $this->orderFactory->create()->load($klarnaData['reservation'],'nwt_reservation');
                if(!$order->getId()) {
                    //set error on push request
                    $errCode = (int)$orderException->getCode();
                    try {
                        //update the request with error code/message (we will do not try, later, to place order)
                        $pushRequest->setError($errCode?$errCode:-1)->setErrorMsg($orderException->getMessage())->save();
                    } catch(\Exception $e) {
                        $this->_logPush("Cannot update push request with latest error, $e->getMessage()",$prefix);
                        $this->helper()->logError($orderException);
                    }

                    //save quote
                    $quote = $klarnaOrder->getQuote();
                    if($quote && $quote->getId()) {
                        //save quote
                        //    -if order was saved, need to save "active" field
                        //    -if order was not saved, need to save customer addresses
                        try {
                            $quote->save(); //try to save addresses & other info to the quote
                        } catch(\Exception $e) {
                            $this->_logPush("Cannot save quote {$quote->getId()} (save addresses), {$e->getMessage()}",$prefix);
                            $this->helper()->logError(__METHOD__."  Klarna reservation {$klarnaData['reservation']}, quote {$quote->getId()}; Order was NOT created, cannot save the quote (addresses), {$e->getMessage()}");
                            $this->helper()->logError($e);
                        }
                    }

                    //cancel reservation?
                    try {
                        $klarnaOrder->cancelReservation();
                        $this->_logPush("Reservation was canceled.",$prefix);
                    } catch(\Exception $e) {
                        $this->_logPush("Fail to cancel reservation, {$e->getMessage()}.",$prefix);
                        $this->helper()->logError($e);
                    }
                    throw $orderException;
                }
                $this->_logPush("Found an already placed (by another request) order {$order->getIncrementId()}, use this one",$prefix);
                $order->setIsAlreadyPlaced(true);
            }
        } else {
            $this->_logPush("Found order {$order->getIncrementId()}",$prefix);
            $order->setIsAlreadyPlaced(true);
        }

        $quote = null;
        try {
            //reload quote; need to check is still active
            $quote = $this->quoteFactory->create()->load($order->getQuoteId());
            if($quote->getId()) {
                $klarnaOrder->assignQuote($quote,$validate = false);
            }
        } catch(\Exception $e) {
            $this->_logPush("Cannot load and assign to Klarna Order the quote {$order->getQuoteId()}");
            $this->helper()->logError($e);
        }

        //inactivate quote is still active
        if($quote && $quote->getId()) {
            if($quote->getIsActive()) {
                //inactivate quote
                $quote->setIsActive(false);
                try {
                    $quote->save(); //need to save the quote (after the order is placed, the quote is inactivated)
                    $this->_logPush("Quote {$quote->getId()} was inactivated",$prefix);
                } catch(\Exception $e) {
                    $this->_logPush("Cannot inactivate quote {$quote->getId()}, {$e->getMessage()}",$prefix);
                    $this->helper()->logError(__METHOD__."  Klarna reservation {$klarnaData['reservation']}, quote {$quote->getId()}; Order was created ({$order->getIncrementId()}) but cannot save the quote (mark as inactive), {$e->getMessage()}");
                    $this->helper()->logError($e);
                }
            } else {
                $this->_logPush("Quote {$quote->getId()} already inactivated",$prefix);
            }
        } else {
            $this->_logPush("We do not have a quote?!?",$prefix);
        }


        if($klarnaData['status'] != 'created') {

            //update klarna status (to created) and register magento order id to klarna system
            try {
                $klarnaOrder->registerOrder($order->getIncrementId());
                $this->_logPush("Klarna status was update to created",$prefix);
                $pushRequest->setMarshal(serialize($klarnaOrder->marshal()))->save();
                $this->_logPush("Klarna data was updated in push request",$prefix);
            } catch(\Exception $e) {
                $this->_logPush("Cannot register Klarna order / update klarna status to created, {$e->getMessage()}",$prefix);
                //just log exception, we will try later, via push action (also, we could activate reservation even the status is not created)
                $this->helper()->logError(__METHOD__." Klarna order {$kID}, reservation {$klarnaData['reservation']}, order is placed ({$order->getIncrementId()}) but status cannot be set to created, {$e->getMessage()}");
                $this->helper()->logError($e);
            }
        } else {
            $this->_logPush("Klarna status is already 'created', do not set again",$prefix);
        }

        $order->setKlarnaOrder($klarnaOrder);
        return $order;
    }

    protected function _fail($msg = 'Marsh!')   { header('HTTP/1.1 503 Service Temporarily Unavailable',true, 503); exit($msg); }
    protected function _bad($msg = 'Marsh!')    { header('HTTP/1.1 400 Bad Request', true, 400); exit($msg); }
    protected function _croak($msg = 'Marsh!')  { header('HTTP/1.1 403 Forbidden',true,403); exit($msg); }



    protected $_pushLogger = null;

    protected function _logPush($msg,$prefix=null) {

        if($prefix) {
            $msg = "[{$prefix}] $msg";
        }
       $this->nwtkcoLogger->push($msg);
       return $this;
    }
    
    



}
