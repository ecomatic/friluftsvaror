<?php
/**
 * Klarna Checkout extension
 *
 * @category  NWT
 * @package   NWT_KCO
 * @copyright 2016 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license   NWT Commercial License (NWTCL 1.0)
 */

namespace NWT\KCO\Controller\Index;


use NWT\KCO\Exception as KCOException;

class Index extends \NWT\KCO\Controller\Checkout
{

    /**
     * Klarna Checkout page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $checkout =  $this->getKlarnaCheckout();
        try {
            $checkout->initCheckout($reloadIfCurrencyChanged = false);  //init/check checkout
            $checkout->updateKlarnaOrder(); //Update Klarna iframe
// 	    $data = $checkout->getKlarnaOrder()->marshal();
// 	    unset($data['gui']);
// 	    echo '<pre>',print_r($data,true),'</pre>';
//             exit();
            
            $this->coreRegistry->register('KlarnaOrder', $checkout->getKlarnaOrder());
        } catch (KCOException $e) {
            if ($e->isReload()) {
                $this->messageManager->addNotice($e->getMessage());
            } else {
                $this->messageManager->addError($e->getMessage());
            }
            if ($e->getRedirect()) {
                $this->_redirect($e->getRedirect());
                return;
            }
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage() ? $e->getMessage() : $this->__('Cannot initialize the Klarna Checkout (%1)', get_class($e)));
            $this->helper()->logError("[" . __METHOD__ . "] (" . get_class($e) . ") {$e->getMessage()} ");
            $this->helper()->logCritical($e);
        }

        $this->coreRegistry->register('quote', $checkout->getQuote());

        $this->getSession()->setCartWasUpdated(false);


        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Klarna Checkout'));
        return $resultPage;
    }

}
