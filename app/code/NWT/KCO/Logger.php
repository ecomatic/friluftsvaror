<?php
namespace NWT\KCO;

class Logger extends \Magento\Framework\Logger\Monolog
{

    const KCOPUSH       = 13; //need to be lower than 100
    const KCOCAPTURE    = 11;

    public function __construct($name, array $handlers = array(), array $processors = array())
    {
        static::$levels[static::KCOPUSH]    = 'PUSH';
        static::$levels[static::KCOCAPTURE] = 'CAPTURE';
        parent::__construct($name,$handlers,$processors);

    }

    public function push($message, array $context = array())
    {
        return $this->addRecord(static::KCOPUSH, $message, $context);
    }

    public function capture($message, array $context = array())
    {
        return $this->addRecord(static::KCOCAPTURE, $message, $context);
    }

}


