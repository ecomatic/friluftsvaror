<?php

/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 * 
 *
 */

/**
 * Checkout Block
 */
 
namespace NWT\KCO\Block;


class Klarna extends \Magento\Framework\View\Element\Template
{


    /**
     * @var \NWT\KCO\Helper\Data
     */
    protected $nwtkcoHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $coreRegistry,
     * @param \NWT\KCO\Helper\Data $nwtkcoHelper
     * @param array $data
     */

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \NWT\KCO\Helper\Data $nwtkcoHelper,
        array $data = []
    )
    {

        $this->nwtkcoHelper = $nwtkcoHelper;
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }


    public function getKlarnaOrder()
    {
        return $this->coreRegistry->registry('KlarnaOrder');
    }
    public function getKCOHelper(){
        return $this->nwtkcoHelper;
    }
 }

