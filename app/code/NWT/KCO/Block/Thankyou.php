<?php

/**
 * Klarna Checkout extension
 *
 * @category    NWT
 * @package     NWT_KCO
 * @copyright   Copyright (c) 2015 Nordic Web Team ( http://nordicwebteam.se/ )
 * @license     http://nordicwebteam.se/licenses/nwtcl/1.0.txt  NWT Commercial License (NWTCL 1.0)
 * 
 *
 */

/**
 * Checkout Block
 */
 
namespace NWT\KCO\Block;


class Thankyou extends \Magento\Checkout\Block\Onepage\Success
{

    /**
     * @var \NWT\KCO\Helper\Data
     */
    protected $nwtkcoHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;


    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Framework\Registry $coreRegistry,
     * @param \NWT\KCO\Helper\Data $nwtkcoHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Framework\Registry $coreRegistry,
        \NWT\KCO\Helper\Data $nwtkcoHelper,
        array $data = []
    ) {

        $this->coreRegistry = $coreRegistry;
        $this->nwtkcoHelper = $nwtkcoHelper;

        parent::__construct($context,$checkoutSession,$orderConfig,$httpContext,$data);
    }


    public function getKlarnaOrder()
    {
        return $this->coreRegistry->registry('KlarnaOrder');
    }

    public function isTest() {
        return $this->getRequest()->getParam('nwtkco') == 'test';
    }


}

