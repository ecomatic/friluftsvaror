<?php

namespace NWT\KCO\Block;

/**
 * PayPal order review page validation messages block
 */
class Messages extends \Magento\Framework\View\Element\Messages
{

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->addMessages($this->messageManager->getMessages(true));
        return parent::_prepareLayout();
    }
    
  
}
