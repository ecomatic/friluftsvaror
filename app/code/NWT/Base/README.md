## Synopsis

NWT Base modules

## Motivation

BASE for the other NWT's modules

## Technical feature

Define menu & config entry

## Contributors

https://www.nordicwebteam.se

## Installation

### Manual
 *download and copy all files  into app/code/NWT/Base/* directory

### Git (Submodule)

  * if you use git into your magento folder, use submodule (or modman)

 `git submodule add git@bitbucket.org:nordicwebteam/nwt-base-m2.git app/code/NWT/Base`
 
  * or, if u don't use git into magento folder, use 

   `git clone git@bitbucket.org:nordicwebteam/nwt-base-m2.git app/code/NWT/Base`

### Modman
  `modman clone git@bitbucket.org:nordicwebteam/nwt-base-m2.git` (the module will be deployed into **app/code/NWT/Base**)

### Composer

* the module will be deployed into `vendor/nwt/base`

   Add 

     {
        "type": "vcs",
        "url": "git@bitbucket.org:nordicwebteam/nwt-base-m2.git"
     }

   into **composer.json, repositories section**
    
   Run 

     composer require --prefer-source  'nwt/base:1.0.*'

   After that, 

     bin/magento module:enable --clear-static-content NWT_Base
     bin/magento setup:upgrade