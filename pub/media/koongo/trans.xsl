<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:g="http://base.google.com/ns/1.0">
<xsl:output method="xml" version="1.0" encoding="utf-8" cdata-section-elements="title description g:google_product_category g:product_type g:product_type link mobile_link g:image_link" />
<xsl:param name="country"/>
<xsl:param name="language"/>
<xsl:param name="locale"/>
<xsl:param name="time"/>
<xsl:param name="date"/>
<xsl:param name="date_time"/>
<xsl:param name="currency"/>
<xsl:param name="file_url"/>

<xsl:template name="categories">
	<xsl:param name="element_name" />	 
	<xsl:for-each select="multi_attributes">
		<xsl:for-each select="categories/category">				    	 				    	 
	    	<xsl:element name="{$element_name}"><xsl:value-of select="name"/></xsl:element>													 	 	
		</xsl:for-each>											 	 	
	</xsl:for-each>						  			
</xsl:template>
<xsl:template name="copy">  	 
	<xsl:copy>
		<xsl:apply-templates/>
	</xsl:copy>							  			
</xsl:template>
<xsl:template name="media_gallery">
	<xsl:param name="element_name" />	 
	<xsl:for-each select="multi_attributes">
		<xsl:for-each select="media_gallery/image">				    	 				    	 
			<xsl:element name="{$element_name}"><xsl:value-of select="value"/></xsl:element>													 	 	
		</xsl:for-each>											 	 	
	</xsl:for-each>						  			
</xsl:template>
<xsl:template name="custom_attributes">
	<xsl:for-each select="custom_attributes/attribute">
		<xsl:element name="{tag}"><xsl:value-of select="value"/></xsl:element>							
	</xsl:for-each>					  			
</xsl:template>
<xsl:template name="basic_attributes">
	<xsl:for-each select="attributes">										
		<xsl:for-each select="*"> 				    	 				    	 
		   	<xsl:call-template name="copy"/>													 	 	
		</xsl:for-each>							
	</xsl:for-each>						  			
</xsl:template>


<xsl:template match="/">	
	<rss version="2.0">						
		<xsl:element name="channel">
			<xsl:element name="title"><xsl:value-of select="$title"/></xsl:element>
			<xsl:element name="link"><xsl:value-of select="$link"/></xsl:element>
			<xsl:element name="description"><xsl:value-of select="$description"/></xsl:element>
			<xsl:for-each select="/items/item">
					<xsl:for-each select="parent|child">
							<xsl:element name="item">
								<xsl:for-each select="attributes">
									<xsl:for-each select="*[name()!='sale_price' and name()!='sale_price_effective_date_from' and name()!='sale_price_effective_date_to' and name()!='upc' and name()!='isbn' and name()!='ean' and name()!='jan' and name()!='ship_country' and name()!='ship_region' and name()!='service' and name()!='ship_price' and name()!='product_type_main']"> 				    	 				    	 
			   							<xsl:choose>
											<xsl:when test="name() = 'link' or name() = 'title' or name() = 'description' or name() = 'promotion_id'">
												<xsl:element name="{name()}"><xsl:value-of select="." /></xsl:element>
											</xsl:when>
											<xsl:otherwise><xsl:element name="g:{name()}"><xsl:value-of select="." /></xsl:element></xsl:otherwise>									
										</xsl:choose>													 	 	
									</xsl:for-each>	
									
									<xsl:if test="sale_price != price">
										<xsl:element name="g:sale_price">
											<xsl:value-of select="sale_price"/>
										</xsl:element>
									</xsl:if>
																						
									<xsl:if test="sale_price_effective_date_to != '' and sale_price_effective_date_from != ''">
										<xsl:element name="g:sale_price_effective_date">
											<xsl:value-of select="sale_price_effective_date_from"/>
											<xsl:text>/</xsl:text>
											<xsl:value-of select="sale_price_effective_date_to"/>
										</xsl:element>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="upc != ''">
											<xsl:element name="g:gtin"><xsl:value-of select="upc" /></xsl:element>
											<xsl:if test="not(brand) and not(mpn) and not(identifier_exists)">
												<xsl:element name="g:identifier_exists"><xsl:text>FALSE</xsl:text></xsl:element>
											</xsl:if>										
										</xsl:when>
										<xsl:when test="ean != ''">
											<xsl:element name="g:gtin"><xsl:value-of select="ean" /></xsl:element>
											<xsl:if test="not(brand) and not(mpn) and not(identifier_exists)">
												<xsl:element name="g:identifier_exists"><xsl:text>FALSE</xsl:text></xsl:element>
											</xsl:if>
										</xsl:when>										
										<xsl:when test="jan != ''">
											<xsl:element name="g:gtin"><xsl:value-of select="jan" /></xsl:element>
											<xsl:if test="not(brand) and not(mpn) and not(identifier_exists)">
												<xsl:element name="g:identifier_exists"><xsl:text>FALSE</xsl:text></xsl:element>
											</xsl:if>
										</xsl:when>
										<xsl:when test="isbn != ''">
											<xsl:element name="g:gtin"><xsl:value-of select="isbn" /></xsl:element>
											<xsl:if test="not(brand) and not(mpn) and not(identifier_exists)">
												<xsl:element name="g:identifier_exists"><xsl:text>FALSE</xsl:text></xsl:element>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="(not(brand) or not(mpn)) and not(identifier_exists)">
												<xsl:element name="g:identifier_exists"><xsl:text>FALSE</xsl:text></xsl:element>
											</xsl:if>				
										</xsl:otherwise>
									</xsl:choose>								
									<xsl:if test="ship_price != ''">
										<xsl:element name="g:shipping">
											<xsl:element name="g:country"><xsl:value-of select="ship_country"/></xsl:element>
											<xsl:element name="g:region"><xsl:value-of select="ship_region"/></xsl:element>
											<xsl:element name="g:service"><xsl:value-of select="service"/></xsl:element>
											<xsl:element name="g:price"><xsl:value-of select="ship_price"/></xsl:element>
										</xsl:element>
									</xsl:if>
									<xsl:if test="ship_country1 != ''">
										<xsl:element name="g:shipping">
											<xsl:element name="g:country"><xsl:value-of select="ship_country1"/></xsl:element>
											<xsl:element name="g:region"><xsl:value-of select="ship_region1"/></xsl:element>
											<xsl:element name="g:service"><xsl:value-of select="service1"/></xsl:element>
											<xsl:element name="g:price"><xsl:value-of select="ship_price1"/></xsl:element>
										</xsl:element>
									</xsl:if>
									<xsl:if test="ship_country2 != ''">
										<xsl:element name="g:shipping">
											<xsl:element name="g:country"><xsl:value-of select="ship_country2"/></xsl:element>
											<xsl:element name="g:region"><xsl:value-of select="ship_region2"/></xsl:element>
											<xsl:element name="g:service"><xsl:value-of select="service2"/></xsl:element>
											<xsl:element name="g:price"><xsl:value-of select="ship_price2"/></xsl:element>
										</xsl:element>
									</xsl:if>
								</xsl:for-each>								
								<xsl:choose>
									<xsl:when test="attributes/product_type_main != ''">
										<xsl:element name="g:product_type"><xsl:value-of select="attributes/product_type_main"/></xsl:element>										
									</xsl:when>
									<xsl:otherwise>
										<xsl:for-each select="multi_attributes">
											<xsl:for-each select="categories">		    	 				    	 								    										    	
										    	<xsl:if test="category[1]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[1]/path"/></xsl:element></xsl:if>
											    <xsl:if test="category[2]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[2]/path"/></xsl:element></xsl:if>	
											    <xsl:if test="category[3]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[3]/path"/></xsl:element></xsl:if>
											    <xsl:if test="category[4]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[4]/path"/></xsl:element></xsl:if>
											    <xsl:if test="category[5]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[5]/path"/></xsl:element></xsl:if>
											    <xsl:if test="category[6]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[6]/path"/></xsl:element></xsl:if>
											    <xsl:if test="category[7]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[7]/path"/></xsl:element></xsl:if>
											    <xsl:if test="category[8]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[8]/path"/></xsl:element></xsl:if>
											    <xsl:if test="category[9]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[9]/path"/></xsl:element></xsl:if>
											    <xsl:if test="category[10]/path != ''"><xsl:element name="g:product_type"><xsl:value-of select="category[10]/path"/></xsl:element></xsl:if>												 	 	
											</xsl:for-each>											 	 	
										</xsl:for-each>	
									</xsl:otherwise>
								</xsl:choose>		
								<xsl:for-each select="multi_attributes/media_gallery">																			    	 				    	 
								    <xsl:if test="image[1]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[1]/value"/></xsl:element></xsl:if>
								    <xsl:if test="image[2]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[2]/value"/></xsl:element></xsl:if>	
								    <xsl:if test="image[3]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[3]/value"/></xsl:element></xsl:if>
								    <xsl:if test="image[4]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[4]/value"/></xsl:element></xsl:if>
								    <xsl:if test="image[5]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[5]/value"/></xsl:element></xsl:if>
								    <xsl:if test="image[6]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[6]/value"/></xsl:element></xsl:if>
								    <xsl:if test="image[7]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[7]/value"/></xsl:element></xsl:if>
								    <xsl:if test="image[8]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[8]/value"/></xsl:element></xsl:if>
								    <xsl:if test="image[9]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[9]/value"/></xsl:element></xsl:if>
								    <xsl:if test="image[10]/value != ''"><xsl:element name="g:additional_image_link"><xsl:value-of select="image[10]/value"/></xsl:element></xsl:if>					    										    										 	 																															 	 	
								</xsl:for-each>	
								<xsl:call-template name="custom_attributes"/>														
							</xsl:element>					
					</xsl:for-each> 
			</xsl:for-each>    
		</xsl:element>   
	</rss>       	
</xsl:template>



</xsl:stylesheet>

